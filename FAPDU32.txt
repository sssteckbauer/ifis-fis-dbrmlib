DBRM   �APCPSB  FAPDU32 mA^! 2                                     1  �NK
                                                                                
DBRM        
q     DECLARE MSL_MSG_LINE_V TABLE ( CMT_ID DECIMAL ( 8 ) NOT
NULL WITH DEFAULT , MDRDEST1 CHAR ( 8 ) NOT NULL WITH DEFAULT , MDRDEST2 CHAR (
8 ) NOT NULL WITH DEFAULT , MDROSDSC CHAR ( 2 ) NOT NULL WITH DEFAULT , MDROSRTC
 CHAR ( 2 ) NOT NULL WITH DEFAULT , MDRDSTID CHAR ( 8 ) NOT NULL WITH DEFAULT ,
MDRSEVCD CHAR ( 1 ) NOT NULL WITH DEFAULT , MDRTEXTL DECIMAL ( 4 ) NOT NULL WITH
 DEFAULT , MSG_1 CHAR ( 66 ) NOT NULL WITH DEFAULT , MSG_2 CHAR ( 66 ) NOT NULL
WITH DEFAULT , FK_MSG_MSG_KEY CHAR ( 8 ) NOT NULL , MSG_TS TIMESTAMP NOT NULL )
    
DBRM  �           �DECLARE CKH_CHK_HDR_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , BANK_ACCT_CD CHAR ( 2 ) NOT NULL , CHECK_NBR CHAR
( 8 ) NOT NULL , CHECK_DT DATE NOT NULL WITH DEFAULT , CHECK_TYP_CD CHAR ( 1 ) N
OT NULL WITH DEFAULT , CHECK_AMT DECIMAL ( 13 , 2 ) NOT NULL WITH DEFAULT , CHEC
K_RGSTR_DT DATE NOT NULL WITH DEFAULT , CNCL_IND CHAR ( 1 ) NOT NULL WITH DEFAUL
T , PRCSD_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , RCNCLTN_IND CHAR ( 1 ) NOT NULL
 WITH DEFAULT , CHECK_RCNCLTN_DT DATE NOT NULL WITH DEFAULT , DISCREPANCY_IND CH
AR ( 1 ) NOT NULL WITH DEFAULT , CNCL_DT DATE NOT NULL WITH DEFAULT , STOP_PAID_
IND CHAR ( 1 ) NOT NULL WITH DEFAULT , ISSUE_BANK_DT DATE NOT NULL WITH DEFAULT
, ISSU_RGSTR_DT DATE NOT NULL WITH DEFAULT , CHECK_CNCL_CD CHAR ( 1 ) NOT NULL W
ITH DEFAULT , FK_CKR_CHECK_ISEQ DECIMAL ( 8 ) NOT NULL WITH DEFAULT , CKR_SET_TS
 TIMESTAMP NOT NULL WITH DEFAULT , IDX_CHK_DT_TS TIMESTAMP NOT NULL WITH DEFAULT
 , IDX_RC_TS TIMESTAMP NOT NULL WITH DEFAULT , IDX_REG_DT_TS TIMESTAMP NOT NULL
WITH DEFAULT )     
DBRM  	1      �     	NDECLARE CKR_CHK_RQST_H_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , CHECK_RQST_ISEQ DECIMAL ( 8 ) NOT NULL , ORGN_C
D CHAR ( 4 ) NOT NULL WITH DEFAULT , RQSTR_ID_DGT_ONE CHAR ( 1 ) NOT NULL WITH D
EFAULT , RQSTR_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , RQST_DT DATE NOT N
ULL WITH DEFAULT , SRVCD_DT DATE NOT NULL WITH DEFAULT , ADR_TYP_CD CHAR ( 2 ) N
OT NULL WITH DEFAULT , BANK_ACCT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , RQST_CHEC
K_AMT DECIMAL ( 13 , 2 ) NOT NULL WITH DEFAULT , AP_FDRL_WTHHLD_PCT DECIMAL ( 6
, 3 ) NOT NULL WITH DEFAULT , AP_ST_WTHHLD_PCT DECIMAL ( 6 , 3 ) NOT NULL WITH D
EFAULT , AP_1099_RPRT_ID CHAR ( 9 ) NOT NULL WITH DEFAULT , AP_RSBLE_IND CHAR (
1 ) NOT NULL WITH DEFAULT , TA_ADVNC_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , TA_R
ECN_EVENT_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , TA_RCNCLTN_DT DATE NOT NULL WIT
H DEFAULT , AR_TRNSC_NBR DECIMAL ( 7 ) NOT NULL WITH DEFAULT , ENTPSN_IND CHAR (
 1 ) NOT NULL WITH DEFAULT , ACH_RCVNG_DFI_ID DECIMAL ( 8 ) NOT NULL WITH DEFAUL
T , ACH_CHECK_DGT DECIMAL ( 1 ) NOT NULL WITH DEFAULT , ACH_DFI_ACCT_NBR CHAR (
17 ) NOT NULL WITH DEFAULT , ACH_ACCT_TYP CHAR ( 1 ) NOT NULL WITH DEFAULT , CHK
_CNCL_CD CHAR ( 1 ) NOT NULL WITH DEFAULT , CHK_RQST_DATA CHAR ( 2 ) NOT NULL WI
TH DEFAULT , CHECK_GRPNG_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , VNDR_NAME CHAR (
 35 ) NOT NULL WITH DEFAULT , IDX_RQSTR_TS TIMESTAMP NOT NULL WITH DEFAULT )    
 
DBRM  e      �     �DECLARE CKD_CHK_DTL_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , DOC_NBR CHAR ( 8 ) NOT NULL , ITEM_NBR DECIMAL ( 4
 ) NOT NULL , SEQ_NBR DECIMAL ( 4 ) NOT NULL , COA_CD CHAR ( 1 ) NOT NULL WITH D
EFAULT , ACCT_INDX_CD CHAR ( 10 ) NOT NULL WITH DEFAULT , FUND_CD CHAR ( 6 ) NOT
 NULL WITH DEFAULT , ORGN_CD CHAR ( 6 ) NOT NULL WITH DEFAULT , ACCT_CD CHAR ( 6
 ) NOT NULL WITH DEFAULT , PRGRM_CD CHAR ( 6 ) NOT NULL WITH DEFAULT , ACTVY_CD
CHAR ( 6 ) NOT NULL WITH DEFAULT , LCTN_CD CHAR ( 6 ) NOT NULL WITH DEFAULT , AP
RVD_AMT DECIMAL ( 12 , 2 ) NOT NULL WITH DEFAULT , DSCNT_AMT DECIMAL ( 12 , 2 )
NOT NULL WITH DEFAULT , TAX_AMT DECIMAL ( 12 , 2 ) NOT NULL WITH DEFAULT , ADDL_
CHRG DECIMAL ( 12 , 2 ) NOT NULL WITH DEFAULT , PAID_AMT DECIMAL ( 12 , 2 ) NOT
NULL WITH DEFAULT , FDRL_WTHHLD DECIMAL ( 8 , 2 ) NOT NULL WITH DEFAULT , STATE_
WTHHLD DECIMAL ( 8 , 2 ) NOT NULL WITH DEFAULT , PSTNG_PRD CHAR ( 2 ) NOT NULL W
ITH DEFAULT , FSCL_YR CHAR ( 2 ) NOT NULL WITH DEFAULT , RULE_CLS_CD CHAR ( 4 )
NOT NULL WITH DEFAULT , DSCNT_RULE_CLS CHAR ( 4 ) NOT NULL WITH DEFAULT , TAX_RU
LE_CLS CHAR ( 4 ) NOT NULL WITH DEFAULT , ADDL_CHRG_RULE_CLS CHAR ( 4 ) NOT NULL
 WITH DEFAULT , PO_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , PO_ITEM_NBR DECIMAL (
4 ) NOT NULL WITH DEFAULT , LIQDTN_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , PRJCT_
CD CHAR ( 8 ) NOT NULL WITH DEFAULT , DOC_TYP_SEQ_NBR DECIMAL ( 4 ) NOT NULL WIT
H DEFAULT , ADJMT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , VNDR_INV_NBR CHAR ( 9 )
NOT NULL WITH DEFAULT , VNDR_INV_DT DATE NOT NULL WITH DEFAULT , DOC_REF_NBR CHA
R ( 10 ) NOT NULL WITH DEFAULT , TAX_RATE_CD CHAR ( 3 ) NOT NULL WITH DEFAULT ,
FK_CKH_BANK_ACCTCD CHAR ( 2 ) NOT NULL , FK_CKH_CHECK_NBR CHAR ( 8 ) NOT NULL ,
CKH_SET_TS TIMESTAMP NOT NULL , IDX_CHK_DTL_TS TIMESTAMP NOT NULL WITH DEFAULT )
     
DBRM        �      �DECLARE BNK_BANK_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL W
ITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR ( 2
 ) NOT NULL WITH DEFAULT , BANK_ACCT_CD CHAR ( 2 ) NOT NULL , ACTVY_DT DATE NOT
NULL WITH DEFAULT )     
DBRM  	}           	�DECLARE IVA_INV_ACCT_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , NS
F_OVRDE CHAR ( 1 ) NOT NULL , ADDL_CHRG_PCT DECIMAL ( 6 , 3 ) NOT NULL , INVD_AM
T DECIMAL ( 12 , 2 ) NOT NULL , DSCNT_AMT DECIMAL ( 12 , 2 ) NOT NULL , LIQDTN_I
ND CHAR ( 1 ) NOT NULL , COA_CD CHAR ( 1 ) NOT NULL , ACCT_INDX_CD CHAR ( 10 ) N
OT NULL , FUND_CD CHAR ( 6 ) NOT NULL , ORGN_CD CHAR ( 6 ) NOT NULL , ACCT_CD CH
AR ( 6 ) NOT NULL , PRGRM_CD CHAR ( 6 ) NOT NULL , ACTVY_CD CHAR ( 6 ) NOT NULL
, LCTN_CD CHAR ( 6 ) NOT NULL , ACCT_ERROR_IND CHAR ( 1 ) NOT NULL , FSCL_YR CHA
R ( 2 ) NOT NULL , BANK_ACCT_CD CHAR ( 2 ) NOT NULL , APRVD_AMT DECIMAL ( 12 , 2
 ) NOT NULL , TAX_AMT DECIMAL ( 12 , 2 ) NOT NULL , INVD_PCT DECIMAL ( 6 , 3 ) N
OT NULL , DSCNT_PCT DECIMAL ( 6 , 3 ) NOT NULL , ADDL_CHRG DECIMAL ( 12 , 2 ) NO
T NULL , INCM_TYP_SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , PAID_AMT DECIMAL ( 12 , 2
 ) NOT NULL , RULE_CLS_CD CHAR ( 4 ) NOT NULL , DSCNT_RULE_CLS CHAR ( 4 ) NOT NU
LL , TAX_RULE_CLS CHAR ( 4 ) NOT NULL , ADDL_CHRG_RUL_CLS CHAR ( 4 ) NOT NULL ,
PSTNG_PRD CHAR ( 2 ) NOT NULL , PRJCT_CD CHAR ( 8 ) NOT NULL , PO_NBR CHAR ( 8 )
 NOT NULL , PO_ITEM_NBR DECIMAL ( 4 , 0 ) NOT NULL , PO_SEQ_NBR DECIMAL ( 4 , 0
) NOT NULL , FK_IVD_DOC_NBR CHAR ( 8 ) NOT NULL , FK_IVD_ITEM_NBR DECIMAL ( 4 ,
0 ) NOT NULL , TRD_IN_IND CHAR ( 1 ) NOT NULL , TRD_IN_AMT DECIMAL ( 12 , 2 ) NO
T NULL , TRD_RULE_CLS CHAR ( 4 ) NOT NULL )     
DBRM  o      �     :DECLARE BKE_BANK_EFCTV_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , START_DT DA
TE NOT NULL , TIME_STMP TIME NOT NULL , END_DT DATE NOT NULL WITH DEFAULT , STAT
US CHAR ( 1 ) NOT NULL WITH DEFAULT , COA_CD CHAR ( 1 ) NOT NULL WITH DEFAULT ,
BANK_ACCT_NBR CHAR ( 15 ) NOT NULL WITH DEFAULT , BANK_CD_DESC CHAR ( 35 ) NOT N
ULL WITH DEFAULT , BANK_FUND_CD CHAR ( 6 ) NOT NULL WITH DEFAULT , CASH_ACCT_CD
CHAR ( 6 ) NOT NULL WITH DEFAULT , INTRFND_ACCT_CD CHAR ( 6 ) NOT NULL WITH DEFA
ULT , BANK_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK_BNK_BANK_ACCTCD CHAR
 ( 2 ) NOT NULL )     
DBRM  .     �     �DECLARE S_4158_4159_N CURSOR FOR SELECT USER_CD , LAST_A
CTVY_DT , UNVRS_CD , DOC_NBR , ITEM_NBR , SEQ_NBR , COA_CD , ACCT_INDX_CD , FUND
_CD , ORGN_CD , ACCT_CD , PRGRM_CD , ACTVY_CD , LCTN_CD , APRVD_AMT , DSCNT_AMT
, TAX_AMT , ADDL_CHRG , PAID_AMT , FDRL_WTHHLD , STATE_WTHHLD , PSTNG_PRD , FSCL
_YR , RULE_CLS_CD , DSCNT_RULE_CLS , TAX_RULE_CLS , ADDL_CHRG_RULE_CLS , PO_NBR
, PO_ITEM_NBR , LIQDTN_IND , PRJCT_CD , DOC_TYP_SEQ_NBR , ADJMT_CD , VNDR_INV_NB
R , VNDR_INV_DT , DOC_REF_NBR , TAX_RATE_CD , FK_CKH_BANK_ACCTCD , FK_CKH_CHECK_
NBR , CKH_SET_TS , IDX_CHK_DTL_TS FROM CKD_CHK_DTL_V WHERE FK_CKH_BANK_ACCTCD =
: H AND FK_CKH_CHECK_NBR = : H AND DOC_NBR >= : H AND ( ( DOC_NBR = : H AND ITEM
_NBR = : H AND SEQ_NBR = : H AND CKH_SET_TS > : H ) OR ( DOC_NBR = : H AND ITEM_
NBR = : H AND SEQ_NBR > : H ) OR ( DOC_NBR = : H AND ITEM_NBR > : H ) OR ( DOC_N
BR > : H ) ) ORDER BY DOC_NBR ASC , ITEM_NBR ASC , SEQ_NBR ASC , CKH_SET_TS ASC
OPTIMIZE FOR 1 ROW     � ��       D    DBLINK-S-4158-4159-KEY-O.DBLINK-S-4
158-4159-01 � d�       D    DBLINK-S-4158-4159-KEY-O.DBLINK-S-4158-4159-02 
� p�       D    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03 � [�      
 D    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03 � {�       U    DBL
INK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-04 � K�       U    DBLINK-S-4158-4
159-KEY-U.DBLINK-S-4158-4159-05 � X�       D    DBLINK-S-4158-4159-KEY-S.DB
LINK-S-4158-4159-06 � ��       D    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-
4159-03 � �       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-04 � 
�       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-05 � �       D
    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03 � ��       U    DBLINK-
S-4158-4159-KEY-U.DBLINK-S-4158-4159-04 � ;�       D    DBLINK-S-4158-4159-
KEY-U.DBLINK-S-4158-4159-03
DBRM            �DECLARE S_4033_4062_P CURSOR FOR SELECT USER_CD , LAST_A
CTVY_DT , START_DT , TIME_STMP , END_DT , STATUS , COA_CD , BANK_ACCT_NBR , BANK
_CD_DESC , BANK_FUND_CD , CASH_ACCT_CD , INTRFND_ACCT_CD , BANK_IREF_ID , FK_BNK
_BANK_ACCTCD FROM BKE_BANK_EFCTV_V WHERE FK_BNK_BANK_ACCTCD = : H AND START_DT <
= : H AND ( ( START_DT = : H AND TIME_STMP < : H ) OR ( START_DT < : H ) ) ORDER
 BY START_DT DESC , TIME_STMP DESC OPTIMIZE FOR 1 ROW  	   � �       D    
DBLINK-S-4033-4062-KEY-O.DBLINK-S-4033-4062-01 � �       D    DBLINK-S-403
3-4062-KEY-U.DBLINK-S-4033-4062-02 � ��       D    DBLINK-S-4033-4062-KEY-U
.DBLINK-S-4033-4062-02 � ��       D    DBLINK-S-4033-4062-KEY-U.DBLINK-S-40
33-4062-03 � %�       D    DBLINK-S-4033-4062-KEY-U.DBLINK-S-4033-4062-02
DBRM   N     (      �SELECT ORGN_CD INTO : H FROM CKR_CHK_RQST_H_V WHERE CHEC
K_RQST_ISEQ = : H       
        D    CKR-CKR-CHK-RQST-H.CKR-ORGN-CD   �
�       U    
CKH-CKH-CHK-HDR.CKH-FK-CKR-CHECK-ISEQ
DBRM  �     �      �SELECT DSCNT_AMT , TRD_IN_IND , TRD_IN_AMT INTO : H , :
H , : H FROM IVA_INV_ACCT_V WHERE FK_IVD_DOC_NBR = : H AND FK_IVD_ITEM_NBR = : H
 AND SEQ_NBR = : H              U    IVA-IVA-INV-ACCT.IVA-DSCNT-AMT   
        D    IVA-IVA-INV-ACCT.IVA-TRD-IN-IND          U    IVA-IVA-IN
V-ACCT.IVA-TRD-IN-AMT   %�       D    IVA-IVA-INV-ACCT.IVA-FK-IVD-DOC-NBR 
  f�       U    IVA-IVA-INV-ACCT.IVA-FK-IVD-ITEM-NBR   q�       U    IV
A-IVA-INV-ACCT.IVA-SEQ-NBR
DBRM   	    �      �SELECT MSG_1 , MSG_2 INTO : H , : H FROM MSL_MSG_LINE_V
WHERE FK_MSG_MSG_KEY = : H               �D    MSL-MSL-MSG-LINE.MSL-MSG-1 
          �D    MSL-MSL-MSG-LINE.MSL-MSG-2   &�       D    MSL-MSL-MSG
-LINE.MSL-FK-MSG-MSG-KEY
DBRM  t     �      OPEN S_4158_4159_N     �   �       D    DBLINK-S-41
58-4159-KEY-O.DBLINK-S-4158-4159-01 �   �       D    DBLINK-S-4158-4159-KEY-
O.DBLINK-S-4158-4159-02 �   �       D    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4
158-4159-03 �   �       D    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03 
�   �       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-04 �   �     
  U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-05 �   �       D    DBL
INK-S-4158-4159-KEY-S.DBLINK-S-4158-4159-06 �   �       D    DBLINK-S-4158-4
159-KEY-U.DBLINK-S-4158-4159-03 �   �       U    DBLINK-S-4158-4159-KEY-U.DB
LINK-S-4158-4159-04 �   �       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-
4159-05 �   �       D    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03 �  
 �       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-04 �   �       D
    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03
DBRM  @     7     FETCH S_4158_4159_N INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H , : H , : H , : H               D    C
KD-CKD-CHK-DTL.CKD-USER-CD           	D    CKD-CKD-CHK-DTL.CKD-LAST-ACTVY-D
T           D    CKD-CKD-CHK-DTL.CKD-UNVRS-CD           D    CKD-CKD
-CHK-DTL.CKD-DOC-NBR           U    CKD-CKD-CHK-DTL.CKD-ITEM-NBR      
     U    CKD-CKD-CHK-DTL.CKD-SEQ-NBR           D    CKD-CKD-CHK-DTL.CKD
-COA-CD   �        D    CKD-CKD-CHK-DTL.CKD-ACCT-INDX-CD   �        D  
  CKD-CKD-CHK-DTL.CKD-FUND-CD   &        D    CKD-CKD-CHK-DTL.CKD-ORGN-CD 
  �        D    CKD-CKD-CHK-DTL.CKD-ACCT-CD   *        D    CKD-CKD-CH
K-DTL.CKD-PRGRM-CD   �        D    CKD-CKD-CHK-DTL.CKD-ACTVY-CD   �      
  D    CKD-CKD-CHK-DTL.CKD-LCTN-CD   >       U    CKD-CKD-CHK-DTL.CKD-A
PRVD-AMT   �       U    CKD-CKD-CHK-DTL.CKD-DSCNT-AMT   :       U    
CKD-CKD-CHK-DTL.CKD-TAX-AMT   �       U    CKD-CKD-CHK-DTL.CKD-ADDL-CHRG 
  f       U    CKD-CKD-CHK-DTL.CKD-PAID-AMT   �       U    CKD-CKD-C
HK-DTL.CKD-FDRL-WTHHLD   k       U    CKD-CKD-CHK-DTL.CKD-STATE-WTHHLD  
 q        D    CKD-CKD-CHK-DTL.CKD-PSTNG-PRD   �        D    CKD-CKD-CHK
-DTL.CKD-FSCL-YR   u        D    CKD-CKD-CHK-DTL.CKD-RULE-CLS-CD   �     
   D    CKD-CKD-CHK-DTL.CKD-DSCNT-RULE-CLS   �        D    CKD-CKD-CHK-D
TL.CKD-TAX-RULE-CLS   �        D    CKD-CKD-CHK-DTL.CKD-ADDL-CHRG-RULE-CLS 
  �        D    CKD-CKD-CHK-DTL.CKD-PO-NBR   B        U    CKD-CKD-CHK
-DTL.CKD-PO-ITEM-NBR   H        D    CKD-CKD-CHK-DTL.CKD-LIQDTN-IND   �  
      D    CKD-CKD-CHK-DTL.CKD-PRJCT-CD   M        U    CKD-CKD-CHK-DTL.
CKD-DOC-TYP-SEQ-NBR   �        D    CKD-CKD-CHK-DTL.CKD-ADJMT-CD   \     
   D    CKD-CKD-CHK-DTL.CKD-VNDR-INV-NBR   W        D    CKD-CKD-CHK-DTL
.CKD-VNDR-INV-DT   �        D    CKD-CKD-CHK-DTL.CKD-DOC-REF-NBR   2     
   D    CKD-CKD-CHK-DTL.CKD-TAX-RATE-CD   8        D    CKD-CKD-CHK-DTL.
CKD-FK-CKH-BANK-ACCTCD   �        D    CKD-CKD-CHK-DTL.CKD-FK-CKH-CHECK-NBR
          D    CKD-CKD-CHK-DTL.CKD-CKH-SET-TS          D    CKD-CK
D-CHK-DTL.CKD-IDX-CHK-DTL-TS
DBRM  c     �      OPEN S_4033_4062_P  	   �   �       D    DBLINK-S-40
33-4062-KEY-O.DBLINK-S-4033-4062-01 �   �       D    DBLINK-S-4033-4062-KEY-
U.DBLINK-S-4033-4062-02 �   �       D    DBLINK-S-4033-4062-KEY-U.DBLINK-S-4
033-4062-02 �   �       D    DBLINK-S-4033-4062-KEY-U.DBLINK-S-4033-4062-03 
�   �       D    DBLINK-S-4033-4062-KEY-U.DBLINK-S-4033-4062-02
DBRM  k     %      ,FETCH S_4033_4062_P INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H               D    BKE
-BKE-BANK-EFCTV.BKE-USER-CD           	D    BKE-BKE-BANK-EFCTV.BKE-LAST-ACT
VY-DT           D    BKE-BKE-BANK-EFCTV.BKE-START-DT           D    
BKE-BKE-BANK-EFCTV.BKE-TIME-STMP           D    BKE-BKE-BANK-EFCTV.BKE-END
-DT           D    BKE-BKE-BANK-EFCTV.BKE-STATUS           D    BKE-
BKE-BANK-EFCTV.BKE-COA-CD   �        D    BKE-BKE-BANK-EFCTV.BKE-BANK-ACCT-
NBR   �        D    BKE-BKE-BANK-EFCTV.BKE-BANK-CD-DESC   &        D   
 BKE-BKE-BANK-EFCTV.BKE-BANK-FUND-CD   �        D    BKE-BKE-BANK-EFCTV.BK
E-CASH-ACCT-CD   *        D    BKE-BKE-BANK-EFCTV.BKE-INTRFND-ACCT-CD   �
        U    BKE-BKE-BANK-EFCTV.BKE-BANK-IREF-ID   �        D    BKE-BKE
-BANK-EFCTV.BKE-FK-BNK-BANK-ACCTCD
DBRM  	�     �     �SELECT USER_CD , LAST_ACTVY_DT , START_DT , TIME_STMP ,
END_DT , STATUS , COA_CD , BANK_ACCT_NBR , BANK_CD_DESC , BANK_FUND_CD , CASH_AC
CT_CD , INTRFND_ACCT_CD , BANK_IREF_ID , FK_BNK_BANK_ACCTCD INTO : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H FROM BKE_BANK
_EFCTV_V WHERE FK_BNK_BANK_ACCTCD = : H AND START_DT = : H AND TIME_STMP = : H  
     �        D    BKE-BKE-BANK-EFCTV.BKE-USER-CD   }        	D    BKE
-BKE-BANK-EFCTV.BKE-LAST-ACTVY-DT   O        D    BKE-BKE-BANK-EFCTV.BKE-ST
ART-DT   �        D    BKE-BKE-BANK-EFCTV.BKE-TIME-STMP   S        D   
 BKE-BKE-BANK-EFCTV.BKE-END-DT   Y        D    BKE-BKE-BANK-EFCTV.BKE-STAT
US   �        D    BKE-BKE-BANK-EFCTV.BKE-COA-CD   4        D    BKE-B
KE-BANK-EFCTV.BKE-BANK-ACCT-NBR   �        D    BKE-BKE-BANK-EFCTV.BKE-BANK
-CD-DESC           D    BKE-BKE-BANK-EFCTV.BKE-BANK-FUND-CD          
D    BKE-BKE-BANK-EFCTV.BKE-CASH-ACCT-CD          D    BKE-BKE-BANK-EFC
TV.BKE-INTRFND-ACCT-CD          U    BKE-BKE-BANK-EFCTV.BKE-BANK-IREF-ID 
         D    BKE-BKE-BANK-EFCTV.BKE-FK-BNK-BANK-ACCTCD � (�       D 
   DBLINK-S-4033-4062-KEY-O.DBLINK-S-4033-4062-01 � -�       D    DBLINK-S
-4033-4062-KEY-U.DBLINK-S-4033-4062-02 � ��       D    DBLINK-S-4033-4062-K
EY-U.DBLINK-S-4033-4062-03
DBRM  -     �     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , DOC_NBR , IT
EM_NBR , SEQ_NBR , COA_CD , ACCT_INDX_CD , FUND_CD , ORGN_CD , ACCT_CD , PRGRM_C
D , ACTVY_CD , LCTN_CD , APRVD_AMT , DSCNT_AMT , TAX_AMT , ADDL_CHRG , PAID_AMT
, FDRL_WTHHLD , STATE_WTHHLD , PSTNG_PRD , FSCL_YR , RULE_CLS_CD , DSCNT_RULE_CL
S , TAX_RULE_CLS , ADDL_CHRG_RULE_CLS , PO_NBR , PO_ITEM_NBR , LIQDTN_IND , PRJC
T_CD , DOC_TYP_SEQ_NBR , ADJMT_CD , VNDR_INV_NBR , VNDR_INV_DT , DOC_REF_NBR , T
AX_RATE_CD , FK_CKH_BANK_ACCTCD , FK_CKH_CHECK_NBR , CKH_SET_TS , IDX_CHK_DTL_TS
 INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
, : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
: H , : H FROM CKD_CHK_DTL_V WHERE FK_CKH_BANK_ACCTCD = : H AND FK_CKH_CHECK_NBR
 = : H AND DOC_NBR = : H AND ITEM_NBR = : H AND SEQ_NBR = : H AND CKH_SET_TS = (
 SELECT MIN ( CKH_SET_TS ) FROM CKD_CHK_DTL_V WHERE FK_CKH_BANK_ACCTCD = : H AND
 FK_CKH_CHECK_NBR = : H AND DOC_NBR = : H AND ITEM_NBR = : H AND SEQ_NBR = : H )
              D    CKD-CKD-CHK-DTL.CKD-USER-CD  
        	D    CKD-
CKD-CHK-DTL.CKD-LAST-ACTVY-DT          D    CKD-CKD-CHK-DTL.CKD-UNVRS-CD 
         D    CKD-CKD-CHK-DTL.CKD-DOC-NBR          U    CKD-CKD-CH
K-DTL.CKD-ITEM-NBR          U    CKD-CKD-CHK-DTL.CKD-SEQ-NBR  �       
 D    CKD-CKD-CHK-DTL.CKD-COA-CD  �        D    CKD-CKD-CHK-DTL.CKD-ACC
T-INDX-CD  |        D    CKD-CKD-CHK-DTL.CKD-FUND-CD  �        D    
CKD-CKD-CHK-DTL.CKD-ORGN-CD  $        D    CKD-CKD-CHK-DTL.CKD-ACCT-CD  
/        D    CKD-CKD-CHK-DTL.CKD-PRGRM-CD  �        D    CKD-CKD-CHK-
DTL.CKD-ACTVY-CD  _        D    CKD-CKD-CHK-DTL.CKD-LCTN-CD  �       
U    CKD-CKD-CHK-DTL.CKD-APRVD-AMT  `       U    CKD-CKD-CHK-DTL.CKD-DS
CNT-AMT  "       U    CKD-CKD-CHK-DTL.CKD-TAX-AMT  e       U    CK
D-CKD-CHK-DTL.CKD-ADDL-CHRG  �       U    CKD-CKD-CHK-DTL.CKD-PAID-AMT  
j       U    CKD-CKD-CHK-DTL.CKD-FDRL-WTHHLD  p       U    CKD-CKD-
CHK-DTL.CKD-STATE-WTHHLD  �        D    CKD-CKD-CHK-DTL.CKD-PSTNG-PRD  
t        D    CKD-CKD-CHK-DTL.CKD-FSCL-YR  z        D    CKD-CKD-CHK-DT
L.CKD-RULE-CLS-CD  �        D    CKD-CKD-CHK-DTL.CKD-DSCNT-RULE-CLS  �
       D    CKD-CKD-CHK-DTL.CKD-TAX-RULE-CLS  �        D    CKD-CKD-CHK
-DTL.CKD-ADDL-CHRG-RULE-CLS  A        D    CKD-CKD-CHK-DTL.CKD-PO-NBR  
G        U    CKD-CKD-CHK-DTL.CKD-PO-ITEM-NBR  �        D    CKD-CKD-CH
K-DTL.CKD-LIQDTN-IND  L        D    CKD-CKD-CHK-DTL.CKD-PRJCT-CD  R   
     U    CKD-CKD-CHK-DTL.CKD-DOC-TYP-SEQ-NBR  �        D    CKD-CKD-CHK
-DTL.CKD-ADJMT-CD  V        D    CKD-CKD-CHK-DTL.CKD-VNDR-INV-NBR  �   
     D    CKD-CKD-CHK-DTL.CKD-VNDR-INV-DT  1        D    CKD-CKD-CHK-DT
L.CKD-DOC-REF-NBR  7        D    CKD-CKD-CHK-DTL.CKD-TAX-RATE-CD  �    
    D    CKD-CKD-CHK-DTL.CKD-FK-CKH-BANK-ACCTCD          D    CKD-CKD-
CHK-DTL.CKD-FK-CKH-CHECK-NBR          D    CKD-CKD-CHK-DTL.CKD-CKH-SET-TS
          D    CKD-CKD-CHK-DTL.CKD-IDX-CHK-DTL-TS � ��       D    DB
LINK-S-4158-4159-KEY-O.DBLINK-S-4158-4159-01 � *�       D    DBLINK-S-4158-
4159-KEY-O.DBLINK-S-4158-4159-02 � >�       D    DBLINK-S-4158-4159-KEY-U.D
BLINK-S-4158-4159-03 � a�       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158
-4159-04 � l�       U    DBLINK-S-4158-4159-KEY-U.DBLINK-S-4158-4159-05 � 
2�       D    DBLINK-S-4158-4159-KEY-O.DBLINK-S-4158-4159-01 � �       
D    DBLINK-S-4158-4159-KEY-O.DBLINK-S-4158-4159-02 � �       D    DBLINK
-S-4158-4159-KEY-U.DBLINK-S-4158-4159-03 � �       U    DBLINK-S-4158-4159
-KEY-U.DBLINK-S-4158-4159-04 � ��       U    DBLINK-S-4158-4159-KEY-U.DBLIN
K-S-4158-4159-05
DBRM  }     "      �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , BANK_ACCT_CD
 , ACTVY_DT INTO : H , : H , : H , : H , : H FROM BNK_BANK_V WHERE BANK_ACCT_CD
= : H       �        D    BNK-BNK-BANK.BNK-USER-CD   &        	D    BN
K-BNK-BANK.BNK-LAST-ACTVY-DT   �        D    BNK-BNK-BANK.BNK-UNVRS-CD   
*        D    BNK-BNK-BANK.BNK-BANK-ACCT-CD   �        D    BNK-BNK-BANK
.BNK-ACTVY-DT   ��       D    BNK-BNK-BANK.BNK-BANK-ACCT-CD
DBRM  �     �     SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , BANK_ACCT_CD
 , CHECK_NBR , CHECK_DT , CHECK_TYP_CD , CHECK_AMT , CHECK_RGSTR_DT , CNCL_IND ,
 PRCSD_IND , RCNCLTN_IND , CHECK_RCNCLTN_DT , DISCREPANCY_IND , CNCL_DT , STOP_P
AID_IND , ISSUE_BANK_DT , ISSU_RGSTR_DT , CHECK_CNCL_CD , FK_CKR_CHECK_ISEQ , CK
R_SET_TS , IDX_CHK_DT_TS , IDX_RC_TS , IDX_REG_DT_TS INTO : H , : H , : H , : H
, : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
: H , : H , : H , : H , : H , : H , : H FROM CKH_CHK_HDR_V WHERE BANK_ACCT_CD =
: H AND CHECK_NBR = : H      �        D    CKH-CKH-CHK-HDR.CKH-USER-CD  
�        	D    CKH-CKH-CHK-HDR.CKH-LAST-ACTVY-DT  ?        D    CKH-CK
H-CHK-HDR.CKH-UNVRS-CD  �        D    CKH-CKH-CHK-HDR.CKH-BANK-ACCT-CD  
#        D    CKH-CKH-CHK-HDR.CKH-CHECK-NBR  a        D    CKH-CKH-CHK
-HDR.CKH-CHECK-DT  g        D    CKH-CKH-CHK-HDR.CKH-CHECK-TYP-CD  �  
     U    CKH-CKH-CHK-HDR.CKH-CHECK-AMT  l        D    CKH-CKH-CHK-HDR.
CKH-CHECK-RGSTR-DT  r        D    CKH-CKH-CHK-HDR.CKH-CNCL-IND  �      
  D    CKH-CKH-CHK-HDR.CKH-PRCSD-IND  v        D    CKH-CKH-CHK-HDR.CKH
-RCNCLTN-IND  �        D    CKH-CKH-CHK-HDR.CKH-CHECK-RCNCLTN-DT  �    
    D    CKH-CKH-CHK-HDR.CKH-DISCREPANCY-IND  �        D    CKH-CKH-CHK
-HDR.CKH-CNCL-DT  ]        D    CKH-CKH-CHK-HDR.CKH-STOP-PAID-IND  C   
     D    CKH-CKH-CHK-HDR.CKH-ISSUE-BANK-DT  I        D    CKH-CKH-CHK-
HDR.CKH-ISSU-RGSTR-DT  �        D    CKH-CKH-CHK-HDR.CKH-CHECK-CNCL-CD  
N        U    
CKH-CKH-CHK-HDR.CKH-FK-CKR-CHECK-ISEQ  �        D    CKH
-CKH-CHK-HDR.CKH-CKR-SET-TS  �        D    CKH-CKH-CHK-HDR.CKH-IDX-CHK-DT-
TS  X        D    CKH-CKH-CHK-HDR.CKH-IDX-RC-TS  �        D    CKH-C
KH-CHK-HDR.CKH-IDX-REG-DT-TS  �       D    CKH-CKH-CHK-HDR.CKH-BANK-ACCT-
CD  �       D    CKH-CKH-CHK-HDR.CKH-CHECK-NBR
DBRM              CLOSE S_4158_4159_N     
DBRM              CLOSE S_4158_4159_N     
DBRM              CLOSE S_4033_4062_P     
DBRM        �      CLOSE S_4033_4062_P     
