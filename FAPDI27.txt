DBRM   �APCPSB  FAPDI27 m9�Ny 2                                     1  �NK
                                                                                
DBRM        �     DECLARE MSL_MSG_LINE_V TABLE ( CMT_ID DECIMAL ( 8 ) NOT
NULL WITH DEFAULT , MDRDEST1 CHAR ( 8 ) NOT NULL WITH DEFAULT , MDRDEST2 CHAR (
8 ) NOT NULL WITH DEFAULT , MDROSDSC CHAR ( 2 ) NOT NULL WITH DEFAULT , MDROSRTC
 CHAR ( 2 ) NOT NULL WITH DEFAULT , MDRDSTID CHAR ( 8 ) NOT NULL WITH DEFAULT ,
MDRSEVCD CHAR ( 1 ) NOT NULL WITH DEFAULT , MDRTEXTL DECIMAL ( 4 ) NOT NULL WITH
 DEFAULT , MSG_1 CHAR ( 66 ) NOT NULL WITH DEFAULT , MSG_2 CHAR ( 66 ) NOT NULL
WITH DEFAULT , FK_MSG_MSG_KEY CHAR ( 8 ) NOT NULL , MSG_TS TIMESTAMP NOT NULL )
    
DBRM  	1           	NDECLARE CKR_CHK_RQST_H_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , CHECK_RQST_ISEQ DECIMAL ( 8 ) NOT NULL , ORGN_C
D CHAR ( 4 ) NOT NULL WITH DEFAULT , RQSTR_ID_DGT_ONE CHAR ( 1 ) NOT NULL WITH D
EFAULT , RQSTR_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , RQST_DT DATE NOT N
ULL WITH DEFAULT , SRVCD_DT DATE NOT NULL WITH DEFAULT , ADR_TYP_CD CHAR ( 2 ) N
OT NULL WITH DEFAULT , BANK_ACCT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , RQST_CHEC
K_AMT DECIMAL ( 13 , 2 ) NOT NULL WITH DEFAULT , AP_FDRL_WTHHLD_PCT DECIMAL ( 6
, 3 ) NOT NULL WITH DEFAULT , AP_ST_WTHHLD_PCT DECIMAL ( 6 , 3 ) NOT NULL WITH D
EFAULT , AP_1099_RPRT_ID CHAR ( 9 ) NOT NULL WITH DEFAULT , AP_RSBLE_IND CHAR (
1 ) NOT NULL WITH DEFAULT , TA_ADVNC_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , TA_R
ECN_EVENT_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , TA_RCNCLTN_DT DATE NOT NULL WIT
H DEFAULT , AR_TRNSC_NBR DECIMAL ( 7 ) NOT NULL WITH DEFAULT , ENTPSN_IND CHAR (
 1 ) NOT NULL WITH DEFAULT , ACH_RCVNG_DFI_ID DECIMAL ( 8 ) NOT NULL WITH DEFAUL
T , ACH_CHECK_DGT DECIMAL ( 1 ) NOT NULL WITH DEFAULT , ACH_DFI_ACCT_NBR CHAR (
17 ) NOT NULL WITH DEFAULT , ACH_ACCT_TYP CHAR ( 1 ) NOT NULL WITH DEFAULT , CHK
_CNCL_CD CHAR ( 1 ) NOT NULL WITH DEFAULT , CHK_RQST_DATA CHAR ( 2 ) NOT NULL WI
TH DEFAULT , CHECK_GRPNG_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , VNDR_NAME CHAR (
 35 ) NOT NULL WITH DEFAULT , IDX_RQSTR_TS TIMESTAMP NOT NULL WITH DEFAULT )    
 
DBRM  J     _     �DECLARE S_INDX_RQSTR_N CURSOR FOR SELECT USER_CD , LAST_
ACTVY_DT , UNVRS_CD , CHECK_RQST_ISEQ , ORGN_CD , RQSTR_ID_DGT_ONE , RQSTR_ID_LS
T_NINE , RQST_DT , SRVCD_DT , ADR_TYP_CD , BANK_ACCT_CD , RQST_CHECK_AMT , AP_FD
RL_WTHHLD_PCT , AP_ST_WTHHLD_PCT , AP_1099_RPRT_ID , AP_RSBLE_IND , TA_ADVNC_NBR
 , TA_RECN_EVENT_NBR , TA_RCNCLTN_DT , AR_TRNSC_NBR , ENTPSN_IND , ACH_RCVNG_DFI
_ID , ACH_CHECK_DGT , ACH_DFI_ACCT_NBR , ACH_ACCT_TYP , CHK_CNCL_CD , CHK_RQST_D
ATA , CHECK_GRPNG_IND , VNDR_NAME , IDX_RQSTR_TS FROM CKR_CHK_RQST_H_V WHERE RQS
TR_ID_DGT_ONE = : H AND RQSTR_ID_LST_NINE >= : H AND ( ( RQSTR_ID_DGT_ONE = : H
AND RQSTR_ID_LST_NINE = : H AND SRVCD_DT = : H AND IDX_RQSTR_TS > : H ) OR ( RQS
TR_ID_DGT_ONE = : H AND RQSTR_ID_LST_NINE = : H AND SRVCD_DT > : H ) OR ( RQSTR_
ID_DGT_ONE = : H AND RQSTR_ID_LST_NINE > : H ) ) ORDER BY RQSTR_ID_DGT_ONE ASC ,
 RQSTR_ID_LST_NINE ASC , SRVCD_DT ASC , IDX_RQSTR_TS ASC FOR FETCH ONLY OPTIMIZE
 FOR 1 ROW     � �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQS
TR-01 � ��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 � 
��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 � a�       
D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 � m�       D    DBL
INK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 � ��       D    DBLINK-S-INDX
-RQSTR-KEY-S.DBLINK-S-INDX-RQSTR-04 � I�       D    DBLINK-S-INDX-RQSTR-KEY
-U.DBLINK-S-INDX-RQSTR-01 � V�       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-
S-INDX-RQSTR-02 � 8�       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQS
TR-03 � �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 � 
�       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02
DBRM  N     �     �DECLARE S_INDX_RQSTR_P CURSOR FOR SELECT USER_CD , LAST_
ACTVY_DT , UNVRS_CD , CHECK_RQST_ISEQ , ORGN_CD , RQSTR_ID_DGT_ONE , RQSTR_ID_LS
T_NINE , RQST_DT , SRVCD_DT , ADR_TYP_CD , BANK_ACCT_CD , RQST_CHECK_AMT , AP_FD
RL_WTHHLD_PCT , AP_ST_WTHHLD_PCT , AP_1099_RPRT_ID , AP_RSBLE_IND , TA_ADVNC_NBR
 , TA_RECN_EVENT_NBR , TA_RCNCLTN_DT , AR_TRNSC_NBR , ENTPSN_IND , ACH_RCVNG_DFI
_ID , ACH_CHECK_DGT , ACH_DFI_ACCT_NBR , ACH_ACCT_TYP , CHK_CNCL_CD , CHK_RQST_D
ATA , CHECK_GRPNG_IND , VNDR_NAME , IDX_RQSTR_TS FROM CKR_CHK_RQST_H_V WHERE RQS
TR_ID_DGT_ONE = : H AND RQSTR_ID_LST_NINE <= : H AND ( ( RQSTR_ID_DGT_ONE = : H
AND RQSTR_ID_LST_NINE = : H AND SRVCD_DT = : H AND IDX_RQSTR_TS < : H ) OR ( RQS
TR_ID_DGT_ONE = : H AND RQSTR_ID_LST_NINE = : H AND SRVCD_DT < : H ) OR ( RQSTR_
ID_DGT_ONE = : H AND RQSTR_ID_LST_NINE < : H ) ) ORDER BY RQSTR_ID_DGT_ONE DESC
, RQSTR_ID_LST_NINE DESC , SRVCD_DT DESC , IDX_RQSTR_TS DESC FOR FETCH ONLY OPTI
MIZE FOR 1 ROW     � �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX
-RQSTR-01 � ��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 
� ��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 � a�    
   D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 � m�       D    
DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 � ��       D    DBLINK-S-
INDX-RQSTR-KEY-S.DBLINK-S-INDX-RQSTR-04 � I�       D    DBLINK-S-INDX-RQSTR
-KEY-U.DBLINK-S-INDX-RQSTR-01 � V�       D    DBLINK-S-INDX-RQSTR-KEY-U.DBL
INK-S-INDX-RQSTR-02 � 8�       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX
-RQSTR-03 � �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 
� �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02
DBRM   �     �      /SELECT 1 INTO : H FROM CKR_CHK_RQST_H_V WHERE RQSTR_ID_D
GT_ONE = ' ' AND RQSTR_ID_LST_NINE = : H     �          0    DBLINK-OTHER-
WORK-AREAS.DBLINK-MEMBER-COUNT   ;�       D    CKR-CKR-CHK-RQST-H.CKR-RQSTR
-ID-LST-NINE
DBRM       :      �SELECT MSG_1 , MSG_2 INTO : H , : H FROM MSL_MSG_LINE_V
WHERE FK_MSG_MSG_KEY = : H               �D    MSL-MSL-MSG-LINE.MSL-MSG-1 
          �D    MSL-MSL-MSG-LINE.MSL-MSG-2   &�       D    MSL-MSL-MSG
-LINE.MSL-FK-MSG-MSG-KEY
DBRM       6      OPEN S_INDX_RQSTR_N     �   �       D    DBLINK-S-I
NDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 �   �       D    DBLINK-S-INDX-RQSTR-
KEY-U.DBLINK-S-INDX-RQSTR-02 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLI
NK-S-INDX-RQSTR-01 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-
RQSTR-02 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 �
   �       D    DBLINK-S-INDX-RQSTR-KEY-S.DBLINK-S-INDX-RQSTR-04 �   �     
  D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 �   �       D    
DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 �   �       D    DBLINK-S-I
NDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 �   �       D    DBLINK-S-INDX-RQSTR-
KEY-U.DBLINK-S-INDX-RQSTR-01 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLI
NK-S-INDX-RQSTR-02
DBRM  w     �      �FETCH S_INDX_RQSTR_N INTO : H , : H , : H , : H , : H ,
: H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H         
      D    CKR-CKR-CHK-RQST-H.CKR-USER-CD           	D    CKR-CKR-CHK-RQ
ST-H.CKR-LAST-ACTVY-DT           D    CKR-CKR-CHK-RQST-H.CKR-UNVRS-CD   
        U    CKR-CKR-CHK-RQST-H.CKR-CHECK-RQST-ISEQ           D    CKR
-CKR-CHK-RQST-H.CKR-ORGN-CD           D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-ID
-DGT-ONE           D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-ID-LST-NINE   �    
    D    CKR-CKR-CHK-RQST-H.CKR-RQST-DT   .        D    CKR-CKR-CHK-RQST
-H.CKR-SRVCD-DT   �        D    CKR-CKR-CHK-RQST-H.CKR-ADR-TYP-CD   �    
    D    CKR-CKR-CHK-RQST-H.CKR-BANK-ACCT-CD   )       U    
CKR-CKR-CHK
-RQST-H.CKR-RQST-CHECK-AMT   �       U    CKR-CKR-CHK-RQST-H.CKR-AP-FDRL-W
THHLD-PCT   �       U    CKR-CKR-CHK-RQST-H.CKR-AP-ST-WTHHLD-PCT   ?    
    D    CKR-CKR-CHK-RQST-H.CKR-AP-1099-RPRT-ID   �        D    CKR-CKR-
CHK-RQST-H.CKR-AP-RSBLE-IND   #        D    CKR-CKR-CHK-RQST-H.CKR-TA-ADVNC
-NBR   a        D    CKR-CKR-CHK-RQST-H.CKR-TA-RECN-EVENT-NBR   g        
D    CKR-CKR-CHK-RQST-H.CKR-TA-RCNCLTN-DT   �        U    CKR-CKR-CHK-RQ
ST-H.CKR-AR-TRNSC-NBR   l        D    CKR-CKR-CHK-RQST-H.CKR-ENTPSN-IND  
 r        U    CKR-CKR-CHK-RQST-H.CKR-ACH-RCVNG-DFI-ID   �        U    C
KR-CKR-CHK-RQST-H.CKR-ACH-CHECK-DGT   v        D    CKR-CKR-CHK-RQST-H.CKR-
ACH-DFI-ACCT-NBR   �        D    CKR-CKR-CHK-RQST-H.CKR-ACH-ACCT-TYP   �
       D    CKR-CKR-CHK-RQST-H.CKR-CHK-CNCL-CD   �        D    CKR-CKR-C
HK-RQST-H.CKR-CHK-RQST-DATA   ]        D    CKR-CKR-CHK-RQST-H.CKR-CHECK-GR
PNG-IND   C        D    CKR-CKR-CHK-RQST-H.CKR-VNDR-NAME   I        D  
  CKR-CKR-CHK-RQST-H.CKR-IDX-RQSTR-TS
DBRM       -      OPEN S_INDX_RQSTR_P     �   �       D    DBLINK-S-I
NDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 �   �       D    DBLINK-S-INDX-RQSTR-
KEY-U.DBLINK-S-INDX-RQSTR-02 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLI
NK-S-INDX-RQSTR-01 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-
RQSTR-02 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 �
   �       D    DBLINK-S-INDX-RQSTR-KEY-S.DBLINK-S-INDX-RQSTR-04 �   �     
  D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 �   �       D    
DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 �   �       D    DBLINK-S-I
NDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 �   �       D    DBLINK-S-INDX-RQSTR-
KEY-U.DBLINK-S-INDX-RQSTR-01 �   �       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLI
NK-S-INDX-RQSTR-02
DBRM  w     �      �FETCH S_INDX_RQSTR_P INTO : H , : H , : H , : H , : H ,
: H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H         
      D    CKR-CKR-CHK-RQST-H.CKR-USER-CD           	D    CKR-CKR-CHK-RQ
ST-H.CKR-LAST-ACTVY-DT           D    CKR-CKR-CHK-RQST-H.CKR-UNVRS-CD   
        U    CKR-CKR-CHK-RQST-H.CKR-CHECK-RQST-ISEQ           D    CKR
-CKR-CHK-RQST-H.CKR-ORGN-CD           D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-ID
-DGT-ONE           D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-ID-LST-NINE   �    
    D    CKR-CKR-CHK-RQST-H.CKR-RQST-DT   .        D    CKR-CKR-CHK-RQST
-H.CKR-SRVCD-DT   �        D    CKR-CKR-CHK-RQST-H.CKR-ADR-TYP-CD   �    
    D    CKR-CKR-CHK-RQST-H.CKR-BANK-ACCT-CD   )       U    
CKR-CKR-CHK
-RQST-H.CKR-RQST-CHECK-AMT   �       U    CKR-CKR-CHK-RQST-H.CKR-AP-FDRL-W
THHLD-PCT   �       U    CKR-CKR-CHK-RQST-H.CKR-AP-ST-WTHHLD-PCT   ?    
    D    CKR-CKR-CHK-RQST-H.CKR-AP-1099-RPRT-ID   �        D    CKR-CKR-
CHK-RQST-H.CKR-AP-RSBLE-IND   #        D    CKR-CKR-CHK-RQST-H.CKR-TA-ADVNC
-NBR   a        D    CKR-CKR-CHK-RQST-H.CKR-TA-RECN-EVENT-NBR   g        
D    CKR-CKR-CHK-RQST-H.CKR-TA-RCNCLTN-DT   �        U    CKR-CKR-CHK-RQ
ST-H.CKR-AR-TRNSC-NBR   l        D    CKR-CKR-CHK-RQST-H.CKR-ENTPSN-IND  
 r        U    CKR-CKR-CHK-RQST-H.CKR-ACH-RCVNG-DFI-ID   �        U    C
KR-CKR-CHK-RQST-H.CKR-ACH-CHECK-DGT   v        D    CKR-CKR-CHK-RQST-H.CKR-
ACH-DFI-ACCT-NBR   �        D    CKR-CKR-CHK-RQST-H.CKR-ACH-ACCT-TYP   �
       D    CKR-CKR-CHK-RQST-H.CKR-CHK-CNCL-CD   �        D    CKR-CKR-C
HK-RQST-H.CKR-CHK-RQST-DATA   ]        D    CKR-CKR-CHK-RQST-H.CKR-CHECK-GR
PNG-IND   C        D    CKR-CKR-CHK-RQST-H.CKR-VNDR-NAME   I        D  
  CKR-CKR-CHK-RQST-H.CKR-IDX-RQSTR-TS
DBRM   	    G     eSELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , CHECK_RQST_I
SEQ , ORGN_CD , RQSTR_ID_DGT_ONE , RQSTR_ID_LST_NINE , RQST_DT , SRVCD_DT , ADR_
TYP_CD , BANK_ACCT_CD , RQST_CHECK_AMT , AP_FDRL_WTHHLD_PCT , AP_ST_WTHHLD_PCT ,
 AP_1099_RPRT_ID , AP_RSBLE_IND , TA_ADVNC_NBR , TA_RECN_EVENT_NBR , TA_RCNCLTN_
DT , AR_TRNSC_NBR , ENTPSN_IND , ACH_RCVNG_DFI_ID , ACH_CHECK_DGT , ACH_DFI_ACCT
_NBR , ACH_ACCT_TYP , CHK_CNCL_CD , CHK_RQST_DATA , CHECK_GRPNG_IND , VNDR_NAME
, IDX_RQSTR_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
: H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H FROM CKR_CHK_RQST_H_V WHERE RQSTR_ID_DGT_O
NE = : H AND RQSTR_ID_LST_NINE = : H AND SRVCD_DT = : H AND IDX_RQSTR_TS = ( SEL
ECT MIN ( IDX_RQSTR_TS ) FROM CKR_CHK_RQST_H_V WHERE RQSTR_ID_DGT_ONE = : H AND
RQSTR_ID_LST_NINE = : H AND SRVCD_DT = : H )      �        D    CKR-CKR-C
HK-RQST-H.CKR-USER-CD  T        	D    CKR-CKR-CHK-RQST-H.CKR-LAST-ACTVY-DT 
 Z        D    CKR-CKR-CHK-RQST-H.CKR-UNVRS-CD  �        U    CKR-CK
R-CHK-RQST-H.CKR-CHECK-RQST-ISEQ  5        D    CKR-CKR-CHK-RQST-H.CKR-ORG
N-CD  �        D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-ID-DGT-ONE          
D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-ID-LST-NINE          D    CKR-CKR-CHK
-RQST-H.CKR-RQST-DT          D    CKR-CKR-CHK-RQST-H.CKR-SRVCD-DT    
      D    CKR-CKR-CHK-RQST-H.CKR-ADR-TYP-CD          D    CKR-CKR-CHK
-RQST-H.CKR-BANK-ACCT-CD         U    
CKR-CKR-CHK-RQST-H.CKR-RQST-CHECK-
AMT  
       U    CKR-CKR-CHK-RQST-H.CKR-AP-FDRL-WTHHLD-PCT        
 U    CKR-CKR-CHK-RQST-H.CKR-AP-ST-WTHHLD-PCT          D    CKR-CKR-CHK
-RQST-H.CKR-AP-1099-RPRT-ID          D    CKR-CKR-CHK-RQST-H.CKR-AP-RSBLE
-IND          D    CKR-CKR-CHK-RQST-H.CKR-TA-ADVNC-NBR  �        D  
  CKR-CKR-CHK-RQST-H.CKR-TA-RECN-EVENT-NBR  �        D    CKR-CKR-CHK-RQS
T-H.CKR-TA-RCNCLTN-DT  |        U    CKR-CKR-CHK-RQST-H.CKR-AR-TRNSC-NBR 
 �        D    CKR-CKR-CHK-RQST-H.CKR-ENTPSN-IND  $        U    CKR-C
KR-CHK-RQST-H.CKR-ACH-RCVNG-DFI-ID  /        U    CKR-CKR-CHK-RQST-H.CKR-A
CH-CHECK-DGT  �        D    CKR-CKR-CHK-RQST-H.CKR-ACH-DFI-ACCT-NBR  _
       D    CKR-CKR-CHK-RQST-H.CKR-ACH-ACCT-TYP  �        D    CKR-CKR-
CHK-RQST-H.CKR-CHK-CNCL-CD  `        D    CKR-CKR-CHK-RQST-H.CKR-CHK-RQST-
DATA  "        D    CKR-CKR-CHK-RQST-H.CKR-CHECK-GRPNG-IND  e        
D    CKR-CKR-CHK-RQST-H.CKR-VNDR-NAME  �        D    CKR-CKR-CHK-RQST-H.C
KR-IDX-RQSTR-TS � ��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQS
TR-01 � ��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 � 
��       D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03 � ��       
D    DBLINK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-01 � _�       D    DBL
INK-S-INDX-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-02 � ��       D    DBLINK-S-INDX
-RQSTR-KEY-U.DBLINK-S-INDX-RQSTR-03
DBRM  O          {SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , CHECK_RQST_I
SEQ , ORGN_CD , RQSTR_ID_DGT_ONE , RQSTR_ID_LST_NINE , RQST_DT , SRVCD_DT , ADR_
TYP_CD , BANK_ACCT_CD , RQST_CHECK_AMT , AP_FDRL_WTHHLD_PCT , AP_ST_WTHHLD_PCT ,
 AP_1099_RPRT_ID , AP_RSBLE_IND , TA_ADVNC_NBR , TA_RECN_EVENT_NBR , TA_RCNCLTN_
DT , AR_TRNSC_NBR , ENTPSN_IND , ACH_RCVNG_DFI_ID , ACH_CHECK_DGT , ACH_DFI_ACCT
_NBR , ACH_ACCT_TYP , CHK_CNCL_CD , CHK_RQST_DATA , CHECK_GRPNG_IND , VNDR_NAME
, IDX_RQSTR_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
: H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H FROM CKR_CHK_RQST_H_V WHERE CHECK_RQST_ISE
Q = : H      �        D    CKR-CKR-CHK-RQST-H.CKR-USER-CD  T        	
D    CKR-CKR-CHK-RQST-H.CKR-LAST-ACTVY-DT  Z        D    CKR-CKR-CHK-RQST
-H.CKR-UNVRS-CD  �        U    CKR-CKR-CHK-RQST-H.CKR-CHECK-RQST-ISEQ  
5        D    CKR-CKR-CHK-RQST-H.CKR-ORGN-CD  �        D    CKR-CKR-CHK
-RQST-H.CKR-RQSTR-ID-DGT-ONE          D    CKR-CKR-CHK-RQST-H.CKR-RQSTR-I
D-LST-NINE          D    CKR-CKR-CHK-RQST-H.CKR-RQST-DT          D 
   CKR-CKR-CHK-RQST-H.CKR-SRVCD-DT          D    CKR-CKR-CHK-RQST-H.CKR-
ADR-TYP-CD          D    CKR-CKR-CHK-RQST-H.CKR-BANK-ACCT-CD       
  U    
CKR-CKR-CHK-RQST-H.CKR-RQST-CHECK-AMT  
       U    CKR-CKR-CHK-
RQST-H.CKR-AP-FDRL-WTHHLD-PCT         U    CKR-CKR-CHK-RQST-H.CKR-AP-ST-
WTHHLD-PCT          D    CKR-CKR-CHK-RQST-H.CKR-AP-1099-RPRT-ID      
    D    CKR-CKR-CHK-RQST-H.CKR-AP-RSBLE-IND          D    CKR-CKR-CHK
-RQST-H.CKR-TA-ADVNC-NBR  �        D    CKR-CKR-CHK-RQST-H.CKR-TA-RECN-EVE
NT-NBR  �        D    CKR-CKR-CHK-RQST-H.CKR-TA-RCNCLTN-DT  |        
U    CKR-CKR-CHK-RQST-H.CKR-AR-TRNSC-NBR  �        D    CKR-CKR-CHK-RQST-
H.CKR-ENTPSN-IND  $        U    CKR-CKR-CHK-RQST-H.CKR-ACH-RCVNG-DFI-ID  
/        U    CKR-CKR-CHK-RQST-H.CKR-ACH-CHECK-DGT  �        D    CKR
-CKR-CHK-RQST-H.CKR-ACH-DFI-ACCT-NBR  _        D    CKR-CKR-CHK-RQST-H.CKR
-ACH-ACCT-TYP  �        D    CKR-CKR-CHK-RQST-H.CKR-CHK-CNCL-CD  `     
   D    CKR-CKR-CHK-RQST-H.CKR-CHK-RQST-DATA  "        D    CKR-CKR-CHK
-RQST-H.CKR-CHECK-GRPNG-IND  e        D    CKR-CKR-CHK-RQST-H.CKR-VNDR-NAM
E  �        D    CKR-CKR-CHK-RQST-H.CKR-IDX-RQSTR-TS  ]�       U    
CKR-CKR-CHK-RQST-H.CKR-CHECK-RQST-ISEQ
DBRM              
CLOSE S_INDX_RQSTR_N     
DBRM              
CLOSE S_INDX_RQSTR_N     
DBRM              
CLOSE S_INDX_RQSTR_P     
DBRM              
CLOSE S_INDX_RQSTR_P     
