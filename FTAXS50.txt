DBRM   �APCPSB  FTAXS50 ���J 2                                     1  �NK
                                                                                
DBRM             DECLARE MSL_MSG_LINE_V TABLE ( CMT_ID DECIMAL ( 8 ) NOT
NULL WITH DEFAULT , MDRDEST1 CHAR ( 8 ) NOT NULL WITH DEFAULT , MDRDEST2 CHAR (
8 ) NOT NULL WITH DEFAULT , MDROSDSC CHAR ( 2 ) NOT NULL WITH DEFAULT , MDROSRTC
 CHAR ( 2 ) NOT NULL WITH DEFAULT , MDRDSTID CHAR ( 8 ) NOT NULL WITH DEFAULT ,
MDRSEVCD CHAR ( 1 ) NOT NULL WITH DEFAULT , MDRTEXTL DECIMAL ( 4 ) NOT NULL WITH
 DEFAULT , MSG_1 CHAR ( 66 ) NOT NULL WITH DEFAULT , MSG_2 CHAR ( 66 ) NOT NULL
WITH DEFAULT , FK_MSG_MSG_KEY CHAR ( 8 ) NOT NULL , MSG_TS TIMESTAMP NOT NULL )
    
DBRM  �      �     �DECLARE TVL_TRVL_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL W
ITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR ( 2
 ) NOT NULL WITH DEFAULT , IREF_ID DECIMAL ( 7 ) NOT NULL , ENTPSN_IND CHAR ( 1
) NOT NULL , TRVL_ACCT_ONE CHAR ( 1 ) NOT NULL WITH DEFAULT , TRVL_ACCT_NINE CHA
R ( 9 ) NOT NULL WITH DEFAULT , EMPLY_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , STA
RT_DT DATE NOT NULL WITH DEFAULT , END_DT DATE NOT NULL WITH DEFAULT , ADR_TYP_C
D CHAR ( 2 ) NOT NULL WITH DEFAULT , CRDT_CARD_NBR CHAR ( 18 ) NOT NULL WITH DEF
AULT , CRDT_CARD_TYP CHAR ( 4 ) NOT NULL WITH DEFAULT , EXPRN_DT DECIMAL ( 4 ) N
OT NULL WITH DEFAULT , ASSOC_EVNT_PRGD_DT DATE NOT NULL WITH DEFAULT )     
DBRM        "     DECLARE UNV_UNVRS_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL
WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR (
2 ) NOT NULL , FICE_CD DECIMAL ( 6 ) NOT NULL WITH DEFAULT , SHORT_NAME CHAR ( 1
0 ) NOT NULL WITH DEFAULT , FULL_NAME CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_A
DR_1 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_2 CHAR ( 35 ) NOT NULL WITH DE
FAULT , LINE_ADR_3 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_4 CHAR ( 35 ) NO
T NULL WITH DEFAULT , TLPHN_ID CHAR ( 14 ) NOT NULL WITH DEFAULT , CITY_NAME CHA
R ( 18 ) NOT NULL WITH DEFAULT , STATE_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ZIP
_CD CHAR ( 10 ) NOT NULL WITH DEFAULT , CNTRY_CD CHAR ( 2 ) NOT NULL WITH DEFAUL
T , FK_SYS_SYS_CD CHAR ( 1 ) NOT NULL WITH DEFAULT , SYS_SET_TS TIMESTAMP NOT NU
LL WITH DEFAULT )     
DBRM  �      �     �DECLARE PRS_PRSN_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL ,
 LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , FK_UNV_UNVRS_CD CHAR ( 2 ) NOT NULL , IREF_
ID DECIMAL ( 7 , 0 ) NOT NULL , SSN_DGT_ONE CHAR ( 1 ) NOT NULL , SSN_LST_NINE C
HAR ( 9 ) NOT NULL , NAME_KEY CHAR ( 35 ) NOT NULL , NAME_SLTN_DESC CHAR ( 4 ) N
OT NULL , PRSN_FULL_NAME CHAR ( 55 ) NOT NULL , NAME_SFX_DESC CHAR ( 4 ) NOT NUL
L , BIRTH_DT DATE NOT NULL , GNDR_FLAG CHAR ( 1 ) NOT NULL , DCSD_FLAG CHAR ( 1
) NOT NULL , DCSD_DT DATE NOT NULL , ALT_ID CHAR ( 9 ) NOT NULL , PAN_NBR CHAR (
 4 ) NOT NULL , PAN_FLAG CHAR ( 1 ) NOT NULL , PAN_DT DATE NOT NULL , MRTL_CD CH
AR ( 1 ) NOT NULL , RLGN_CD CHAR ( 4 ) NOT NULL , ETHNC_CD CHAR ( 2 ) NOT NULL ,
 ARCHVD_DT DATE NOT NULL , UNV_SETF CHAR ( 1 ) NOT NULL , UNV_SET_TS TIMESTAMP N
OT NULL , IDX_SSN_TS TIMESTAMP NOT NULL , EMP_ID CHAR ( 9 ) NOT NULL , EMP_STATU
S CHAR ( 1 ) NOT NULL )     
DBRM  x           �DECLARE ADT_ADR_TYP_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CHAR ( 2 ) NOT NULL , DSPLY_PRTY_SEQ DE
CIMAL ( 2 ) NOT NULL WITH DEFAULT , SHORT_DESC CHAR ( 10 ) NOT NULL WITH DEFAULT
 , LONG_DESC CHAR ( 30 ) NOT NULL WITH DEFAULT , RCRD_USED_FLAG CHAR ( 1 ) NOT N
ULL WITH DEFAULT )     
DBRM  �           �DECLARE PRA_PRSN_ADR_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CH
AR ( 2 ) NOT NULL , END_DT DATE NOT NULL , START_DT DATE NOT NULL WITH DEFAULT ,
 LINE_ADR_1 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_2 CHAR ( 35 ) NOT NULL
WITH DEFAULT , LINE_ADR_3 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_4 CHAR (
35 ) NOT NULL WITH DEFAULT , CITY_NAME CHAR ( 18 ) NOT NULL WITH DEFAULT , TLPHN
_ID CHAR ( 14 ) NOT NULL WITH DEFAULT , CNTY_CD CHAR ( 4 ) NOT NULL WITH DEFAULT
 , STATE_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ZIP_CD CHAR ( 10 ) NOT NULL WITH
DEFAULT , FK_ZIP_CNTRY_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , LINE_ADR_1_UC CHAR
( 35 ) NOT NULL WITH DEFAULT , FAX_TLPHN_ID CHAR ( 10 ) NOT NULL WITH DEFAULT ,
EMADR_LINE CHAR ( 70 ) NOT NULL WITH DEFAULT , FAX_STOP CHAR ( 1 ) NOT NULL WITH
 DEFAULT , FK_PRS_IREF_ID DECIMAL ( 7 ) NOT NULL , ZIP_SETF CHAR ( 1 ) NOT NULL
WITH DEFAULT , FK_ZIP_ZIP_CD CHAR ( 10 ) NOT NULL WITH DEFAULT , ZIP_SET_TS TIME
STAMP NOT NULL WITH DEFAULT , IDX_PRSN_ADR1_TS TIMESTAMP NOT NULL WITH DEFAULT )
     
DBRM  �      ^     DECLARE CTY_COUNTRY_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , FK_UNV_UNVRS_C
D CHAR ( 2 ) NOT NULL WITH DEFAULT , CNTRY_CD CHAR ( 2 ) NOT NULL , FULL_NAME CH
AR ( 35 ) NOT NULL WITH DEFAULT , RCRD_USED_FLAG CHAR ( 1 ) NOT NULL WITH DEFAUL
T )     
DBRM  .      `     DECLARE PPI_PRSN_PRVID_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , PRSN_ID_DGT_ONE CHAR ( 1 ) NOT NULL WITH DEFAUL
T , PRSN_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , CHANGE_DT DATE NOT NULL
, FK_PRS_IREF_ID DECIMAL ( 7 ) NOT NULL , PRS_SET_TS TIMESTAMP NOT NULL , FK_PCH
_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK_PCH_CHG_DT DATE NOT NULL WITH
DEFAULT , FK_PCH_SET_TS TIMESTAMP NOT NULL WITH DEFAULT , PCH_SET_TS TIMESTAMP N
OT NULL WITH DEFAULT )     
DBRM  �      �     >DECLARE ENA_ENTY_ADR_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CH
AR ( 2 ) NOT NULL , END_DT DATE NOT NULL , START_DT DATE NOT NULL WITH DEFAULT ,
 LINE_ADR_1 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_2 CHAR ( 35 ) NOT NULL
WITH DEFAULT , LINE_ADR_3 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_4 CHAR (
35 ) NOT NULL WITH DEFAULT , CITY_NAME CHAR ( 18 ) NOT NULL WITH DEFAULT , TLPHN
_ID CHAR ( 14 ) NOT NULL WITH DEFAULT , CNTY_CD CHAR ( 4 ) NOT NULL WITH DEFAULT
 , STATE_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ZIP_CD CHAR ( 10 ) NOT NULL WITH
DEFAULT , FK_ZIP_CNTRY_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , LINE_ADR_1_UC CHAR
( 35 ) NOT NULL WITH DEFAULT , FAX_TLPHN_ID CHAR ( 10 ) NOT NULL WITH DEFAULT ,
EMADR_LINE CHAR ( 70 ) NOT NULL WITH DEFAULT , FAX_STOP CHAR ( 1 ) NOT NULL WITH
 DEFAULT , ZIP_SETF CHAR ( 1 ) NOT NULL WITH DEFAULT , FK_ZIP_ZIP_CD CHAR ( 10 )
 NOT NULL WITH DEFAULT , ZIP_SET_TS TIMESTAMP NOT NULL WITH DEFAULT , FK_ENT_ENT
Y_ONE CHAR ( 1 ) NOT NULL , FK_ENT_ENTY_NINE CHAR ( 9 ) NOT NULL , IDX_ENTY_ADR1
_TS TIMESTAMP NOT NULL WITH DEFAULT )     
DBRM  b      �     �DECLARE ENI_ENTY_ID_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , ENTY_ID_DGT_ONE CHAR ( 1 ) NOT NULL WITH DEFAULT ,
 ENTY_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , FK2_ECG_CHG_DT DATE NOT NUL
L , FK1_ENT_ENTY_ONE CHAR ( 1 ) NOT NULL , FK1_ENT_ENTY_NINE CHAR ( 9 ) NOT NULL
 , ENT_SET_TS TIMESTAMP NOT NULL , FK2_ECG_ENTY_ONE CHAR ( 1 ) NOT NULL WITH DEF
AULT , FK2_ECG_ENTY_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , FK2_ENT_ECG_SET_TS T
IMESTAMP NOT NULL WITH DEFAULT , ECG_SET_TS TIMESTAMP NOT NULL WITH DEFAULT )   
  
DBRM  B           wDECLARE ENT_ENTY_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL ,
 LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , UNVRS_CD CHAR ( 2 ) NOT NULL , ENTY_ID_DGT_
ONE CHAR ( 1 ) NOT NULL , ENTY_ID_LST_NINE CHAR ( 9 ) NOT NULL , NAME_KEY CHAR (
 35 ) NOT NULL , ENTY_FULL_NAME CHAR ( 55 ) NOT NULL , IREF_ID DECIMAL ( 7 , 0 )
 NOT NULL , PAN_NBR CHAR ( 4 ) NOT NULL , PAN_FLAG CHAR ( 1 ) NOT NULL , PAN_DT
DATE NOT NULL , FEIN_ID CHAR ( 9 ) NOT NULL )     
DBRM  Y     �     �DECLARE S_INDX_TRVL_N CURSOR WITH HOLD FOR SELECT USER_C
D , LAST_ACTVY_DT , UNVRS_CD , IREF_ID , ENTPSN_IND , TRVL_ACCT_ONE , TRVL_ACCT_
NINE , EMPLY_IND , START_DT , END_DT , ADR_TYP_CD , CRDT_CARD_NBR , CRDT_CARD_TY
P , EXPRN_DT , ASSOC_EVNT_PRGD_DT FROM TVL_TRVL_V WHERE TRVL_ACCT_ONE >= : H AND
 ( ( TRVL_ACCT_ONE = : H AND TRVL_ACCT_NINE > : H ) OR ( TRVL_ACCT_ONE > : H ) )
 ORDER BY TRVL_ACCT_ONE ASC , TRVL_ACCT_NINE ASC OPTIMIZE FOR 1 ROW     � � 
      D    DBLINK-S-INDX-TRVL-KEY-U.DBLINK-S-INDX-TRVL-01 � �       D   
 DBLINK-S-INDX-TRVL-KEY-U.DBLINK-S-INDX-TRVL-01 � ��       D    DBLINK-S-I
NDX-TRVL-KEY-U.DBLINK-S-INDX-TRVL-02 � ��       D    DBLINK-S-INDX-TRVL-KEY
-U.DBLINK-S-INDX-TRVL-01
DBRM  �     �     DECLARE S_6117_6139_P CURSOR WITH HOLD FOR SELECT USER_C
D , LAST_ACTVY_DT , ADR_TYP_CD , END_DT , START_DT , LINE_ADR_1 , LINE_ADR_2 , L
INE_ADR_3 , LINE_ADR_4 , CITY_NAME , TLPHN_ID , CNTY_CD , STATE_CD , ZIP_CD , FK
_ZIP_CNTRY_CD , LINE_ADR_1_UC , FAX_TLPHN_ID , EMADR_LINE , FAX_STOP , FK_PRS_IR
EF_ID , ZIP_SETF , FK_ZIP_ZIP_CD , ZIP_SET_TS , IDX_PRSN_ADR1_TS FROM PRA_PRSN_A
DR_V WHERE FK_PRS_IREF_ID = : H AND ADR_TYP_CD <= : H AND ( ( ADR_TYP_CD = : H A
ND END_DT < : H ) OR ( ADR_TYP_CD < : H ) ) ORDER BY ADR_TYP_CD DESC , END_DT DE
SC OPTIMIZE FOR 1 ROW  	   � n�       U    DBLINK-S-6117-6139-KEY-O.DBLINK-
S-6117-6139-01 � ��       D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-
02 � D�       D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-02 � N�   
    D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-03 � ��       D    
DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-02
DBRM       �     ;DECLARE S_6311_6185_P CURSOR WITH HOLD FOR SELECT USER_C
D , LAST_ACTVY_DT , ADR_TYP_CD , END_DT , START_DT , LINE_ADR_1 , LINE_ADR_2 , L
INE_ADR_3 , LINE_ADR_4 , CITY_NAME , TLPHN_ID , CNTY_CD , STATE_CD , ZIP_CD , FK
_ZIP_CNTRY_CD , LINE_ADR_1_UC , FAX_TLPHN_ID , EMADR_LINE , FAX_STOP , ZIP_SETF
, FK_ZIP_ZIP_CD , ZIP_SET_TS , FK_ENT_ENTY_ONE , FK_ENT_ENTY_NINE , IDX_ENTY_ADR
1_TS FROM ENA_ENTY_ADR_V WHERE FK_ENT_ENTY_ONE = : H AND FK_ENT_ENTY_NINE = : H
AND ADR_TYP_CD <= : H AND ( ( ADR_TYP_CD = : H AND END_DT < : H ) OR ( ADR_TYP_C
D < : H ) ) ORDER BY ADR_TYP_CD DESC , END_DT DESC OPTIMIZE FOR 1 ROW     � �
�       D    DBLINK-S-6311-6185-KEY-O.DBLINK-S-6311-6185-01 � E�       D 
   DBLINK-S-6311-6185-KEY-O.DBLINK-S-6311-6185-02 � ��       D    DBLINK-S
-6311-6185-KEY-U.DBLINK-S-6311-6185-03 � 4�       D    DBLINK-S-6311-6185-K
EY-U.DBLINK-S-6311-6185-03 � 	�       D    DBLINK-S-6311-6185-KEY-U.DBLINK-
S-6311-6185-04 � �       D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-6311-6185-
03
DBRM       �      OPEN S_INDX_TRVL_N     �   �       D    DBLINK-S-IN
DX-TRVL-KEY-U.DBLINK-S-INDX-TRVL-01 �   �       D    DBLINK-S-INDX-TRVL-KEY-
U.DBLINK-S-INDX-TRVL-01 �   �       D    DBLINK-S-INDX-TRVL-KEY-U.DBLINK-S-I
NDX-TRVL-02 �   �       D    DBLINK-S-INDX-TRVL-KEY-U.DBLINK-S-INDX-TRVL-01
DBRM  �     E      �FETCH S_INDX_TRVL_N INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H               D  
  TVL-TVL-TRVL.TVL-USER-CD           	D    TVL-TVL-TRVL.TVL-LAST-ACTVY-DT 
          D    TVL-TVL-TRVL.TVL-UNVRS-CD           U    TVL-TVL-TRVL
.TVL-IREF-ID           D    TVL-TVL-TRVL.TVL-ENTPSN-IND           D  
  TVL-TVL-TRVL.TVL-TRVL-ACCT-ONE           D    TVL-TVL-TRVL.TVL-TRVL-ACC
T-NINE   �        D    TVL-TVL-TRVL.TVL-EMPLY-IND   �        D    TVL-
TVL-TRVL.TVL-START-DT   &        D    TVL-TVL-TRVL.TVL-END-DT   �        
D    TVL-TVL-TRVL.TVL-ADR-TYP-CD   *        D    TVL-TVL-TRVL.TVL-CRDT-C
ARD-NBR   �        D    TVL-TVL-TRVL.TVL-CRDT-CARD-TYP   �        U    
TVL-TVL-TRVL.TVL-EXPRN-DT   >        D    TVL-TVL-TRVL.TVL-ASSOC-EVNT-PRGD
-DT
DBRM  c           OPEN S_6117_6139_P  	   �   �       U    DBLINK-S-61
17-6139-KEY-O.DBLINK-S-6117-6139-01 �   �       D    DBLINK-S-6117-6139-KEY-
U.DBLINK-S-6117-6139-02 �   �       D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6
117-6139-02 �   �       D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-03 
�   �       D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-02
DBRM  	�           xFETCH S_6117_6139_P INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H               D    PRA-PRA-PRSN-ADR.PRA-US
ER-CD           	D    PRA-PRA-PRSN-ADR.PRA-LAST-ACTVY-DT           D  
  PRA-PRA-PRSN-ADR.PRA-ADR-TYP-CD           D    PRA-PRA-PRSN-ADR.PRA-END
-DT           D    PRA-PRA-PRSN-ADR.PRA-START-DT           D    PRA-
PRA-PRSN-ADR.PRA-LINE-ADR-1           D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-2
   �        D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-3   �        D    PRA-P
RA-PRSN-ADR.PRA-LINE-ADR-4   &        D    PRA-PRA-PRSN-ADR.PRA-CITY-NAME 
  �        D    PRA-PRA-PRSN-ADR.PRA-TLPHN-ID   *        D    PRA-PRA-P
RSN-ADR.PRA-CNTY-CD   �        D    PRA-PRA-PRSN-ADR.PRA-STATE-CD   �    
    D    PRA-PRA-PRSN-ADR.PRA-ZIP-CD   >        D    PRA-PRA-PRSN-ADR.PR
A-FK-ZIP-CNTRY-CD   �        D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-1-UC   :
       D    PRA-PRA-PRSN-ADR.PRA-FAX-TLPHN-ID   �        �D    PRA-PRA-PR
SN-ADR.PRA-EMADR-LINE   f        D    PRA-PRA-PRSN-ADR.PRA-FAX-STOP   �  
      U    PRA-PRA-PRSN-ADR.PRA-FK-PRS-IREF-ID   k        D    PRA-PRA-P
RSN-ADR.PRA-ZIP-SETF   q        D    PRA-PRA-PRSN-ADR.PRA-FK-ZIP-ZIP-CD  
 �        D    PRA-PRA-PRSN-ADR.PRA-ZIP-SET-TS   u        D    
PRA-PRA-P
RSN-ADR.PRA-IDX-PRSN-ADR1-TS
DBRM  G     �      OPEN S_6311_6185_P     �   �       D    DBLINK-S-63
11-6185-KEY-O.DBLINK-S-6311-6185-01 �   �       D    DBLINK-S-6311-6185-KEY-
O.DBLINK-S-6311-6185-02 �   �       D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-6
311-6185-03 �   �       D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-6311-6185-03 
�   �       D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-6311-6185-04 �   �      
 D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-6311-6185-03
DBRM  	�     #      [FETCH S_6311_6185_P INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H               D    ENA-ENA-ENTY-ADR.
ENA-USER-CD           	D    ENA-ENA-ENTY-ADR.ENA-LAST-ACTVY-DT          
 D    ENA-ENA-ENTY-ADR.ENA-ADR-TYP-CD           D    ENA-ENA-ENTY-ADR.E
NA-END-DT           D    ENA-ENA-ENTY-ADR.ENA-START-DT           D   
 ENA-ENA-ENTY-ADR.ENA-LINE-ADR-1           D    ENA-ENA-ENTY-ADR.ENA-LINE
-ADR-2   �        D    ENA-ENA-ENTY-ADR.ENA-LINE-ADR-3   �        D    
ENA-ENA-ENTY-ADR.ENA-LINE-ADR-4   &        D    ENA-ENA-ENTY-ADR.ENA-CITY-
NAME   �        D    ENA-ENA-ENTY-ADR.ENA-TLPHN-ID   *        D    ENA
-ENA-ENTY-ADR.ENA-CNTY-CD   �        D    ENA-ENA-ENTY-ADR.ENA-STATE-CD  
 �        D    ENA-ENA-ENTY-ADR.ENA-ZIP-CD   >        D    ENA-ENA-ENTY-
ADR.ENA-FK-ZIP-CNTRY-CD   �        D    ENA-ENA-ENTY-ADR.ENA-LINE-ADR-1-UC 
  :        D    ENA-ENA-ENTY-ADR.ENA-FAX-TLPHN-ID   �        �D    ENA-
ENA-ENTY-ADR.ENA-EMADR-LINE   f        D    ENA-ENA-ENTY-ADR.ENA-FAX-STOP 
  �        D    ENA-ENA-ENTY-ADR.ENA-ZIP-SETF   k        D    ENA-ENA-E
NTY-ADR.ENA-FK-ZIP-ZIP-CD   q        D    ENA-ENA-ENTY-ADR.ENA-ZIP-SET-TS 
  �        D    ENA-ENA-ENTY-ADR.ENA-FK-ENT-ENTY-ONE   u        D    
EN
A-ENA-ENTY-ADR.ENA-FK-ENT-ENTY-NINE   �        D    
ENA-ENA-ENTY-ADR.ENA-ID
X-ENTY-ADR1-TS
DBRM       �     SELECT USER_CD , LAST_ACTVY_DT , ADR_TYP_CD , END_DT , S
TART_DT , LINE_ADR_1 , LINE_ADR_2 , LINE_ADR_3 , LINE_ADR_4 , CITY_NAME , TLPHN_
ID , CNTY_CD , STATE_CD , ZIP_CD , FK_ZIP_CNTRY_CD , LINE_ADR_1_UC , FAX_TLPHN_I
D , EMADR_LINE , FAX_STOP , FK_PRS_IREF_ID , ZIP_SETF , FK_ZIP_ZIP_CD , ZIP_SET_
TS , IDX_PRSN_ADR1_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H FROM PRA_PRSN_ADR_V WHERE FK_PRS_IREF_ID = : H AND ADR_TYP_CD = : H AND
 END_DT = : H      �        D    PRA-PRA-PRSN-ADR.PRA-USER-CD  �      
  	D    PRA-PRA-PRSN-ADR.PRA-LAST-ACTVY-DT  &        D    PRA-PRA-PRSN-A
DR.PRA-ADR-TYP-CD  �        D    PRA-PRA-PRSN-ADR.PRA-END-DT  *        
D    PRA-PRA-PRSN-ADR.PRA-START-DT  �        D    PRA-PRA-PRSN-ADR.PRA-
LINE-ADR-1  �        D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-2  >        D
    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-3  �        D    PRA-PRA-PRSN-ADR.PRA-L
INE-ADR-4  :        D    PRA-PRA-PRSN-ADR.PRA-CITY-NAME  �        D  
  PRA-PRA-PRSN-ADR.PRA-TLPHN-ID  f        D    PRA-PRA-PRSN-ADR.PRA-CNTY-
CD  �        D    PRA-PRA-PRSN-ADR.PRA-STATE-CD  k        D    PRA-P
RA-PRSN-ADR.PRA-ZIP-CD  q        D    PRA-PRA-PRSN-ADR.PRA-FK-ZIP-CNTRY-CD
  �        D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-1-UC  u        D    PR
A-PRA-PRSN-ADR.PRA-FAX-TLPHN-ID  �        �D    PRA-PRA-PRSN-ADR.PRA-EMADR-
LINE  �        D    PRA-PRA-PRSN-ADR.PRA-FAX-STOP  �        U    PRA
-PRA-PRSN-ADR.PRA-FK-PRS-IREF-ID  �        D    PRA-PRA-PRSN-ADR.PRA-ZIP-S
ETF  B        D    PRA-PRA-PRSN-ADR.PRA-FK-ZIP-ZIP-CD  H        D    
PRA-PRA-PRSN-ADR.PRA-ZIP-SET-TS  �        D    
PRA-PRA-PRSN-ADR.PRA-IDX-P
RSN-ADR1-TS � ��       U    DBLINK-S-6117-6139-KEY-O.DBLINK-S-6117-6139-01 
� �       D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-02 � �      
 D    DBLINK-S-6117-6139-KEY-U.DBLINK-S-6117-6139-03
DBRM  ] 	         *SELECT USER_CD , LAST_ACTVY_DT , ADR_TYP_CD , END_DT , S
TART_DT , LINE_ADR_1 , LINE_ADR_2 , LINE_ADR_3 , LINE_ADR_4 , CITY_NAME , TLPHN_
ID , CNTY_CD , STATE_CD , ZIP_CD , FK_ZIP_CNTRY_CD , LINE_ADR_1_UC , FAX_TLPHN_I
D , EMADR_LINE , FAX_STOP , ZIP_SETF , FK_ZIP_ZIP_CD , ZIP_SET_TS , FK_ENT_ENTY_
ONE , FK_ENT_ENTY_NINE , IDX_ENTY_ADR1_TS INTO : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H FROM ENA_ENTY_ADR_V WHERE FK_ENT_ENTY_ONE = :
 H AND FK_ENT_ENTY_NINE = : H AND ADR_TYP_CD = : H AND END_DT = : H      �  
      D    ENA-ENA-ENTY-ADR.ENA-USER-CD  ;        	D    ENA-ENA-ENTY-ADR
.ENA-LAST-ACTVY-DT  �        D    ENA-ENA-ENTY-ADR.ENA-ADR-TYP-CD  �   
     D    ENA-ENA-ENTY-ADR.ENA-END-DT  �        D    ENA-ENA-ENTY-ADR.E
NA-START-DT  �        D    ENA-ENA-ENTY-ADR.ENA-LINE-ADR-1  @        
D    ENA-ENA-ENTY-ADR.ENA-LINE-ADR-2  b        D    ENA-ENA-ENTY-ADR.ENA-
LINE-ADR-3  h        D    ENA-ENA-ENTY-ADR.ENA-LINE-ADR-4  �        D
    ENA-ENA-ENTY-ADR.ENA-CITY-NAME  m        D    ENA-ENA-ENTY-ADR.ENA-TL
PHN-ID  �        D    ENA-ENA-ENTY-ADR.ENA-CNTY-CD  �        D    EN
A-ENA-ENTY-ADR.ENA-STATE-CD  w        D    ENA-ENA-ENTY-ADR.ENA-ZIP-CD  
�        D    ENA-ENA-ENTY-ADR.ENA-FK-ZIP-CNTRY-CD  �        D    ENA-
ENA-ENTY-ADR.ENA-LINE-ADR-1-UC  �        D    ENA-ENA-ENTY-ADR.ENA-FAX-TLP
HN-ID  �        �D    ENA-ENA-ENTY-ADR.ENA-EMADR-LINE  D        D    
ENA-ENA-ENTY-ADR.ENA-FAX-STOP  �        D    ENA-ENA-ENTY-ADR.ENA-ZIP-SETF
  }        D    ENA-ENA-ENTY-ADR.ENA-FK-ZIP-ZIP-CD  O        D    EN
A-ENA-ENTY-ADR.ENA-ZIP-SET-TS  �        D    ENA-ENA-ENTY-ADR.ENA-FK-ENT-E
NTY-ONE  S        D    
ENA-ENA-ENTY-ADR.ENA-FK-ENT-ENTY-NINE  Y        
D    
ENA-ENA-ENTY-ADR.ENA-IDX-ENTY-ADR1-TS � �       D    DBLINK-S-6311
-6185-KEY-O.DBLINK-S-6311-6185-01 � �       D    DBLINK-S-6311-6185-KEY-O.
DBLINK-S-6311-6185-02 � ��       D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-631
1-6185-03 � ��       D    DBLINK-S-6311-6185-KEY-U.DBLINK-S-6311-6185-04
DBRM  �     �     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , ENTY_ID_DGT_
ONE , ENTY_ID_LST_NINE , FK2_ECG_CHG_DT , FK1_ENT_ENTY_ONE , FK1_ENT_ENTY_NINE ,
 ENT_SET_TS , FK2_ECG_ENTY_ONE , FK2_ECG_ENTY_NINE , FK2_ENT_ECG_SET_TS , ECG_SE
T_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
, : H FROM ENI_ENTY_ID_V WHERE ENTY_ID_DGT_ONE = : H AND ENTY_ID_LST_NINE = : H
      T        D    ENI-ENI-ENTY-ID.ENI-USER-CD   Z        	D    ENI-E
NI-ENTY-ID.ENI-LAST-ACTVY-DT   �        D    ENI-ENI-ENTY-ID.ENI-UNVRS-CD 
  5        D    ENI-ENI-ENTY-ID.ENI-ENTY-ID-DGT-ONE   �        D    ENI
-ENI-ENTY-ID.ENI-ENTY-ID-LST-NINE          D    ENI-ENI-ENTY-ID.ENI-FK2-E
CG-CHG-DT          D    ENI-ENI-ENTY-ID.ENI-FK1-ENT-ENTY-ONE         
 D    
ENI-ENI-ENTY-ID.ENI-FK1-ENT-ENTY-NINE          D    ENI-ENI-ENTY
-ID.ENI-ENT-SET-TS          D    ENI-ENI-ENTY-ID.ENI-FK2-ECG-ENTY-ONE  
        D    
ENI-ENI-ENTY-ID.ENI-FK2-ECG-ENTY-NINE  
        D    ENI
-ENI-ENTY-ID.ENI-FK2-ENT-ECG-SET-TS          D    ENI-ENI-ENTY-ID.ENI-ECG
-SET-TS � !�       D    DBLINK-S-INDX-ENTY-ID-KEY-U.DBLINK-S-INDX-ENTY-ID-0
1 � ��       D    DBLINK-S-INDX-ENTY-ID-KEY-U.DBLINK-S-INDX-ENTY-ID-02
DBRM  z     �     |SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , PRSN_ID_DGT_
ONE , PRSN_ID_LST_NINE , CHANGE_DT , FK_PRS_IREF_ID , PRS_SET_TS , FK_PCH_IREF_I
D , FK_PCH_CHG_DT , FK_PCH_SET_TS , PCH_SET_TS INTO : H , : H , : H , : H , : H
, : H , : H , : H , : H , : H , : H , : H FROM PPI_PRSN_PRVID_V WHERE PRSN_ID_DG
T_ONE = : H AND PRSN_ID_LST_NINE = : H       ]        D    PPI-PPI-PRSN-PR
VID.PPI-USER-CD   C        	D    PPI-PPI-PRSN-PRVID.PPI-LAST-ACTVY-DT   I
       D    PPI-PPI-PRSN-PRVID.PPI-UNVRS-CD   �        D    PPI-PPI-PRSN
-PRVID.PPI-PRSN-ID-DGT-ONE   N        D    PPI-PPI-PRSN-PRVID.PPI-PRSN-ID-L
ST-NINE   �        D    PPI-PPI-PRSN-PRVID.PPI-CHANGE-DT   �        U  
  
PPI-PPI-PRSN-PRVID.PPI-FK-PRS-IREF-ID   X        D    PPI-PPI-PRSN-PRVID
.PPI-PRS-SET-TS   �        U    
PPI-PPI-PRSN-PRVID.PPI-FK-PCH-IREF-ID   3
        D    PPI-PPI-PRSN-PRVID.PPI-FK-PCH-CHG-DT   9        D    PPI-PP
I-PRSN-PRVID.PPI-FK-PCH-SET-TS   �        D    PPI-PPI-PRSN-PRVID.PPI-PCH-S
ET-TS � �       D    DBLINK-S-INDX-PRSN-ID-KEY-U.DBLINK-S-INDX-PRSN-ID-01 
� <�       D    DBLINK-S-INDX-PRSN-ID-KEY-U.DBLINK-S-INDX-PRSN-ID-02
DBRM  9          ?SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , IREF_ID , EN
TPSN_IND , TRVL_ACCT_ONE , TRVL_ACCT_NINE , EMPLY_IND , START_DT , END_DT , ADR_
TYP_CD , CRDT_CARD_NBR , CRDT_CARD_TYP , EXPRN_DT , ASSOC_EVNT_PRGD_DT INTO : H
, : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
: H FROM TVL_TRVL_V WHERE TRVL_ACCT_ONE = : H AND TRVL_ACCT_NINE = : H       
N        D    TVL-TVL-TRVL.TVL-USER-CD   �        	D    TVL-TVL-TRVL.TVL-
LAST-ACTVY-DT   �        D    TVL-TVL-TRVL.TVL-UNVRS-CD   X        U   
 TVL-TVL-TRVL.TVL-IREF-ID   �        D    TVL-TVL-TRVL.TVL-ENTPSN-IND   
3        D    TVL-TVL-TRVL.TVL-TRVL-ACCT-ONE   9        D    TVL-TVL-TRV
L.TVL-TRVL-ACCT-NINE   �        D    TVL-TVL-TRVL.TVL-EMPLY-IND  	      
  D    TVL-TVL-TRVL.TVL-START-DT          D    TVL-TVL-TRVL.TVL-END-DT
          D    TVL-TVL-TRVL.TVL-ADR-TYP-CD          D    TVL-TVL-T
RVL.TVL-CRDT-CARD-NBR          D    TVL-TVL-TRVL.TVL-CRDT-CARD-TYP  
       U    TVL-TVL-TRVL.TVL-EXPRN-DT          D    TVL-TVL-TRVL.TVL-A
SSOC-EVNT-PRGD-DT � ��       D    DBLINK-S-INDX-TRVL-KEY-U.DBLINK-S-INDX-TR
VL-01 � %�       D    DBLINK-S-INDX-TRVL-KEY-U.DBLINK-S-INDX-TRVL-02
DBRM  3     :     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , FICE_CD , SH
ORT_NAME , FULL_NAME , LINE_ADR_1 , LINE_ADR_2 , LINE_ADR_3 , LINE_ADR_4 , TLPHN
_ID , CITY_NAME , STATE_CD , ZIP_CD , CNTRY_CD , FK_SYS_SYS_CD , SYS_SET_TS INTO
 : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H , : H , : H FROM UNV_UNVRS_V WHERE UNVRS_CD = : H       �        D
    UNV-UNV-UNVRS.UNV-USER-CD   \        	D    UNV-UNV-UNVRS.UNV-LAST-ACTVY
-DT   W        D    UNV-UNV-UNVRS.UNV-UNVRS-CD   �        U    UNV-UNV
-UNVRS.UNV-FICE-CD   2        D    UNV-UNV-UNVRS.UNV-SHORT-NAME   8      
  D    UNV-UNV-UNVRS.UNV-FULL-NAME   �        D    UNV-UNV-UNVRS.UNV-LIN
E-ADR-1          D    UNV-UNV-UNVRS.UNV-LINE-ADR-2          D    U
NV-UNV-UNVRS.UNV-LINE-ADR-3          D    UNV-UNV-UNVRS.UNV-LINE-ADR-4  
        D    UNV-UNV-UNVRS.UNV-TLPHN-ID          D    UNV-UNV-UNVRS
.UNV-CITY-NAME          D    UNV-UNV-UNVRS.UNV-STATE-CD          D 
   UNV-UNV-UNVRS.UNV-ZIP-CD          D    UNV-UNV-UNVRS.UNV-CNTRY-CD  
        D    UNV-UNV-UNVRS.UNV-FK-SYS-SYS-CD          D    UNV-UNV-U
NVRS.UNV-SYS-SET-TS  -�       D    UNV-UNV-UNVRS.UNV-UNVRS-CD
DBRM  8     �     6SELECT USER_CD , LAST_ACTVY_DT , FK_UNV_UNVRS_CD , IREF_
ID , SSN_DGT_ONE , SSN_LST_NINE , NAME_KEY , NAME_SLTN_DESC , PRSN_FULL_NAME , N
AME_SFX_DESC , BIRTH_DT , GNDR_FLAG , DCSD_FLAG , DCSD_DT , ALT_ID , PAN_NBR , P
AN_FLAG , PAN_DT , MRTL_CD , RLGN_CD , ETHNC_CD , ARCHVD_DT , UNV_SETF , UNV_SET
_TS , IDX_SSN_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
, : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
: H , : H FROM PRS_PRSN_V WHERE IREF_ID = : H              D    PRS-PRS-
PRSN.PRS-USER-CD  �        	D    PRS-PRS-PRSN.PRS-LAST-ACTVY-DT  .      
  D    PRS-PRS-PRSN.PRS-FK-UNV-UNVRS-CD  �        U    PRS-PRS-PRSN.PRS
-IREF-ID  �        D    PRS-PRS-PRSN.PRS-SSN-DGT-ONE  )        D    
PRS-PRS-PRSN.PRS-SSN-LST-NINE  �        D    PRS-PRS-PRSN.PRS-NAME-KEY  
�        D    PRS-PRS-PRSN.PRS-NAME-SLTN-DESC  ?        D    PRS-PRS-P
RSN.PRS-PRSN-FULL-NAME  �        D    PRS-PRS-PRSN.PRS-NAME-SFX-DESC  #
        D    PRS-PRS-PRSN.PRS-BIRTH-DT  a        D    PRS-PRS-PRSN.PRS-
GNDR-FLAG  g        D    PRS-PRS-PRSN.PRS-DCSD-FLAG  �        D    P
RS-PRS-PRSN.PRS-DCSD-DT  l        D    PRS-PRS-PRSN.PRS-ALT-ID  r      
  D    PRS-PRS-PRSN.PRS-PAN-NBR  �        D    PRS-PRS-PRSN.PRS-PAN-FLA
G  v        D    PRS-PRS-PRSN.PRS-PAN-DT  �        D    PRS-PRS-PRSN
.PRS-MRTL-CD  �        D    PRS-PRS-PRSN.PRS-RLGN-CD  �        D    
PRS-PRS-PRSN.PRS-ETHNC-CD  ]        D    PRS-PRS-PRSN.PRS-ARCHVD-DT  C
       D    PRS-PRS-PRSN.PRS-UNV-SETF  I        D    PRS-PRS-PRSN.PRS-U
NV-SET-TS  �        D    PRS-PRS-PRSN.PRS-IDX-SSN-TS  3�       U    
PRS-PRS-PRSN.PRS-IREF-ID
DBRM  D           �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , ADR_TYP_CD ,
 DSPLY_PRTY_SEQ , SHORT_DESC , LONG_DESC , RCRD_USED_FLAG INTO : H , : H , : H ,
 : H , : H , : H , : H , : H FROM ADT_ADR_TYP_V WHERE ADR_TYP_CD = : H       
�        D    ADT-ADT-ADR-TYP.ADT-USER-CD   =        	D    ADT-ADT-ADR-TY
P.ADT-LAST-ACTVY-DT   d        D    ADT-ADT-ADR-TYP.ADT-UNVRS-CD   �     
   D    ADT-ADT-ADR-TYP.ADT-ADR-TYP-CD   �        U    ADT-ADT-ADR-TYP.A
DT-DSPLY-PRTY-SEQ   o        D    ADT-ADT-ADR-TYP.ADT-SHORT-DESC   �     
   D    ADT-ADT-ADR-TYP.ADT-LONG-DESC   s        D    ADT-ADT-ADR-TYP.AD
T-RCRD-USED-FLAG   ��       D    ADT-ADT-ADR-TYP.ADT-ADR-TYP-CD
DBRM       ,      ySELECT USER_CD , LAST_ACTVY_DT , FK_UNV_UNVRS_CD , CNTRY
_CD , FULL_NAME , RCRD_USED_FLAG INTO : H , : H , : H , : H , : H , : H FROM CTY
_COUNTRY_V WHERE CNTRY_CD = : H       ^        D    CTY-CTY-COUNTRY.CTY-US
ER-CD   �        	D    CTY-CTY-COUNTRY.CTY-LAST-ACTVY-DT   ,        D   
 CTY-CTY-COUNTRY.CTY-FK-UNV-UNVRS-CD   �        D    CTY-CTY-COUNTRY.CTY-C
NTRY-CD   �        D    CTY-CTY-COUNTRY.CTY-FULL-NAME   '        D    
CTY-CTY-COUNTRY.CTY-RCRD-USED-FLAG   v�       D    CTY-CTY-COUNTRY.CTY-CNTR
Y-CD
DBRM  B     w     SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , ENTY_ID_DGT_
ONE , ENTY_ID_LST_NINE , NAME_KEY , ENTY_FULL_NAME , IREF_ID , PAN_NBR , PAN_FLA
G , PAN_DT INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
FROM ENT_ENTY_V WHERE ENTY_ID_DGT_ONE = : H AND ENTY_ID_LST_NINE = : H       
r        D    ENT-ENT-ENTY.ENT-USER-CD   �        	D    ENT-ENT-ENTY.ENT-
LAST-ACTVY-DT   v        D    ENT-ENT-ENTY.ENT-UNVRS-CD   �        D   
 ENT-ENT-ENTY.ENT-ENTY-ID-DGT-ONE   �        D    ENT-ENT-ENTY.ENT-ENTY-ID
-LST-NINE   �        D    ENT-ENT-ENTY.ENT-NAME-KEY   ]        D    EN
T-ENT-ENTY.ENT-ENTY-FULL-NAME   C        U    ENT-ENT-ENTY.ENT-IREF-ID   
I        D    ENT-ENT-ENTY.ENT-PAN-NBR   �        D    ENT-ENT-ENTY.ENT-
PAN-FLAG   N        D    ENT-ENT-ENTY.ENT-PAN-DT  �       D    ENT-E
NT-ENTY.ENT-ENTY-ID-DGT-ONE  �       D    ENT-ENT-ENTY.ENT-ENTY-ID-LST-NI
NE
DBRM        
      CLOSE S_INDX_TRVL_N     
DBRM        
      CLOSE S_INDX_TRVL_N     
DBRM        
       CLOSE S_6117_6139_P     
DBRM        
.      CLOSE S_6117_6139_P     
DBRM        
!      CLOSE S_6311_6185_P     
DBRM        
�      CLOSE S_6311_6185_P     
DBRM         
`      COMMIT WORK     
DBRM   
      
s      ROLLBACK     
DBRM   
      
2      ROLLBACK     
