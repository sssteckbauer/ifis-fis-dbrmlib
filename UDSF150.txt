DBRM   �APCEGE  UDSF150 jk9 2                                     1  �NK
                                                                                
DBRM  N           �DECLARE IVH_INV_HDR_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , DOC_NBR CHAR ( 8 ) NOT NULL , PYMT_DUE_DT DATE NOT
 NULL WITH DEFAULT , BANK_ACCT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , SALES_USE_T
AX_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , A1099_IND CHAR ( 1 ) NOT NULL WITH DEF
AULT , CRDT_MEMO CHAR ( 1 ) NOT NULL WITH DEFAULT , CHECK_GRPNG_IND CHAR ( 1 ) N
OT NULL WITH DEFAULT , CMPLT_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , RSBLE_IND CH
AR ( 1 ) NOT NULL WITH DEFAULT , ADDL_CHRG DECIMAL ( 12 , 2 ) NOT NULL WITH DEFA
ULT , ADJMT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ADJMT_DT DATE NOT NULL WITH DE
FAULT , OPN_PD_HLD_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , INVD_AMT DECIMAL ( 12
, 2 ) NOT NULL WITH DEFAULT , APRVL_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , HDR_E
RROR_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , HDR_CNTR_DTL DECIMAL ( 4 ) NOT NULL
WITH DEFAULT , HDR_CNTR_ACCT DECIMAL ( 4 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , PO_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , DSCNT
_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , VNDR_INV_NBR CHAR ( 9 ) NOT NULL WITH DEF
AULT , TAX_RATE_CD CHAR ( 3 ) NOT NULL WITH DEFAULT , VNDR_ID_DGT_ONE CHAR ( 1 )
 NOT NULL WITH DEFAULT , VNDR_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , INC
M_TYP_SEQ_NBR DECIMAL ( 4 ) NOT NULL WITH DEFAULT , A1099_RPRT_ID CHAR ( 9 ) NOT
 NULL WITH DEFAULT , TRANS_DT DATE NOT NULL WITH DEFAULT , CNCL_IND CHAR ( 1 ) N
OT NULL WITH DEFAULT , CNCL_DT DATE NOT NULL WITH DEFAULT , CHECKS_WRITTEN_QTY D
ECIMAL ( 5 ) NOT NULL WITH DEFAULT , VNDR_INV_DT DATE NOT NULL WITH DEFAULT , DO
C_REF_NBR CHAR ( 10 ) NOT NULL WITH DEFAULT , MAILCODE CHAR ( 6 ) NOT NULL WITH
DEFAULT , SUB_DOC_TYP CHAR ( 3 ) NOT NULL WITH DEFAULT , ORGN_CD CHAR ( 4 ) NOT
NULL WITH DEFAULT , IDC_APPL_AMT DECIMAL ( 7 , 2 ) NOT NULL WITH DEFAULT , IDC_A
CTL_AMT DECIMAL ( 7 , 2 ) NOT NULL WITH DEFAULT , VDR_I_SETF1 CHAR ( 1 ) NOT NUL
L WITH DEFAULT , FK1_VDR_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK1_VDR_E
NTPSN_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , VDR_V_SETF2 CHAR ( 1 ) NOT NULL WIT
H DEFAULT , FK2_VDR_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK2_VDR_ENTPSN
_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , VDR_SET_TS TIMESTAMP NOT NULL WITH DEFAU
LT , IDX_INV_SETF CHAR ( 1 ) NOT NULL WITH DEFAULT , RSV_SETF CHAR ( 1 ) NOT NUL
L WITH DEFAULT , FK_RSV_RSBLE_ID CHAR ( 3 ) NOT NULL WITH DEFAULT , RSV_SET_TS T
IMESTAMP NOT NULL WITH DEFAULT )     
DBRM  �      �     �DECLARE IVD_INV_DTL_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , ITEM_NBR DECIMAL ( 4 , 0 ) NOT NULL , CM
DTY_CD CHAR ( 8 ) NOT NULL , OPN_CLS_HLD_IND CHAR ( 1 ) NOT NULL , ACCPT_QTY DEC
IMAL ( 8 , 2 ) NOT NULL , ACCPT_UNIT_PRC DECIMAL ( 14 , 4 ) NOT NULL , PO_NBR CH
AR ( 8 ) NOT NULL , CMDTY_DESC CHAR ( 35 ) NOT NULL , UNIT_MEA_CD CHAR ( 3 ) NOT
 NULL , DTL_CNTR_ACCT DECIMAL ( 4 , 0 ) NOT NULL , DTL_ERROR_IND CHAR ( 1 ) NOT
NULL , DSCNT_AMT DECIMAL ( 12 , 2 ) NOT NULL , TAX_AMT DECIMAL ( 12 , 2 ) NOT NU
LL , APRVD_QTY DECIMAL ( 8 , 2 ) NOT NULL , APRVD_UNT_PRC DECIMAL ( 14 , 4 ) NOT
 NULL , INVD_QTY DECIMAL ( 8 , 2 ) NOT NULL , INVD_UNIT_PRC DECIMAL ( 14 , 4 ) N
OT NULL , PO_ITEM_NBR DECIMAL ( 4 , 0 ) NOT NULL , TLRNC_OVRD_IND CHAR ( 1 ) NOT
 NULL , PREV_PAID_AMT DECIMAL ( 12 , 2 ) NOT NULL , FK_IVH_DOC_NBR CHAR ( 8 ) NO
T NULL , POD_SETF CHAR ( 1 ) NOT NULL , FK_POD_PO_NBR CHAR ( 8 ) NOT NULL , FK_P
OD_CHG_SEQ_NBR CHAR ( 3 ) NOT NULL , FK_POD_ITEM_NBR DECIMAL ( 4 , 0 ) NOT NULL
, POD_SET_TS TIMESTAMP NOT NULL , TRD_IN_IND CHAR ( 1 ) NOT NULL , TRD_IN_AMT DE
CIMAL ( 12 , 2 ) NOT NULL )     
DBRM  	}           	�DECLARE IVA_INV_ACCT_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , NS
F_OVRDE CHAR ( 1 ) NOT NULL , ADDL_CHRG_PCT DECIMAL ( 6 , 3 ) NOT NULL , INVD_AM
T DECIMAL ( 12 , 2 ) NOT NULL , DSCNT_AMT DECIMAL ( 12 , 2 ) NOT NULL , LIQDTN_I
ND CHAR ( 1 ) NOT NULL , COA_CD CHAR ( 1 ) NOT NULL , ACCT_INDX_CD CHAR ( 10 ) N
OT NULL , FUND_CD CHAR ( 6 ) NOT NULL , ORGN_CD CHAR ( 6 ) NOT NULL , ACCT_CD CH
AR ( 6 ) NOT NULL , PRGRM_CD CHAR ( 6 ) NOT NULL , ACTVY_CD CHAR ( 6 ) NOT NULL
, LCTN_CD CHAR ( 6 ) NOT NULL , ACCT_ERROR_IND CHAR ( 1 ) NOT NULL , FSCL_YR CHA
R ( 2 ) NOT NULL , BANK_ACCT_CD CHAR ( 2 ) NOT NULL , APRVD_AMT DECIMAL ( 12 , 2
 ) NOT NULL , TAX_AMT DECIMAL ( 12 , 2 ) NOT NULL , INVD_PCT DECIMAL ( 6 , 3 ) N
OT NULL , DSCNT_PCT DECIMAL ( 6 , 3 ) NOT NULL , ADDL_CHRG DECIMAL ( 12 , 2 ) NO
T NULL , INCM_TYP_SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , PAID_AMT DECIMAL ( 12 , 2
 ) NOT NULL , RULE_CLS_CD CHAR ( 4 ) NOT NULL , DSCNT_RULE_CLS CHAR ( 4 ) NOT NU
LL , TAX_RULE_CLS CHAR ( 4 ) NOT NULL , ADDL_CHRG_RUL_CLS CHAR ( 4 ) NOT NULL ,
PSTNG_PRD CHAR ( 2 ) NOT NULL , PRJCT_CD CHAR ( 8 ) NOT NULL , PO_NBR CHAR ( 8 )
 NOT NULL , PO_ITEM_NBR DECIMAL ( 4 , 0 ) NOT NULL , PO_SEQ_NBR DECIMAL ( 4 , 0
) NOT NULL , FK_IVD_DOC_NBR CHAR ( 8 ) NOT NULL , FK_IVD_ITEM_NBR DECIMAL ( 4 ,
0 ) NOT NULL , TRD_IN_IND CHAR ( 1 ) NOT NULL , TRD_IN_AMT DECIMAL ( 12 , 2 ) NO
T NULL , TRD_RULE_CLS CHAR ( 4 ) NOT NULL )     
DBRM  <     �     DECLARE R4150_INV_HDR_N CURSOR WITH HOLD FOR SELECT USER
_CD , LAST_ACTVY_DT , UNVRS_CD , DOC_NBR , PYMT_DUE_DT , BANK_ACCT_CD , SALES_US
E_TAX_IND , A1099_IND , CRDT_MEMO , CHECK_GRPNG_IND , CMPLT_IND , RSBLE_IND , AD
DL_CHRG , ADJMT_CD , ADJMT_DT , OPN_PD_HLD_IND , INVD_AMT , APRVL_IND , HDR_ERRO
R_IND , HDR_CNTR_DTL , HDR_CNTR_ACCT , ADR_TYP_CD , PO_NBR , DSCNT_CD , VNDR_INV
_NBR , TAX_RATE_CD , VNDR_ID_DGT_ONE , VNDR_ID_LST_NINE , INCM_TYP_SEQ_NBR , A10
99_RPRT_ID , TRANS_DT , CNCL_IND , CNCL_DT , CHECKS_WRITTEN_QTY , VNDR_INV_DT ,
DOC_REF_NBR , MAILCODE , SUB_DOC_TYP , ORGN_CD , IDC_APPL_AMT , IDC_ACTL_AMT , V
DR_I_SETF1 , FK1_VDR_IREF_ID , FK1_VDR_ENTPSN_IND , VDR_V_SETF2 , FK2_VDR_IREF_I
D , FK2_VDR_ENTPSN_IND , VDR_SET_TS , IDX_INV_SETF , RSV_SETF , FK_RSV_RSBLE_ID
, RSV_SET_TS FROM IVH_INV_HDR_V WITH UR     
DBRM  E     5     DECLARE S_4150_4151_N CURSOR WITH HOLD FOR SELECT USER_C
D , LAST_ACTVY_DT , ITEM_NBR , CMDTY_CD , OPN_CLS_HLD_IND , ACCPT_QTY , ACCPT_UN
IT_PRC , PO_NBR , CMDTY_DESC , UNIT_MEA_CD , DTL_CNTR_ACCT , DTL_ERROR_IND , DSC
NT_AMT , TAX_AMT , APRVD_QTY , APRVD_UNT_PRC , INVD_QTY , INVD_UNIT_PRC , PO_ITE
M_NBR , TLRNC_OVRD_IND , PREV_PAID_AMT , FK_IVH_DOC_NBR , POD_SETF , FK_POD_PO_N
BR , FK_POD_CHG_SEQ_NBR , FK_POD_ITEM_NBR , POD_SET_TS FROM IVD_INV_DTL_V WHERE
FK_IVH_DOC_NBR = : H AND ITEM_NBR > : H ORDER BY ITEM_NBR ASC WITH UR OPTIMIZE F
OR 1 ROW     � ��       D    DBLINK-S-4150-4151-KEY-O.DBLINK-S-4150-4151-0
1 � ��       U    DBLINK-S-4150-4151-KEY-U.DBLINK-S-4150-4151-02
DBRM  =          oDECLARE S_4151_4152_N CURSOR WITH HOLD FOR SELECT USER_C
D , LAST_ACTVY_DT , SEQ_NBR , NSF_OVRDE , ADDL_CHRG_PCT , INVD_AMT , DSCNT_AMT ,
 LIQDTN_IND , COA_CD , ACCT_INDX_CD , FUND_CD , ORGN_CD , ACCT_CD , PRGRM_CD , A
CTVY_CD , LCTN_CD , ACCT_ERROR_IND , FSCL_YR , BANK_ACCT_CD , APRVD_AMT , TAX_AM
T , INVD_PCT , DSCNT_PCT , ADDL_CHRG , INCM_TYP_SEQ_NBR , PAID_AMT , RULE_CLS_CD
 , DSCNT_RULE_CLS , TAX_RULE_CLS , ADDL_CHRG_RUL_CLS , PSTNG_PRD , PRJCT_CD , PO
_NBR , PO_ITEM_NBR , PO_SEQ_NBR , FK_IVD_DOC_NBR , FK_IVD_ITEM_NBR FROM IVA_INV_
ACCT_V WHERE FK_IVD_DOC_NBR = : H AND FK_IVD_ITEM_NBR = : H AND SEQ_NBR > : H OR
DER BY SEQ_NBR ASC WITH UR OPTIMIZE FOR 1 ROW     � �       D    DBLINK-S
-4151-4152-KEY-O.DBLINK-S-4151-4152-01 � ��       U    DBLINK-S-4151-4152-K
EY-O.DBLINK-S-4151-4152-02 � ��       U    DBLINK-S-4151-4152-KEY-U.DBLINK-
S-4151-4152-03
DBRM   Z     �      �SELECT 1 INTO : H FROM IVD_INV_DTL_V WHERE FK_IVH_DOC_NB
R = : H WITH UR     �          0    DBLINK-OTHER-WORK-AREAS.DBLINK-MEMBER-
COUNT �  �       D    DBLINK-S-4150-4151-KEY-O.DBLINK-S-4150-4151-01
DBRM  � 	          �SELECT 1 INTO : H FROM IVA_INV_ACCT_V WHERE FK_IVD_DOC_N
BR = : H AND FK_IVD_ITEM_NBR = : H WITH UR     �          0    DBLINK-OTHE
R-WORK-AREAS.DBLINK-MEMBER-COUNT �  �       D    DBLINK-S-4151-4152-KEY-O.D
BLINK-S-4151-4152-01 �  ��       U    DBLINK-S-4151-4152-KEY-O.DBLINK-S-4151
-4152-02
DBRM        �      
OPEN R4150_INV_HDR_N     
DBRM       .     �FETCH R4150_INV_HDR_N INTO : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H , : H               D    IVH-IVH-INV-H
DR.IVH-USER-CD           	D    IVH-IVH-INV-HDR.IVH-LAST-ACTVY-DT        
   D    IVH-IVH-INV-HDR.IVH-UNVRS-CD           D    IVH-IVH-INV-HDR.IVH
-DOC-NBR           D    IVH-IVH-INV-HDR.IVH-PYMT-DUE-DT           D  
  IVH-IVH-INV-HDR.IVH-BANK-ACCT-CD            D    
IVH-IVH-INV-HDR.IVH-SAL
ES-USE-TAX-IND   �        D    IVH-IVH-INV-HDR.IVH-A1099-IND   <        
D    IVH-IVH-INV-HDR.IVH-CRDT-MEMO   �        D    IVH-IVH-INV-HDR.IVH-CH
ECK-GRPNG-IND   �        D    IVH-IVH-INV-HDR.IVH-CMPLT-IND   ;        
D    IVH-IVH-INV-HDR.IVH-RSBLE-IND   �       U    IVH-IVH-INV-HDR.IVH-ADD
L-CHRG   �        D    IVH-IVH-INV-HDR.IVH-ADJMT-CD   �        D    IV
H-IVH-INV-HDR.IVH-ADJMT-DT   �        D    IVH-IVH-INV-HDR.IVH-OPN-PD-HLD-I
ND   @       U    IVH-IVH-INV-HDR.IVH-INVD-AMT   b        D    IVH-IV
H-INV-HDR.IVH-APRVL-IND   h        D    IVH-IVH-INV-HDR.IVH-HDR-ERROR-IND 
  �        U    IVH-IVH-INV-HDR.IVH-HDR-CNTR-DTL   m        U    IVH-IV
H-INV-HDR.IVH-HDR-CNTR-ACCT   �        D    IVH-IVH-INV-HDR.IVH-ADR-TYP-CD 
  �        D    IVH-IVH-INV-HDR.IVH-PO-NBR   w        D    IVH-IVH-INV
-HDR.IVH-DSCNT-CD   �        D    IVH-IVH-INV-HDR.IVH-VNDR-INV-NBR   �   
     D    IVH-IVH-INV-HDR.IVH-TAX-RATE-CD   �        D    IVH-IVH-INV-HD
R.IVH-VNDR-ID-DGT-ONE   �        D    IVH-IVH-INV-HDR.IVH-VNDR-ID-LST-NINE 
  D        U    IVH-IVH-INV-HDR.IVH-INCM-TYP-SEQ-NBR   �        D    I
VH-IVH-INV-HDR.IVH-A1099-RPRT-ID   }        D    IVH-IVH-INV-HDR.IVH-TRANS-
DT   O        D    IVH-IVH-INV-HDR.IVH-CNCL-IND   �        D    IVH-IV
H-INV-HDR.IVH-CNCL-DT   S  	      U    IVH-IVH-INV-HDR.IVH-CHECKS-WRITTEN-QT
Y   Y        D    IVH-IVH-INV-HDR.IVH-VNDR-INV-DT   �        D    IVH-
IVH-INV-HDR.IVH-DOC-REF-NBR   4        D    IVH-IVH-INV-HDR.IVH-MAILCODE  
 �        D    IVH-IVH-INV-HDR.IVH-SUB-DOC-TYP           D    IVH-IVH-
INV-HDR.IVH-ORGN-CD         U    IVH-IVH-INV-HDR.IVH-IDC-APPL-AMT  
      U    IVH-IVH-INV-HDR.IVH-IDC-ACTL-AMT          D    IVH-IVH-INV
-HDR.IVH-VDR-I-SETF1          U    IVH-IVH-INV-HDR.IVH-FK1-VDR-IREF-ID  
        D    IVH-IVH-INV-HDR.IVH-FK1-VDR-ENTPSN-IND          D    I
VH-IVH-INV-HDR.IVH-VDR-V-SETF2          U    IVH-IVH-INV-HDR.IVH-FK2-VDR-
IREF-ID          D    IVH-IVH-INV-HDR.IVH-FK2-VDR-ENTPSN-IND         
 D    IVH-IVH-INV-HDR.IVH-VDR-SET-TS          D    IVH-IVH-INV-HDR.IVH
-IDX-INV-SETF  �        D    IVH-IVH-INV-HDR.IVH-RSV-SETF  �        D
    IVH-IVH-INV-HDR.IVH-FK-RSV-RSBLE-ID  +        D    IVH-IVH-INV-HDR.IV
H-RSV-SET-TS
DBRM   �     {      OPEN S_4150_4151_N     �   �       D    DBLINK-S-41
50-4151-KEY-O.DBLINK-S-4150-4151-01 �   �       U    DBLINK-S-4150-4151-KEY-
U.DBLINK-S-4150-4151-02
DBRM  �     I      �FETCH S_4150_4151_N INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H               D    IVD-I
VD-INV-DTL.IVD-USER-CD           	D    IVD-IVD-INV-DTL.IVD-LAST-ACTVY-DT  
         U    IVD-IVD-INV-DTL.IVD-ITEM-NBR           D    IVD-IVD-INV
-DTL.IVD-CMDTY-CD           D    IVD-IVD-INV-DTL.IVD-OPN-CLS-HLD-IND   
       U    IVD-IVD-INV-DTL.IVD-ACCPT-QTY          U    IVD-IVD-INV-D
TL.IVD-ACCPT-UNIT-PRC   �        D    IVD-IVD-INV-DTL.IVD-PO-NBR   �     
   D    IVD-IVD-INV-DTL.IVD-CMDTY-DESC   &        D    IVD-IVD-INV-DTL.I
VD-UNIT-MEA-CD   �        U    IVD-IVD-INV-DTL.IVD-DTL-CNTR-ACCT   *     
   D    IVD-IVD-INV-DTL.IVD-DTL-ERROR-IND   �       U    IVD-IVD-INV-DT
L.IVD-DSCNT-AMT   �       U    IVD-IVD-INV-DTL.IVD-TAX-AMT   >       
U    IVD-IVD-INV-DTL.IVD-APRVD-QTY   �       U    IVD-IVD-INV-DTL.IVD-APR
VD-UNT-PRC   :       U    IVD-IVD-INV-DTL.IVD-INVD-QTY   �       U   
 IVD-IVD-INV-DTL.IVD-INVD-UNIT-PRC   f        U    IVD-IVD-INV-DTL.IVD-PO-
ITEM-NBR   �        D    IVD-IVD-INV-DTL.IVD-TLRNC-OVRD-IND   k       
U    IVD-IVD-INV-DTL.IVD-PREV-PAID-AMT   q        D    IVD-IVD-INV-DTL.IVD
-FK-IVH-DOC-NBR   �        D    IVD-IVD-INV-DTL.IVD-POD-SETF   u        
D    IVD-IVD-INV-DTL.IVD-FK-POD-PO-NBR   �        D    IVD-IVD-INV-DTL.IV
D-FK-POD-CHG-SEQ-NBR   �        U    IVD-IVD-INV-DTL.IVD-FK-POD-ITEM-NBR  
 �        D    IVD-IVD-INV-DTL.IVD-POD-SET-TS
DBRM   �           OPEN S_4151_4152_N     �   �       D    DBLINK-S-41
51-4152-KEY-O.DBLINK-S-4151-4152-01 �   �       U    DBLINK-S-4151-4152-KEY-
O.DBLINK-S-4151-4152-02 �   �       U    DBLINK-S-4151-4152-KEY-U.DBLINK-S-4
151-4152-03
DBRM  �           5FETCH S_4151_4152_N INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H  
             D    IVA-IVA-INV-ACCT.IVA-USER
-CD           	D    IVA-IVA-INV-ACCT.IVA-LAST-ACTVY-DT           U    
IVA-IVA-INV-ACCT.IVA-SEQ-NBR           D    IVA-IVA-INV-ACCT.IVA-NSF-OVRD
E          U    IVA-IVA-INV-ACCT.IVA-ADDL-CHRG-PCT          U    I
VA-IVA-INV-ACCT.IVA-INVD-AMT          U    IVA-IVA-INV-ACCT.IVA-DSCNT-AMT
   �        D    IVA-IVA-INV-ACCT.IVA-LIQDTN-IND   �        D    IVA-I
VA-INV-ACCT.IVA-COA-CD   &        D    IVA-IVA-INV-ACCT.IVA-ACCT-INDX-CD  
 �        D    IVA-IVA-INV-ACCT.IVA-FUND-CD   *        D    IVA-IVA-INV
-ACCT.IVA-ORGN-CD   �        D    IVA-IVA-INV-ACCT.IVA-ACCT-CD   �       
 D    IVA-IVA-INV-ACCT.IVA-PRGRM-CD   >        D    IVA-IVA-INV-ACCT.IVA
-ACTVY-CD   �        D    IVA-IVA-INV-ACCT.IVA-LCTN-CD   :        D    
IVA-IVA-INV-ACCT.IVA-ACCT-ERROR-IND   �        D    IVA-IVA-INV-ACCT.IVA-F
SCL-YR   f        D    IVA-IVA-INV-ACCT.IVA-BANK-ACCT-CD   �       U  
  IVA-IVA-INV-ACCT.IVA-APRVD-AMT   k       U    IVA-IVA-INV-ACCT.IVA-TAX-
AMT   q       U    IVA-IVA-INV-ACCT.IVA-INVD-PCT   �       U    IVA-
IVA-INV-ACCT.IVA-DSCNT-PCT   u       U    IVA-IVA-INV-ACCT.IVA-ADDL-CHRG 
  �        U    
IVA-IVA-INV-ACCT.IVA-INCM-TYP-SEQ-NBR   �       U    I
VA-IVA-INV-ACCT.IVA-PAID-AMT   �        D    IVA-IVA-INV-ACCT.IVA-RULE-CLS-
CD   �        D    IVA-IVA-INV-ACCT.IVA-DSCNT-RULE-CLS   B        D    
IVA-IVA-INV-ACCT.IVA-TAX-RULE-CLS   H        D    IVA-IVA-INV-ACCT.IVA-ADD
L-CHRG-RUL-CLS   �        D    IVA-IVA-INV-ACCT.IVA-PSTNG-PRD   M        
D    IVA-IVA-INV-ACCT.IVA-PRJCT-CD   �        D    IVA-IVA-INV-ACCT.IVA-
PO-NBR   \        U    IVA-IVA-INV-ACCT.IVA-PO-ITEM-NBR   W        U   
 IVA-IVA-INV-ACCT.IVA-PO-SEQ-NBR   �        D    IVA-IVA-INV-ACCT.IVA-FK-I
VD-DOC-NBR   2        U    IVA-IVA-INV-ACCT.IVA-FK-IVD-ITEM-NBR
DBRM  >     =     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , DOC_NBR , PY
MT_DUE_DT , BANK_ACCT_CD , SALES_USE_TAX_IND , A1099_IND , CRDT_MEMO , CHECK_GRP
NG_IND , CMPLT_IND , RSBLE_IND , ADDL_CHRG , ADJMT_CD , ADJMT_DT , OPN_PD_HLD_IN
D , INVD_AMT , APRVL_IND , HDR_ERROR_IND , HDR_CNTR_DTL , HDR_CNTR_ACCT , ADR_TY
P_CD , PO_NBR , DSCNT_CD , VNDR_INV_NBR , TAX_RATE_CD , VNDR_ID_DGT_ONE , VNDR_I
D_LST_NINE , INCM_TYP_SEQ_NBR , A1099_RPRT_ID , TRANS_DT , CNCL_IND , CNCL_DT ,
CHECKS_WRITTEN_QTY , VNDR_INV_DT , DOC_REF_NBR , MAILCODE , SUB_DOC_TYP , ORGN_C
D , IDC_APPL_AMT , IDC_ACTL_AMT , VDR_I_SETF1 , FK1_VDR_IREF_ID , FK1_VDR_ENTPSN
_IND , VDR_V_SETF2 , FK2_VDR_IREF_ID , FK2_VDR_ENTPSN_IND , VDR_SET_TS , IDX_INV
_SETF , RSV_SETF , FK_RSV_RSBLE_ID , RSV_SET_TS INTO : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H FROM IVH_INV_HDR_V WHERE DOC_NBR = :
H WITH UR      �        D    IVH-IVH-INV-HDR.IVH-USER-CD  4        	D
    IVH-IVH-INV-HDR.IVH-LAST-ACTVY-DT  �        D    IVH-IVH-INV-HDR.IVH-
UNVRS-CD           D    IVH-IVH-INV-HDR.IVH-DOC-NBR          D    I
VH-IVH-INV-HDR.IVH-PYMT-DUE-DT          D    IVH-IVH-INV-HDR.IVH-BANK-ACC
T-CD          D    
IVH-IVH-INV-HDR.IVH-SALES-USE-TAX-IND          D
    IVH-IVH-INV-HDR.IVH-A1099-IND          D    IVH-IVH-INV-HDR.IVH-CRDT
-MEMO          D    IVH-IVH-INV-HDR.IVH-CHECK-GRPNG-IND          D 
   IVH-IVH-INV-HDR.IVH-CMPLT-IND          D    IVH-IVH-INV-HDR.IVH-RSBLE
-IND         U    IVH-IVH-INV-HDR.IVH-ADDL-CHRG          D    IVH
-IVH-INV-HDR.IVH-ADJMT-CD  �        D    IVH-IVH-INV-HDR.IVH-ADJMT-DT  
�        D    IVH-IVH-INV-HDR.IVH-OPN-PD-HLD-IND  +       U    IVH-IVH
-INV-HDR.IVH-INVD-AMT  �        D    IVH-IVH-INV-HDR.IVH-APRVL-IND  !  
      D    IVH-IVH-INV-HDR.IVH-HDR-ERROR-IND  -        U    IVH-IVH-INV
-HDR.IVH-HDR-CNTR-DTL  �        U    IVH-IVH-INV-HDR.IVH-HDR-CNTR-ACCT  
%        D    IVH-IVH-INV-HDR.IVH-ADR-TYP-CD  �        D    IVH-IVH-IN
V-HDR.IVH-PO-NBR  �        D    IVH-IVH-INV-HDR.IVH-DSCNT-CD  =        
D    IVH-IVH-INV-HDR.IVH-VNDR-INV-NBR  d        D    IVH-IVH-INV-HDR.IV
H-TAX-RATE-CD  �        D    IVH-IVH-INV-HDR.IVH-VNDR-ID-DGT-ONE  �    
    D    IVH-IVH-INV-HDR.IVH-VNDR-ID-LST-NINE  o        U    IVH-IVH-IN
V-HDR.IVH-INCM-TYP-SEQ-NBR  �        D    IVH-IVH-INV-HDR.IVH-A1099-RPRT-I
D  s        D    IVH-IVH-INV-HDR.IVH-TRANS-DT  y        D    IVH-IVH
-INV-HDR.IVH-CNCL-IND  �        D    IVH-IVH-INV-HDR.IVH-CNCL-DT  �  	 
     U    IVH-IVH-INV-HDR.IVH-CHECKS-WRITTEN-QTY  �        D    IVH-IVH-
INV-HDR.IVH-VNDR-INV-DT  {        D    IVH-IVH-INV-HDR.IVH-DOC-REF-NBR  
F        D    IVH-IVH-INV-HDR.IVH-MAILCODE  �        D    IVH-IVH-INV-
HDR.IVH-SUB-DOC-TYP  K        D    IVH-IVH-INV-HDR.IVH-ORGN-CD  Q    
   U    IVH-IVH-INV-HDR.IVH-IDC-APPL-AMT  �       U    IVH-IVH-INV-HDR.
IVH-IDC-ACTL-AMT  U        D    IVH-IVH-INV-HDR.IVH-VDR-I-SETF1  �    
    U    IVH-IVH-INV-HDR.IVH-FK1-VDR-IREF-ID  0        D    IVH-IVH-INV-
HDR.IVH-FK1-VDR-ENTPSN-IND  6        D    IVH-IVH-INV-HDR.IVH-VDR-V-SETF2 
 �        U    IVH-IVH-INV-HDR.IVH-FK2-VDR-IREF-ID          D    IV
H-IVH-INV-HDR.IVH-FK2-VDR-ENTPSN-IND          D    IVH-IVH-INV-HDR.IVH-VD
R-SET-TS          D    IVH-IVH-INV-HDR.IVH-IDX-INV-SETF          D 
   IVH-IVH-INV-HDR.IVH-RSV-SETF          D    IVH-IVH-INV-HDR.IVH-FK-RSV
-RSBLE-ID          D    IVH-IVH-INV-HDR.IVH-RSV-SET-TS  ��       D  
  IVH-IVH-INV-HDR.IVH-DOC-NBR
DBRM  m          �SELECT USER_CD , LAST_ACTVY_DT , ITEM_NBR , CMDTY_CD , O
PN_CLS_HLD_IND , ACCPT_QTY , ACCPT_UNIT_PRC , PO_NBR , CMDTY_DESC , UNIT_MEA_CD
, DTL_CNTR_ACCT , DTL_ERROR_IND , DSCNT_AMT , TAX_AMT , APRVD_QTY , APRVD_UNT_PR
C , INVD_QTY , INVD_UNIT_PRC , PO_ITEM_NBR , TLRNC_OVRD_IND , PREV_PAID_AMT , FK
_IVH_DOC_NBR , POD_SETF , FK_POD_PO_NBR , FK_POD_CHG_SEQ_NBR , FK_POD_ITEM_NBR ,
 POD_SET_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H FROM IVD_INV_DTL_V WHERE FK_IVH_DOC_NBR = : H AND ITEM_NBR = :
H WITH UR      �        D    IVD-IVD-INV-DTL.IVD-USER-CD  �        	D
    IVD-IVD-INV-DTL.IVD-LAST-ACTVY-DT  o        U    IVD-IVD-INV-DTL.IVD-
ITEM-NBR  �        D    IVD-IVD-INV-DTL.IVD-CMDTY-CD  s        D    
IVD-IVD-INV-DTL.IVD-OPN-CLS-HLD-IND  y       U    IVD-IVD-INV-DTL.IVD-ACC
PT-QTY  �       U    IVD-IVD-INV-DTL.IVD-ACCPT-UNIT-PRC  �        D 
   IVD-IVD-INV-DTL.IVD-PO-NBR  �        D    IVD-IVD-INV-DTL.IVD-CMDTY-DE
SC  {        D    IVD-IVD-INV-DTL.IVD-UNIT-MEA-CD  F        U    IVD
-IVD-INV-DTL.IVD-DTL-CNTR-ACCT  �        D    IVD-IVD-INV-DTL.IVD-DTL-ERRO
R-IND  K       U    IVD-IVD-INV-DTL.IVD-DSCNT-AMT  Q       U    IV
D-IVD-INV-DTL.IVD-TAX-AMT  �       U    IVD-IVD-INV-DTL.IVD-APRVD-QTY  
U       U    IVD-IVD-INV-DTL.IVD-APRVD-UNT-PRC  �       U    IVD-IVD
-INV-DTL.IVD-INVD-QTY  0       U    IVD-IVD-INV-DTL.IVD-INVD-UNIT-PRC  
6        U    IVD-IVD-INV-DTL.IVD-PO-ITEM-NBR  �        D    IVD-IVD-I
NV-DTL.IVD-TLRNC-OVRD-IND         U    IVD-IVD-INV-DTL.IVD-PREV-PAID-AMT
          D    IVD-IVD-INV-DTL.IVD-FK-IVH-DOC-NBR          D    IV
D-IVD-INV-DTL.IVD-POD-SETF          D    IVD-IVD-INV-DTL.IVD-FK-POD-PO-NB
R          D    IVD-IVD-INV-DTL.IVD-FK-POD-CHG-SEQ-NBR          U  
  IVD-IVD-INV-DTL.IVD-FK-POD-ITEM-NBR          D    IVD-IVD-INV-DTL.IVD-
POD-SET-TS  ��       D    IVD-IVD-INV-DTL.IVD-FK-IVH-DOC-NBR  ��      
 U    IVD-IVD-INV-DTL.IVD-ITEM-NBR
DBRM  h     '     SELECT USER_CD , LAST_ACTVY_DT , SEQ_NBR , NSF_OVRDE , A
DDL_CHRG_PCT , INVD_AMT , DSCNT_AMT , LIQDTN_IND , COA_CD , ACCT_INDX_CD , FUND_
CD , ORGN_CD , ACCT_CD , PRGRM_CD , ACTVY_CD , LCTN_CD , ACCT_ERROR_IND , FSCL_Y
R , BANK_ACCT_CD , APRVD_AMT , TAX_AMT , INVD_PCT , DSCNT_PCT , ADDL_CHRG , INCM
_TYP_SEQ_NBR , PAID_AMT , RULE_CLS_CD , DSCNT_RULE_CLS , TAX_RULE_CLS , ADDL_CHR
G_RUL_CLS , PSTNG_PRD , PRJCT_CD , PO_NBR , PO_ITEM_NBR , PO_SEQ_NBR , FK_IVD_DO
C_NBR , FK_IVD_ITEM_NBR INTO : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H ,
 : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H FROM IVA_INV_ACCT_V WHERE FK_IVD_DOC_NBR = : H AND FK_IVD_ITEM_NBR = :
H AND SEQ_NBR = : H WITH UR      W        D    IVA-IVA-INV-ACCT.IVA-USER-
CD  �        	D    IVA-IVA-INV-ACCT.IVA-LAST-ACTVY-DT  2        U    
IVA-IVA-INV-ACCT.IVA-SEQ-NBR  8        D    IVA-IVA-INV-ACCT.IVA-NSF-OVRDE
  �       U    IVA-IVA-INV-ACCT.IVA-ADDL-CHRG-PCT         U    IV
A-IVA-INV-ACCT.IVA-INVD-AMT         U    IVA-IVA-INV-ACCT.IVA-DSCNT-AMT 
         D    IVA-IVA-INV-ACCT.IVA-LIQDTN-IND          D    IVA-IV
A-INV-ACCT.IVA-COA-CD          D    IVA-IVA-INV-ACCT.IVA-ACCT-INDX-CD  
        D    IVA-IVA-INV-ACCT.IVA-FUND-CD          D    IVA-IVA-INV-
ACCT.IVA-ORGN-CD          D    IVA-IVA-INV-ACCT.IVA-ACCT-CD          
D    IVA-IVA-INV-ACCT.IVA-PRGRM-CD          D    IVA-IVA-INV-ACCT.IVA-
ACTVY-CD           D    IVA-IVA-INV-ACCT.IVA-LCTN-CD  �        D    
IVA-IVA-INV-ACCT.IVA-ACCT-ERROR-IND  <        D    IVA-IVA-INV-ACCT.IVA-FS
CL-YR  �        D    IVA-IVA-INV-ACCT.IVA-BANK-ACCT-CD  �       U   
 IVA-IVA-INV-ACCT.IVA-APRVD-AMT  ;       U    IVA-IVA-INV-ACCT.IVA-TAX-A
MT  �       U    IVA-IVA-INV-ACCT.IVA-INVD-PCT  �       U    IVA-I
VA-INV-ACCT.IVA-DSCNT-PCT  �       U    IVA-IVA-INV-ACCT.IVA-ADDL-CHRG  
�        U    
IVA-IVA-INV-ACCT.IVA-INCM-TYP-SEQ-NBR  @       U    IV
A-IVA-INV-ACCT.IVA-PAID-AMT  b        D    IVA-IVA-INV-ACCT.IVA-RULE-CLS-C
D  h        D    IVA-IVA-INV-ACCT.IVA-DSCNT-RULE-CLS  �        D    
IVA-IVA-INV-ACCT.IVA-TAX-RULE-CLS  m        D    IVA-IVA-INV-ACCT.IVA-ADDL
-CHRG-RUL-CLS  �        D    IVA-IVA-INV-ACCT.IVA-PSTNG-PRD  �        
D    IVA-IVA-INV-ACCT.IVA-PRJCT-CD  w        D    IVA-IVA-INV-ACCT.IVA-P
O-NBR  �        U    IVA-IVA-INV-ACCT.IVA-PO-ITEM-NBR  �        U    
IVA-IVA-INV-ACCT.IVA-PO-SEQ-NBR  �        D    IVA-IVA-INV-ACCT.IVA-FK-IV
D-DOC-NBR  �        U    IVA-IVA-INV-ACCT.IVA-FK-IVD-ITEM-NBR  ��      
 D    IVA-IVA-INV-ACCT.IVA-FK-IVD-DOC-NBR  �       U    IVA-IVA-INV-AC
CT.IVA-FK-IVD-ITEM-NBR  �       U    IVA-IVA-INV-ACCT.IVA-SEQ-NBR
DBRM              CLOSE R4150_INV_HDR_N     
DBRM        �      CLOSE S_4150_4151_N     
DBRM        <      CLOSE S_4150_4151_N     
DBRM        $      CLOSE S_4151_4152_N     
DBRM        �      CLOSE S_4151_4152_N     
DBRM         :      COMMIT WORK     
DBRM   
      t      ROLLBACK     
DBRM   
      3      ROLLBACK     
