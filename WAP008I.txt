DBRM   �DEVHV   WAP008I ��LY 2                                     1  �NK
                                                                                
DBRM  j       �     �DECLARE APH_APRVL_HDR_V TABLE ( USER_CD CHAR ( 8 ) NOT N
ULL , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , UNVRS_CD CHAR ( 2 ) NOT NULL , FSCL_YR
 CHAR ( 2 ) NOT NULL , SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , DOC_NBR CHAR ( 8 ) N
OT NULL , CHNG_SEQ_NBR CHAR ( 3 ) NOT NULL , APRVL_IND CHAR ( 1 ) NOT NULL , APR
VL_ID CHAR ( 8 ) NOT NULL , DOC_AMT DECIMAL ( 12 , 2 ) NOT NULL , USER_ID CHAR (
 8 ) NOT NULL , APV_TPLT_CD CHAR ( 3 ) NOT NULL , ACTG_PRD CHAR ( 2 ) NOT NULL ,
 DOC_DT DATE NOT NULL , PURGE_DT DATE NOT NULL , IDX_UNAPRVD_SETF CHAR ( 1 ) NOT
 NULL , IDX_APRVL_SETF CHAR ( 1 ) NOT NULL , IDX_APRVL_TS TIMESTAMP NOT NULL , A
PL_SETF CHAR ( 1 ) NOT NULL , FK_APL_SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , FK_APL
_USER_ID CHAR ( 8 ) NOT NULL , FK_APL_APV_TPLT_CD CHAR ( 3 ) NOT NULL , FK_APL_L
VL_SEQ_NBR DECIMAL ( 4 , 0 ) NOT NULL , APL_SET_TS TIMESTAMP NOT NULL , SCIQ_BAT
CH_ID CHAR ( 25 ) NOT NULL )     
DBRM  8       N     �DECLARE IVH_INV_HDR_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , DOC_NBR CHAR ( 8 ) NOT NULL , PYMT_DUE_DT DATE NOT
 NULL WITH DEFAULT , BANK_ACCT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , SALES_USE_T
AX_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , A1099_IND CHAR ( 1 ) NOT NULL WITH DEF
AULT , CRDT_MEMO CHAR ( 1 ) NOT NULL WITH DEFAULT , CHECK_GRPNG_IND CHAR ( 1 ) N
OT NULL WITH DEFAULT , CMPLT_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , RSBLE_IND CH
AR ( 1 ) NOT NULL WITH DEFAULT , ADDL_CHRG DECIMAL ( 12 , 2 ) NOT NULL WITH DEFA
ULT , ADJMT_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ADJMT_DT DATE NOT NULL WITH DE
FAULT , OPN_PD_HLD_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , INVD_AMT DECIMAL ( 12
, 2 ) NOT NULL WITH DEFAULT , APRVL_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , HDR_E
RROR_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , HDR_CNTR_DTL DECIMAL ( 4 ) NOT NULL
WITH DEFAULT , HDR_CNTR_ACCT DECIMAL ( 4 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , PO_NBR CHAR ( 8 ) NOT NULL WITH DEFAULT , DSCNT
_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , VNDR_INV_NBR CHAR ( 9 ) NOT NULL WITH DEF
AULT , TAX_RATE_CD CHAR ( 3 ) NOT NULL WITH DEFAULT , VNDR_ID_DGT_ONE CHAR ( 1 )
 NOT NULL WITH DEFAULT , VNDR_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , INC
M_TYP_SEQ_NBR DECIMAL ( 4 ) NOT NULL WITH DEFAULT , A1099_RPRT_ID CHAR ( 9 ) NOT
 NULL WITH DEFAULT , TRANS_DT DATE NOT NULL WITH DEFAULT , CNCL_IND CHAR ( 1 ) N
OT NULL WITH DEFAULT , CNCL_DT DATE NOT NULL WITH DEFAULT , CHECKS_WRITTEN_QTY D
ECIMAL ( 5 ) NOT NULL WITH DEFAULT , VNDR_INV_DT DATE NOT NULL WITH DEFAULT , DO
C_REF_NBR CHAR ( 10 ) NOT NULL WITH DEFAULT , MAILCODE CHAR ( 6 ) NOT NULL WITH
DEFAULT , SUB_DOC_TYP CHAR ( 3 ) NOT NULL WITH DEFAULT , ORGN_CD CHAR ( 4 ) NOT
NULL WITH DEFAULT , IDC_APPL_AMT DECIMAL ( 7 , 2 ) NOT NULL WITH DEFAULT , IDC_A
CTL_AMT DECIMAL ( 7 , 2 ) NOT NULL WITH DEFAULT , VDR_I_SETF1 CHAR ( 1 ) NOT NUL
L WITH DEFAULT , FK1_VDR_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK1_VDR_E
NTPSN_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , VDR_V_SETF2 CHAR ( 1 ) NOT NULL WIT
H DEFAULT , FK2_VDR_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK2_VDR_ENTPSN
_IND CHAR ( 1 ) NOT NULL WITH DEFAULT , VDR_SET_TS TIMESTAMP NOT NULL WITH DEFAU
LT , IDX_INV_SETF CHAR ( 1 ) NOT NULL WITH DEFAULT , RSV_SETF CHAR ( 1 ) NOT NUL
L WITH DEFAULT , FK_RSV_RSBLE_ID CHAR ( 3 ) NOT NULL WITH DEFAULT , RSV_SET_TS T
IMESTAMP NOT NULL WITH DEFAULT , ACRL_FSCL_YR CHAR ( 2 ) NOT NULL )     
DBRM   D           �SELECT CHAR ( DATE ( : H ) , ISO ) INTO : H FROM SYSIBM
. SYSDUMMY1       �       D    WS-MISCELLANEOUS.WS-DB2-DATE           
D    WS-MISCELLANEOUS.WS-ISO-DATE
DBRM  9     �     �DECLARE APH-CURSOR CURSOR FOR SELECT DOC_NBR , SCIQ_BATC
H_ID , APL_SET_TS FROM APH_APRVL_HDR_V WHERE FK_APL_SEQ_NBR = : H AND FK_APL_USE
R_ID = : H AND FK_APL_APV_TPLT_CD = : H AND FK_APL_LVL_SEQ_NBR = : H AND DATE (
APL_SET_TS ) BETWEEN : H AND : H AND DOC_NBR >= : H AND DOC_NBR LIKE '8%' ORDER
BY DOC_NBR OPTIMIZE FOR 201 ROWS FOR FETCH ONLY       ��       U    APH-AP
H-APRVL-HDR.APH-FK-APL-SEQ-NBR   ��       D    APH-APH-APRVL-HDR.APH-FK-APL
-USER-ID   [�       D    APH-APH-APRVL-HDR.APH-FK-APL-APV-TPLT-CD   ��  
     U    APH-APH-APRVL-HDR.APH-FK-APL-LVL-SEQ-NBR   ��       D    WS-MIS
CELLANEOUS.WS-FROM-DT   6�       D    WS-MISCELLANEOUS.WS-THRU-DT  �   
    D    APH-APH-APRVL-HDR.APH-DOC-NBR
DBRM  �     �      OPEN APH-CURSOR        �       U    APH-APH-APRVL-
HDR.APH-FK-APL-SEQ-NBR    �       D    APH-APH-APRVL-HDR.APH-FK-APL-USER-ID
    �       D    APH-APH-APRVL-HDR.APH-FK-APL-APV-TPLT-CD    �       U 
   APH-APH-APRVL-HDR.APH-FK-APL-LVL-SEQ-NBR    �       D    WS-MISCELLANEO
US.WS-FROM-DT    �       D    WS-MISCELLANEOUS.WS-THRU-DT    �       D 
   APH-APH-APRVL-HDR.APH-DOC-NBR
DBRM   U     �      FETCH APH-CURSOR INTO : H , : H , : H               
D    APH-APH-APRVL-HDR.APH-DOC-NBR           D    APH-APH-APRVL-HDR.APH
-SCIQ-BATCH-ID           D    APH-APH-APRVL-HDR.APH-APL-SET-TS
DBRM        �      CLOSE APH-CURSOR     
DBRM   �     u      &SELECT IVH . INVD_AMT INTO : H FROM IVH_INV_HDR_V IVH WH
ERE IVH . DOC_NBR = : H              U    IVH-IVH-INV-HDR.IVH-INVD-AMT 
  (�       D    IVH-IVH-INV-HDR.IVH-DOC-NBR
