DBRM   �APCPSB  FSYDT14 o�W�� 2                                     1  �NK
                                                                                
DBRM        V      �DECLARE FCC_KEEP_LONGTERM TABLE ( FCC_EIBTRMID CHAR ( 4
) NOT NULL , FCC_KEEP_LONG_ID CHAR ( 16 ) NOT NULL , FCC_KEEP_LONG_TYPE CHAR ( 8
 ) NOT NULL , FCC_KEEP_LONG_NAME CHAR ( 16 ) NOT NULL , FCC_KEEP_LONG_DATA VARCH
AR ( 3900 ) NOT NULL )     
DBRM  ;      �     �DECLARE YNP_NAV_PRF_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , SCRTY_UNIT CHAR ( 4 ) NOT NULL , PRFL_CD CHAR ( 8
) NOT NULL , LONG_DESC CHAR ( 30 ) NOT NULL WITH DEFAULT , SHORT_DESC CHAR ( 10
) NOT NULL WITH DEFAULT )     
DBRM  8      d     �DECLARE YUS_SEC_USR_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , SCRTY_USER_CD CHAR ( 8 ) NOT NULL , SHORT_NAME CHA
R ( 10 ) NOT NULL WITH DEFAULT , FULL_NAME CHAR ( 35 ) NOT NULL WITH DEFAULT , S
SA_FLAG CHAR ( 1 ) NOT NULL WITH DEFAULT , USA_FLAG CHAR ( 1 ) NOT NULL WITH DEF
AULT , SCRTY_UNIT CHAR ( 4 ) NOT NULL WITH DEFAULT , FULL_NAME_UPPER CHAR ( 35 )
 NOT NULL WITH DEFAULT , YTH_SCRN_SETF CHAR ( 1 ) NOT NULL WITH DEFAULT , FK1_YT
H_SCRTY_UNIT CHAR ( 4 ) NOT NULL WITH DEFAULT , FK1_YTH_TMPLT_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , YTH_ELEM_SETF CHAR ( 1 ) NOT NULL WITH DEFAULT , FK2_YTH_SCR
TY_UNIT CHAR ( 4 ) NOT NULL WITH DEFAULT , FK2_YTH_TMPLT_CD CHAR ( 8 ) NOT NULL
WITH DEFAULT , YTH_CONT_SETF CHAR ( 1 ) NOT NULL WITH DEFAULT , FK3_YTH_SCRTY_UN
IT CHAR ( 4 ) NOT NULL WITH DEFAULT , FK3_YTH_TMPLT_CD CHAR ( 8 ) NOT NULL WITH
DEFAULT , INDX_TS TIMESTAMP NOT NULL WITH DEFAULT )     
DBRM  T      �     GDECLARE YUT_SEC_UNT_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , SCRTY_UNIT CHAR ( 4 ) NOT NULL , SHORT_DESC CHAR (
 10 ) NOT NULL WITH DEFAULT , YSP_SCRN_SETF1 CHAR ( 1 ) NOT NULL WITH DEFAULT ,
FK1_YSP_SCRTY_PRFL CHAR ( 8 ) NOT NULL WITH DEFAULT , YSP_SCRN_SET_TS1 TIMESTAMP
 NOT NULL WITH DEFAULT , YSP_ELEM_SETF2 CHAR ( 1 ) NOT NULL WITH DEFAULT , FK2_Y
SP_SCRTY_PRFL CHAR ( 8 ) NOT NULL WITH DEFAULT , YSP_ELEM_SET_TS2 TIMESTAMP NOT
NULL WITH DEFAULT , YSP_CONT_SETF3 CHAR ( 1 ) NOT NULL WITH DEFAULT , FK3_YSP_SC
RTY_PRFL CHAR ( 8 ) NOT NULL WITH DEFAULT , YSP_CONT_SET_TS3 TIMESTAMP NOT NULL
WITH DEFAULT )     
DBRM  �      U     �DECLARE YUP_USR_PRF_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , PRFL_CD CHAR (
 8 ) NOT NULL WITH DEFAULT , FK_YNP_SCRTY_UNIT CHAR ( 4 ) NOT NULL , FK_YNP_PRFL
_CD CHAR ( 8 ) NOT NULL , YNP_SET_TS TIMESTAMP NOT NULL , FK_YUS_SCRTY_USRCD CHA
R ( 8 ) NOT NULL WITH DEFAULT )     
DBRM  k            �DECLARE YTH_UNT_TMP_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , SCRTY_UNIT CHAR ( 4 ) NOT NULL , TMPLT_CD CHAR ( 8
 ) NOT NULL , TMPLT_TYP CHAR ( 1 ) NOT NULL WITH DEFAULT , LONG_DESC CHAR ( 30 )
 NOT NULL WITH DEFAULT , FK_YUT_SCRTY_UNIT CHAR ( 4 ) NOT NULL WITH DEFAULT )   
  
DBRM  o           :DECLARE YTP_TMP_PRF_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , PRFL_CD CHAR (
 8 ) NOT NULL WITH DEFAULT , FK_YNP_SCRTY_UNIT CHAR ( 4 ) NOT NULL , FK_YNP_PRFL
_CD CHAR ( 8 ) NOT NULL , YNP_SET_TS TIMESTAMP NOT NULL , FK_YTH_SCRTY_UNIT CHAR
 ( 4 ) NOT NULL WITH DEFAULT , FK_YTH_TMPLT_CD CHAR ( 8 ) NOT NULL WITH DEFAULT
)     
DBRM  �           �DECLARE YNS_NAV_SCN_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , SCRTY_UNIT CHAR ( 4 ) NOT NULL WITH DEFAULT , PRFL
_CD CHAR ( 8 ) NOT NULL WITH DEFAULT , DLG_CD CHAR ( 8 ) NOT NULL WITH DEFAULT ,
 MODULE_CD CHAR ( 8 ) NOT NULL WITH DEFAULT , SCRN_CD CHAR ( 8 ) NOT NULL WITH D
EFAULT , DFLT_MODULE_CD CHAR ( 8 ) NOT NULL WITH DEFAULT , DFLT_SCRN_CD CHAR ( 8
 ) NOT NULL WITH DEFAULT , FK_YNP_SCRTY_UNT CHAR ( 4 ) NOT NULL , FK_YNP_PRFL_CD
 CHAR ( 8 ) NOT NULL , YNP_SET_TS TIMESTAMP NOT NULL , FK1_YSC_MODULE_CD CHAR (
8 ) NOT NULL WITH DEFAULT , FK1_YSC_SCRN_CD CHAR ( 8 ) NOT NULL WITH DEFAULT , Y
SC_SET_TS TIMESTAMP NOT NULL WITH DEFAULT , YSC_DFLT_SETF1 CHAR ( 1 ) NOT NULL W
ITH DEFAULT , FK2_YSC_MODULE_CD CHAR ( 8 ) NOT NULL WITH DEFAULT , FK2_YSC_SCRN_
CD CHAR ( 8 ) NOT NULL WITH DEFAULT , YSC_DFLT_SETF2 TIMESTAMP NOT NULL WITH DEF
AULT )     
DBRM        �     DECLARE MSL_MSG_LINE_V TABLE ( CMT_ID DECIMAL ( 8 ) NOT
NULL WITH DEFAULT , MDRDEST1 CHAR ( 8 ) NOT NULL WITH DEFAULT , MDRDEST2 CHAR (
8 ) NOT NULL WITH DEFAULT , MDROSDSC CHAR ( 2 ) NOT NULL WITH DEFAULT , MDROSRTC
 CHAR ( 2 ) NOT NULL WITH DEFAULT , MDRDSTID CHAR ( 8 ) NOT NULL WITH DEFAULT ,
MDRSEVCD CHAR ( 1 ) NOT NULL WITH DEFAULT , MDRTEXTL DECIMAL ( 4 ) NOT NULL WITH
 DEFAULT , MSG_1 CHAR ( 66 ) NOT NULL WITH DEFAULT , MSG_2 CHAR ( 66 ) NOT NULL
WITH DEFAULT , FK_MSG_MSG_KEY CHAR ( 8 ) NOT NULL , MSG_TS TIMESTAMP NOT NULL )
    
DBRM  �     s     ODECLARE S_8013_8016_N CURSOR FOR SELECT USER_CD , LAST_A
CTVY_DT , UNVRS_CD , SCRTY_UNIT , PRFL_CD , DLG_CD , MODULE_CD , SCRN_CD , DFLT_
MODULE_CD , DFLT_SCRN_CD , FK_YNP_SCRTY_UNT , FK_YNP_PRFL_CD , YNP_SET_TS , FK1_
YSC_MODULE_CD , FK1_YSC_SCRN_CD , YSC_SET_TS , YSC_DFLT_SETF1 , FK2_YSC_MODULE_C
D , FK2_YSC_SCRN_CD , YSC_DFLT_SETF2 FROM YNS_NAV_SCN_V WHERE FK_YNP_SCRTY_UNT =
 : H AND FK_YNP_PRFL_CD = : H AND YNP_SET_TS > : H ORDER BY YNP_SET_TS ASC OPTIM
IZE FOR 1 ROW     � :�       D    DBLINK-S-8013-8016-KEY-O.DBLINK-S-8013-8
016-01 � l�       D    DBLINK-S-8013-8016-KEY-O.DBLINK-S-8013-8016-02 � y
�       D    DBLINK-S-8013-8016-KEY-S.DBLINK-S-8013-8016-03
DBRM       D     
DECLARE S_INDX_PRFL_CODE_N CURSOR FOR SELECT USER_CD , L
AST_ACTVY_DT , UNVRS_CD , SCRTY_UNIT , PRFL_CD , LONG_DESC , SHORT_DESC FROM YNP
_NAV_PRF_V WHERE ( ( SCRTY_UNIT = : H AND PRFL_CD > : H ) OR ( SCRTY_UNIT > : H
) ) ORDER BY SCRTY_UNIT ASC , PRFL_CD ASC OPTIMIZE FOR 1 ROW     +  ��       
D    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01 +  ]�       D
    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-02 +  N�       D  
  DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01
DBRM       �     DECLARE S_INDX_PRFL_CODE_P CURSOR FOR SELECT USER_CD , L
AST_ACTVY_DT , UNVRS_CD , SCRTY_UNIT , PRFL_CD , LONG_DESC , SHORT_DESC FROM YNP
_NAV_PRF_V WHERE ( ( SCRTY_UNIT = : H AND PRFL_CD < : H ) OR ( SCRTY_UNIT < : H
) ) ORDER BY SCRTY_UNIT DESC , PRFL_CD DESC OPTIMIZE FOR 1 ROW     +  ��      
 D    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01 +  ]�       
D    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-02 +  N�       D
    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01
DBRM             �SELECT MSG_1 , MSG_2 INTO : H , : H FROM MSL_MSG_LINE_V
WHERE FK_MSG_MSG_KEY = : H               �D    MSL-MSL-MSG-LINE.MSL-MSG-1 
          �D    MSL-MSL-MSG-LINE.MSL-MSG-2   &�       D    MSL-MSL-MSG
-LINE.MSL-FK-MSG-MSG-KEY
DBRM   �     �      OPEN S_8013_8016_N     �   �       D    DBLINK-S-80
13-8016-KEY-O.DBLINK-S-8013-8016-01 �   �       D    DBLINK-S-8013-8016-KEY-
O.DBLINK-S-8013-8016-02 �   �       D    DBLINK-S-8013-8016-KEY-S.DBLINK-S-8
013-8016-03
DBRM  �     *      �FETCH S_8013_8016_N INTO : H , : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H               D    YNS-YNS-NAV-SCN.YNS-USER-CD           	D   
 YNS-YNS-NAV-SCN.YNS-LAST-ACTVY-DT           D    YNS-YNS-NAV-SCN.YNS-UNV
RS-CD           D    YNS-YNS-NAV-SCN.YNS-SCRTY-UNIT           D    Y
NS-YNS-NAV-SCN.YNS-PRFL-CD           D    YNS-YNS-NAV-SCN.YNS-DLG-CD   
        D    YNS-YNS-NAV-SCN.YNS-MODULE-CD   �        D    YNS-YNS-NAV-S
CN.YNS-SCRN-CD   �        D    YNS-YNS-NAV-SCN.YNS-DFLT-MODULE-CD   &    
    D    YNS-YNS-NAV-SCN.YNS-DFLT-SCRN-CD   �        D    YNS-YNS-NAV-SC
N.YNS-FK-YNP-SCRTY-UNT   *        D    YNS-YNS-NAV-SCN.YNS-FK-YNP-PRFL-CD 
  �        D    YNS-YNS-NAV-SCN.YNS-YNP-SET-TS   �        D    
YNS-YNS-
NAV-SCN.YNS-FK1-YSC-MODULE-CD   >        D    YNS-YNS-NAV-SCN.YNS-FK1-YSC-S
CRN-CD   �        D    YNS-YNS-NAV-SCN.YNS-YSC-SET-TS   :        D    
YNS-YNS-NAV-SCN.YNS-YSC-DFLT-SETF1   �        D    
YNS-YNS-NAV-SCN.YNS-FK2-
YSC-MODULE-CD   f        D    YNS-YNS-NAV-SCN.YNS-FK2-YSC-SCRN-CD   �    
    D    YNS-YNS-NAV-SCN.YNS-YSC-DFLT-SETF2
DBRM       �      OPEN S_INDX_PRFL_CODE_N     +   �       D    DBLINK
-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01 +   �       D    DBLINK-S
-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-02 +   �       D    DBLINK-S-I
NDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01
DBRM  H     C      �FETCH S_INDX_PRFL_CODE_N INTO : H , : H , : H , : H , :
H , : H , : H               D    YNP-YNP-NAV-PRF.YNP-USER-CD   
       
 	D    YNP-YNP-NAV-PRF.YNP-LAST-ACTVY-DT           D    YNP-YNP-NAV-PRF.
YNP-UNVRS-CD           D    YNP-YNP-NAV-PRF.YNP-SCRTY-UNIT           
D    YNP-YNP-NAV-PRF.YNP-PRFL-CD           D    YNP-YNP-NAV-PRF.YNP-LONG-
DESC   �        D    YNP-YNP-NAV-PRF.YNP-SHORT-DESC
DBRM             OPEN S_INDX_PRFL_CODE_P     +   �       D    DBLINK
-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01 +   �       D    DBLINK-S
-INDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-02 +   �       D    DBLINK-S-I
NDX-PRFL-CODE-KEY-U.DBLINK-S-INDX-PRFL-CODE-01
DBRM  H           �FETCH S_INDX_PRFL_CODE_P INTO : H , : H , : H , : H , :
H , : H , : H               D    YNP-YNP-NAV-PRF.YNP-USER-CD   
       
 	D    YNP-YNP-NAV-PRF.YNP-LAST-ACTVY-DT           D    YNP-YNP-NAV-PRF.
YNP-UNVRS-CD           D    YNP-YNP-NAV-PRF.YNP-SCRTY-UNIT           
D    YNP-YNP-NAV-PRF.YNP-PRFL-CD           D    YNP-YNP-NAV-PRF.YNP-LONG-
DESC   �        D    YNP-YNP-NAV-PRF.YNP-SHORT-DESC
DBRM   	    �      �SELECT USER_CD , LAST_ACTVY_DT , PRFL_CD , FK_YNP_SCRTY_
UNIT , FK_YNP_PRFL_CD , YNP_SET_TS , FK_YUS_SCRTY_USRCD INTO : H , : H , : H , :
 H , : H , : H , : H FROM YUP_USR_PRF_V WHERE FK_YUS_SCRTY_USRCD = : H AND PRFL_
CD = : H       �        D    YUP-YUP-USR-PRF.YUP-USER-CD   @        	D 
   YUP-YUP-USR-PRF.YUP-LAST-ACTVY-DT   b        D    YUP-YUP-USR-PRF.YUP-P
RFL-CD   h        D    
YUP-YUP-USR-PRF.YUP-FK-YNP-SCRTY-UNIT   �        
D    YUP-YUP-USR-PRF.YUP-FK-YNP-PRFL-CD   m        D    YUP-YUP-USR-PRF.Y
UP-YNP-SET-TS   �        D    YUP-YUP-USR-PRF.YUP-FK-YUS-SCRTY-USRCD �  ��
       D    DBLINK-S-8005-8015-KEY-O.DBLINK-S-8005-8015-01 �  ��       D  
  DBLINK-S-8005-8015-KEY-U.DBLINK-S-8005-8015-02
DBRM  [     l     SELECT USER_CD , LAST_ACTVY_DT , PRFL_CD , FK_YNP_SCRTY_
UNIT , FK_YNP_PRFL_CD , YNP_SET_TS , FK_YTH_SCRTY_UNIT , FK_YTH_TMPLT_CD INTO :
H , : H , : H , : H , : H , : H , : H , : H FROM YTP_TMP_PRF_V WHERE FK_YTH_SCRT
Y_UNIT = : H AND FK_YTH_TMPLT_CD = : H AND PRFL_CD = : H       g        D  
  YTP-YTP-TMP-PRF.YTP-USER-CD   �        	D    YTP-YTP-TMP-PRF.YTP-LAST-ACT
VY-DT   l        D    YTP-YTP-TMP-PRF.YTP-PRFL-CD   r        D    
YTP-
YTP-TMP-PRF.YTP-FK-YNP-SCRTY-UNIT   �        D    YTP-YTP-TMP-PRF.YTP-FK-YN
P-PRFL-CD   v        D    YTP-YTP-TMP-PRF.YTP-YNP-SET-TS   �        D  
  
YTP-YTP-TMP-PRF.YTP-FK-YTH-SCRTY-UNIT   �        D    YTP-YTP-TMP-PRF.YT
P-FK-YTH-TMPLT-CD �  S�       D    DBLINK-S-8040-8014-KEY-O.DBLINK-S-8040-80
14-01 �  ��       D    DBLINK-S-8040-8014-KEY-O.DBLINK-S-8040-8014-02 � �
       D    DBLINK-S-8040-8014-KEY-U.DBLINK-S-8040-8014-03
DBRM  �     M      CSELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , SCRTY_UNIT ,
 PRFL_CD , LONG_DESC , SHORT_DESC INTO : H , : H , : H , : H , : H , : H , : H F
ROM YNP_NAV_PRF_V WHERE SCRTY_UNIT = : H AND PRFL_CD = : H       -        D
    YNP-YNP-NAV-PRF.YNP-USER-CD   �        	D    YNP-YNP-NAV-PRF.YNP-LAST-A
CTVY-DT   %        D    YNP-YNP-NAV-PRF.YNP-UNVRS-CD   �        D    Y
NP-YNP-NAV-PRF.YNP-SCRTY-UNIT   �        D    YNP-YNP-NAV-PRF.YNP-PRFL-CD 
  =        D    YNP-YNP-NAV-PRF.YNP-LONG-DESC   d        D    YNP-YNP-N
AV-PRF.YNP-SHORT-DESC +  ��       D    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-
S-INDX-PRFL-CODE-01 +  {�       D    DBLINK-S-INDX-PRFL-CODE-KEY-U.DBLINK-S-
INDX-PRFL-CODE-02
DBRM  �          SSELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , SCRTY_USER_C
D , SHORT_NAME , FULL_NAME , SSA_FLAG , USA_FLAG , SCRTY_UNIT , FULL_NAME_UPPER
, YTH_SCRN_SETF , FK1_YTH_SCRTY_UNIT , FK1_YTH_TMPLT_CD , YTH_ELEM_SETF , FK2_YT
H_SCRTY_UNIT , FK2_YTH_TMPLT_CD , YTH_CONT_SETF , FK3_YTH_SCRTY_UNIT , FK3_YTH_T
MPLT_CD , INDX_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H FROM YUS_SEC_USR_V
WHERE SCRTY_USER_CD = : H  
             D    YUS-YUS-SEC-USR.YUS-USER-CD 
 �        	D    YUS-YUS-SEC-USR.YUS-LAST-ACTVY-DT  <        D    YUS-
YUS-SEC-USR.YUS-UNVRS-CD  �        D    YUS-YUS-SEC-USR.YUS-SCRTY-USER-CD 
 �        D    YUS-YUS-SEC-USR.YUS-SHORT-NAME  ;        D    YUS-YUS
-SEC-USR.YUS-FULL-NAME  �        D    YUS-YUS-SEC-USR.YUS-SSA-FLAG  �  
      D    YUS-YUS-SEC-USR.YUS-USA-FLAG  �        D    YUS-YUS-SEC-USR.
YUS-SCRTY-UNIT  �        D    YUS-YUS-SEC-USR.YUS-FULL-NAME-UPPER  @   
     D    YUS-YUS-SEC-USR.YUS-YTH-SCRN-SETF  b        D    YUS-YUS-SEC-
USR.YUS-FK1-YTH-SCRTY-UNIT  h        D    YUS-YUS-SEC-USR.YUS-FK1-YTH-TMPL
T-CD  �        D    YUS-YUS-SEC-USR.YUS-YTH-ELEM-SETF  m        D    
YUS-YUS-SEC-USR.YUS-FK2-YTH-SCRTY-UNIT  �        D    YUS-YUS-SEC-USR.YUS
-FK2-YTH-TMPLT-CD  �        D    YUS-YUS-SEC-USR.YUS-YTH-CONT-SETF  w  
      D    YUS-YUS-SEC-USR.YUS-FK3-YTH-SCRTY-UNIT  �        D    YUS-YU
S-SEC-USR.YUS-FK3-YTH-TMPLT-CD  �        D    YUS-YUS-SEC-USR.YUS-INDX-TS 
 ��       D    YUS-YUS-SEC-USR.YUS-SCRTY-USER-CD
DBRM  �     �      CSELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , SCRTY_UNIT ,
 PRFL_CD , LONG_DESC , SHORT_DESC INTO : H , : H , : H , : H , : H , : H , : H F
ROM YNP_NAV_PRF_V WHERE SCRTY_UNIT = : H AND PRFL_CD = : H       -        D
    YNP-YNP-NAV-PRF.YNP-USER-CD   �        	D    YNP-YNP-NAV-PRF.YNP-LAST-A
CTVY-DT   %        D    YNP-YNP-NAV-PRF.YNP-UNVRS-CD   �        D    Y
NP-YNP-NAV-PRF.YNP-SCRTY-UNIT   �        D    YNP-YNP-NAV-PRF.YNP-PRFL-CD 
  =        D    YNP-YNP-NAV-PRF.YNP-LONG-DESC   d        D    YNP-YNP-N
AV-PRF.YNP-SHORT-DESC   ��       D    YNP-YNP-NAV-PRF.YNP-SCRTY-UNIT   {�
       D    YNP-YNP-NAV-PRF.YNP-PRFL-CD
DBRM       �      �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , SCRTY_UNIT ,
 TMPLT_CD , TMPLT_TYP , LONG_DESC , FK_YUT_SCRTY_UNIT INTO : H , : H , : H , : H
 , : H , : H , : H , : H FROM YTH_UNT_TMP_V WHERE SCRTY_UNIT = : H AND TMPLT_CD
= : H       �        D    YTH-YTH-UNT-TMP.YTH-USER-CD   :        	D    
YTH-YTH-UNT-TMP.YTH-LAST-ACTVY-DT   �        D    YTH-YTH-UNT-TMP.YTH-UNVR
S-CD   f        D    YTH-YTH-UNT-TMP.YTH-SCRTY-UNIT   �        D    YT
H-YTH-UNT-TMP.YTH-TMPLT-CD   k        D    YTH-YTH-UNT-TMP.YTH-TMPLT-TYP  
 q        D    YTH-YTH-UNT-TMP.YTH-LONG-DESC   �        D    
YTH-YTH-UN
T-TMP.YTH-FK-YUT-SCRTY-UNIT   H�       D    YTH-YTH-UNT-TMP.YTH-SCRTY-UNIT 
  ��       D    YTH-YTH-UNT-TMP.YTH-TMPLT-CD
DBRM  �     0     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , SCRTY_UNIT ,
 SHORT_DESC , YSP_SCRN_SETF1 , FK1_YSP_SCRTY_PRFL , YSP_SCRN_SET_TS1 , YSP_ELEM_
SETF2 , FK2_YSP_SCRTY_PRFL , YSP_ELEM_SET_TS2 , YSP_CONT_SETF3 , FK3_YSP_SCRTY_P
RFL , YSP_CONT_SET_TS3 INTO : H , : H , : H , : H , : H , : H , : H , : H , : H
, : H , : H , : H , : H , : H FROM YUT_SEC_UNT_V WHERE SCRTY_UNIT = : H      
 5        D    YUT-YUT-SEC-UNT.YUT-USER-CD   �        	D    YUT-YUT-SEC-U
NT.YUT-LAST-ACTVY-DT          D    YUT-YUT-SEC-UNT.YUT-UNVRS-CD      
    D    YUT-YUT-SEC-UNT.YUT-SCRTY-UNIT          D    YUT-YUT-SEC-UNT.
YUT-SHORT-DESC          D    YUT-YUT-SEC-UNT.YUT-YSP-SCRN-SETF1      
    D    YUT-YUT-SEC-UNT.YUT-FK1-YSP-SCRTY-PRFL          D    YUT-YUT-
SEC-UNT.YUT-YSP-SCRN-SET-TS1  
        D    YUT-YUT-SEC-UNT.YUT-YSP-ELEM-S
ETF2          D    YUT-YUT-SEC-UNT.YUT-FK2-YSP-SCRTY-PRFL          
D    YUT-YUT-SEC-UNT.YUT-YSP-ELEM-SET-TS2          D    YUT-YUT-SEC-UNT.
YUT-YSP-CONT-SETF3          D    YUT-YUT-SEC-UNT.YUT-FK3-YSP-SCRTY-PRFL 
 �        D    YUT-YUT-SEC-UNT.YUT-YSP-CONT-SET-TS3  _�       D    YU
T-YUT-SEC-UNT.YUT-SCRTY-UNIT
DBRM        �      CLOSE S_8013_8016_N     
DBRM        _      CLOSE S_8013_8016_N     
DBRM        #      CLOSE S_INDX_PRFL_CODE_N     
DBRM        f      CLOSE S_INDX_PRFL_CODE_N     
DBRM        m      CLOSE S_INDX_PRFL_CODE_P     
DBRM        �      CLOSE S_INDX_PRFL_CODE_P     
