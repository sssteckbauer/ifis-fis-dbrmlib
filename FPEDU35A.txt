DBRM   �APCPSB  FPEDU35Ao�� 2                                     1  �NK
                                                                                
DBRM        �     DECLARE MSL_MSG_LINE_V TABLE ( CMT_ID DECIMAL ( 8 ) NOT
NULL WITH DEFAULT , MDRDEST1 CHAR ( 8 ) NOT NULL WITH DEFAULT , MDRDEST2 CHAR (
8 ) NOT NULL WITH DEFAULT , MDROSDSC CHAR ( 2 ) NOT NULL WITH DEFAULT , MDROSRTC
 CHAR ( 2 ) NOT NULL WITH DEFAULT , MDRDSTID CHAR ( 8 ) NOT NULL WITH DEFAULT ,
MDRSEVCD CHAR ( 1 ) NOT NULL WITH DEFAULT , MDRTEXTL DECIMAL ( 4 ) NOT NULL WITH
 DEFAULT , MSG_1 CHAR ( 66 ) NOT NULL WITH DEFAULT , MSG_2 CHAR ( 66 ) NOT NULL
WITH DEFAULT , FK_MSG_MSG_KEY CHAR ( 8 ) NOT NULL , MSG_TS TIMESTAMP NOT NULL )
    
DBRM  �      �     �DECLARE PRA_PRSN_ADR_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CH
AR ( 2 ) NOT NULL , END_DT DATE NOT NULL , START_DT DATE NOT NULL WITH DEFAULT ,
 LINE_ADR_1 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_2 CHAR ( 35 ) NOT NULL
WITH DEFAULT , LINE_ADR_3 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_4 CHAR (
35 ) NOT NULL WITH DEFAULT , CITY_NAME CHAR ( 18 ) NOT NULL WITH DEFAULT , TLPHN
_ID CHAR ( 14 ) NOT NULL WITH DEFAULT , CNTY_CD CHAR ( 4 ) NOT NULL WITH DEFAULT
 , STATE_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ZIP_CD CHAR ( 10 ) NOT NULL WITH
DEFAULT , FK_ZIP_CNTRY_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , LINE_ADR_1_UC CHAR
( 35 ) NOT NULL WITH DEFAULT , FAX_TLPHN_ID CHAR ( 10 ) NOT NULL WITH DEFAULT ,
EMADR_LINE CHAR ( 70 ) NOT NULL WITH DEFAULT , FAX_STOP CHAR ( 1 ) NOT NULL WITH
 DEFAULT , FK_PRS_IREF_ID DECIMAL ( 7 ) NOT NULL , ZIP_SETF CHAR ( 1 ) NOT NULL
WITH DEFAULT , FK_ZIP_ZIP_CD CHAR ( 10 ) NOT NULL WITH DEFAULT , ZIP_SET_TS TIME
STAMP NOT NULL WITH DEFAULT , IDX_PRSN_ADR1_TS TIMESTAMP NOT NULL WITH DEFAULT )
     
DBRM  .      l     DECLARE PPI_PRSN_PRVID_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , PRSN_ID_DGT_ONE CHAR ( 1 ) NOT NULL WITH DEFAUL
T , PRSN_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , CHANGE_DT DATE NOT NULL
, FK_PRS_IREF_ID DECIMAL ( 7 ) NOT NULL , PRS_SET_TS TIMESTAMP NOT NULL , FK_PCH
_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT , FK_PCH_CHG_DT DATE NOT NULL WITH
DEFAULT , FK_PCH_SET_TS TIMESTAMP NOT NULL WITH DEFAULT , PCH_SET_TS TIMESTAMP N
OT NULL WITH DEFAULT )     
DBRM  b      �     �DECLARE ENI_ENTY_ID_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , ENTY_ID_DGT_ONE CHAR ( 1 ) NOT NULL WITH DEFAULT ,
 ENTY_ID_LST_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , FK2_ECG_CHG_DT DATE NOT NUL
L , FK1_ENT_ENTY_ONE CHAR ( 1 ) NOT NULL , FK1_ENT_ENTY_NINE CHAR ( 9 ) NOT NULL
 , ENT_SET_TS TIMESTAMP NOT NULL , FK2_ECG_ENTY_ONE CHAR ( 1 ) NOT NULL WITH DEF
AULT , FK2_ECG_ENTY_NINE CHAR ( 9 ) NOT NULL WITH DEFAULT , FK2_ENT_ECG_SET_TS T
IMESTAMP NOT NULL WITH DEFAULT , ECG_SET_TS TIMESTAMP NOT NULL WITH DEFAULT )   
  
DBRM  8      �     �DECLARE TBH_TABL_HDR_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
 ( 2 ) NOT NULL WITH DEFAULT , APLCN_CD CHAR ( 8 ) NOT NULL , TABLE_CD CHAR ( 8
) NOT NULL , FNCTN_DESC CHAR ( 28 ) NOT NULL WITH DEFAULT , TBL_DTL_CD_LGTH DECI
MAL ( 1 ) NOT NULL WITH DEFAULT , DTL_CD_CLMN_LBL_1 CHAR ( 10 ) NOT NULL WITH DE
FAULT , DTL_CD_CLMN_LBL_2 CHAR ( 10 ) NOT NULL WITH DEFAULT , FLAG_1_LABEL_1 CHA
R ( 5 ) NOT NULL WITH DEFAULT , FLAG_1_LABEL_2 CHAR ( 5 ) NOT NULL WITH DEFAULT
, FLAG_1_VALUE_1 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_1_VALUE_2 CHAR ( 1 ) NO
T NULL WITH DEFAULT , FLAG_1_VALUE_3 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_1_V
ALUE_4 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_1_VALUE_5 CHAR ( 1 ) NOT NULL WIT
H DEFAULT , FLAG_1_DFLT_VALUE CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_2_LABEL_1
CHAR ( 5 ) NOT NULL WITH DEFAULT , FLAG_2_LABEL_2 CHAR ( 5 ) NOT NULL WITH DEFAU
LT , FLAG_2_VALUE_1 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_2_VALUE_2 CHAR ( 1 )
 NOT NULL WITH DEFAULT , FLAG_2_VALUE_3 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_
2_VALUE_4 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_2_VALUE_5 CHAR ( 1 ) NOT NULL
WITH DEFAULT , FLAG_2_DFLT_VALUE CHAR ( 1 ) NOT NULL WITH DEFAULT )     
DBRM  �           �DECLARE TBD_TABL_DTL_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , DTL_CD CHAR (
 8 ) NOT NULL , SHORT_DESC CHAR ( 10 ) NOT NULL WITH DEFAULT , LONG_DESC CHAR (
30 ) NOT NULL WITH DEFAULT , FLAG_1 CHAR ( 1 ) NOT NULL WITH DEFAULT , FLAG_2 CH
AR ( 1 ) NOT NULL WITH DEFAULT , ACTV_INATV_CD_FLAG CHAR ( 1 ) NOT NULL WITH DEF
AULT , START_SESN_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , START_YEAR_DT DECIMAL (
2 ) NOT NULL WITH DEFAULT , END_SESN_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , END_Y
EAR_DT DECIMAL ( 2 ) NOT NULL WITH DEFAULT , ALT_ACCESS1 CHAR ( 8 ) NOT NULL WIT
H DEFAULT , ALT_ACCESS2 CHAR ( 8 ) NOT NULL WITH DEFAULT , SUB_DATA CHAR ( 60 )
NOT NULL WITH DEFAULT , LGCL_DLT_FLAG CHAR ( 1 ) NOT NULL WITH DEFAULT , FK_TBH_
APLCN_CD CHAR ( 8 ) NOT NULL , FK_TBH_TBL_CD CHAR ( 8 ) NOT NULL , TBH_SET_AC1_S
ETF CHAR ( 1 ) NOT NULL WITH DEFAULT , TBH_SET_AC2_SETF CHAR ( 1 ) NOT NULL WITH
 DEFAULT )     
DBRM  x      �     �DECLARE ADT_ADR_TYP_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR
( 2 ) NOT NULL WITH DEFAULT , ADR_TYP_CD CHAR ( 2 ) NOT NULL , DSPLY_PRTY_SEQ DE
CIMAL ( 2 ) NOT NULL WITH DEFAULT , SHORT_DESC CHAR ( 10 ) NOT NULL WITH DEFAULT
 , LONG_DESC CHAR ( 30 ) NOT NULL WITH DEFAULT , RCRD_USED_FLAG CHAR ( 1 ) NOT N
ULL WITH DEFAULT )     
DBRM  
      �     DECLARE ZIP_ZIP_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL WI
TH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , FK_UNV_UNVRS_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , FK_STA_CNTRY_CD CHAR ( 2 ) NOT NULL , ZIP_CD CH
AR ( 10 ) NOT NULL , CITY_NAME CHAR ( 18 ) NOT NULL WITH DEFAULT , SYS_ADD_FLAG
CHAR ( 1 ) NOT NULL WITH DEFAULT , RCRD_USED_FLAG CHAR ( 1 ) NOT NULL WITH DEFAU
LT , FK_STA_STATE_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , STA_SETF CHAR ( 1 ) NOT
NULL WITH DEFAULT , STA_SET_TS TIMESTAMP NOT NULL WITH DEFAULT )     
DBRM  �      r     DECLARE CTY_COUNTRY_V TABLE ( USER_CD CHAR ( 8 ) NOT NUL
L WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , FK_UNV_UNVRS_C
D CHAR ( 2 ) NOT NULL WITH DEFAULT , CNTRY_CD CHAR ( 2 ) NOT NULL , FULL_NAME CH
AR ( 35 ) NOT NULL WITH DEFAULT , RCRD_USED_FLAG CHAR ( 1 ) NOT NULL WITH DEFAUL
T )     
DBRM  q      �     @DECLARE STA_STATE_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL
WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , FK_UNV_UNVRS_CD
CHAR ( 2 ) NOT NULL WITH DEFAULT , CNTRY_CD CHAR ( 2 ) NOT NULL , STATE_CD CHAR
( 2 ) NOT NULL , FULL_NAME CHAR ( 35 ) NOT NULL WITH DEFAULT , RCRD_USED_FLAG CH
AR ( 1 ) NOT NULL WITH DEFAULT , FK_CTY_CNTRY_CD CHAR ( 2 ) NOT NULL WITH DEFAUL
T )     
DBRM   �      J      �DECLARE SPI_LSTSYS_PID_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL , LAST_IREF_ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT )     
DBRM  �      1     �DECLARE PRS_PRSN_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL ,
 LAST_ACTVY_DT CHAR ( 5 ) NOT NULL , FK_UNV_UNVRS_CD CHAR ( 2 ) NOT NULL , IREF_
ID DECIMAL ( 7 , 0 ) NOT NULL , SSN_DGT_ONE CHAR ( 1 ) NOT NULL , SSN_LST_NINE C
HAR ( 9 ) NOT NULL , NAME_KEY CHAR ( 35 ) NOT NULL , NAME_SLTN_DESC CHAR ( 4 ) N
OT NULL , PRSN_FULL_NAME CHAR ( 55 ) NOT NULL , NAME_SFX_DESC CHAR ( 4 ) NOT NUL
L , BIRTH_DT DATE NOT NULL , GNDR_FLAG CHAR ( 1 ) NOT NULL , DCSD_FLAG CHAR ( 1
) NOT NULL , DCSD_DT DATE NOT NULL , ALT_ID CHAR ( 9 ) NOT NULL , PAN_NBR CHAR (
 4 ) NOT NULL , PAN_FLAG CHAR ( 1 ) NOT NULL , PAN_DT DATE NOT NULL , MRTL_CD CH
AR ( 1 ) NOT NULL , RLGN_CD CHAR ( 4 ) NOT NULL , ETHNC_CD CHAR ( 2 ) NOT NULL ,
 ARCHVD_DT DATE NOT NULL , UNV_SETF CHAR ( 1 ) NOT NULL , UNV_SET_TS TIMESTAMP N
OT NULL , IDX_SSN_TS TIMESTAMP NOT NULL , EMP_ID CHAR ( 9 ) NOT NULL , EMP_STATU
S CHAR ( 1 ) NOT NULL )     
DBRM   9            �DECLARE PCH_PRSN_CHG_V TABLE ( USER_CD CHAR ( 8 ) NOT NU
LL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , CHANGE_DT DAT
E NOT NULL , FK_PRS_IREF_ID DECIMAL ( 7 ) NOT NULL , PRS_SET_TS TIMESTAMP NOT NU
LL )     
DBRM  �      �     DECLARE PPN_PRSN_PRVNM_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL WITH DEFAULT , NAME_KEY CHAR ( 35 ) NOT NULL WITH DEFAULT , NA
ME_CD CHAR ( 1 ) NOT NULL WITH DEFAULT , FK_PCH_CHG_DT DATE NOT NULL , FK_PRS_IR
EF_ID DECIMAL ( 7 ) NOT NULL , PRS_SET_TS TIMESTAMP NOT NULL , FK_PCH_IREF_ID DE
CIMAL ( 7 ) NOT NULL WITH DEFAULT , FK_PCH_SET_TS TIMESTAMP NOT NULL WITH DEFAUL
T , PCH_SET_TS TIMESTAMP NOT NULL WITH DEFAULT , IDX_PRSN_NAME_TS TIMESTAMP NOT
NULL WITH DEFAULT )     
DBRM        `     DECLARE UNV_UNVRS_V TABLE ( USER_CD CHAR ( 8 ) NOT NULL
WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CHAR (
2 ) NOT NULL , FICE_CD DECIMAL ( 6 ) NOT NULL WITH DEFAULT , SHORT_NAME CHAR ( 1
0 ) NOT NULL WITH DEFAULT , FULL_NAME CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_A
DR_1 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_2 CHAR ( 35 ) NOT NULL WITH DE
FAULT , LINE_ADR_3 CHAR ( 35 ) NOT NULL WITH DEFAULT , LINE_ADR_4 CHAR ( 35 ) NO
T NULL WITH DEFAULT , TLPHN_ID CHAR ( 14 ) NOT NULL WITH DEFAULT , CITY_NAME CHA
R ( 18 ) NOT NULL WITH DEFAULT , STATE_CD CHAR ( 2 ) NOT NULL WITH DEFAULT , ZIP
_CD CHAR ( 10 ) NOT NULL WITH DEFAULT , CNTRY_CD CHAR ( 2 ) NOT NULL WITH DEFAUL
T , FK_SYS_SYS_CD CHAR ( 1 ) NOT NULL WITH DEFAULT , SYS_SET_TS TIMESTAMP NOT NU
LL WITH DEFAULT )     
DBRM        z     DECLARE LPI_LSTASGNPID_V TABLE ( USER_CD CHAR ( 8 ) NOT
NULL WITH DEFAULT , LAST_ACTVY_DT CHAR ( 5 ) NOT NULL WITH DEFAULT , UNVRS_CD CH
AR ( 2 ) NOT NULL , ALPHA_PREFIX CHAR ( 1 ) NOT NULL WITH DEFAULT , LST_ASSGND_P
ID DECIMAL ( 7 ) NOT NULL WITH DEFAULT )     
DBRM       �      �SELECT MSG_1 , MSG_2 INTO : H , : H FROM MSL_MSG_LINE_V
WHERE FK_MSG_MSG_KEY = : H               �D    MSL-MSL-MSG-LINE.MSL-MSG-1 
          �D    MSL-MSL-MSG-LINE.MSL-MSG-2   &�       D    MSL-MSL-MSG
-LINE.MSL-FK-MSG-MSG-KEY
DBRM       �     �INSERT INTO PRA_PRSN_ADR_V ( USER_CD , LAST_ACTVY_DT , A
DR_TYP_CD , END_DT , START_DT , LINE_ADR_1 , LINE_ADR_2 , LINE_ADR_3 , LINE_ADR_
4 , CITY_NAME , TLPHN_ID , CNTY_CD , STATE_CD , ZIP_CD , FK_ZIP_CNTRY_CD , LINE_
ADR_1_UC , FAX_TLPHN_ID , EMADR_LINE , FAX_STOP , FK_PRS_IREF_ID , ZIP_SETF , FK
_ZIP_ZIP_CD , ZIP_SET_TS , IDX_PRSN_ADR1_TS ) VALUES ( : H , : H , : H , : H , :
 H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
 , : H , : H , : H , : H , : H , : H )      -�       D    PRA-PRA-PRSN-AD
R.PRA-USER-CD  ��       	D    PRA-PRA-PRSN-ADR.PRA-LAST-ACTVY-DT  %�    
   D    PRA-PRA-PRSN-ADR.PRA-ADR-TYP-CD  ��       D    PRA-PRA-PRSN-ADR
.PRA-END-DT  ��       D    PRA-PRA-PRSN-ADR.PRA-START-DT  =�       D 
   PRA-PRA-PRSN-ADR.PRA-LINE-ADR-1  d�       D    PRA-PRA-PRSN-ADR.PRA-LI
NE-ADR-2  ��       D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-3  ��       D  
  PRA-PRA-PRSN-ADR.PRA-LINE-ADR-4  o�       D    PRA-PRA-PRSN-ADR.PRA-CIT
Y-NAME  ��       D    PRA-PRA-PRSN-ADR.PRA-TLPHN-ID  s�       D    P
RA-PRA-PRSN-ADR.PRA-CNTY-CD  y�       D    PRA-PRA-PRSN-ADR.PRA-STATE-CD 
 ��       D    PRA-PRA-PRSN-ADR.PRA-ZIP-CD  ��       D    PRA-PRA-PRS
N-ADR.PRA-FK-ZIP-CNTRY-CD  ��       D    PRA-PRA-PRSN-ADR.PRA-LINE-ADR-1-U
C  {�       D    PRA-PRA-PRSN-ADR.PRA-FAX-TLPHN-ID  F�       �D    PR
A-PRA-PRSN-ADR.PRA-EMADR-LINE  ��       D    PRA-PRA-PRSN-ADR.PRA-FAX-STOP
  K�       U    PRA-PRA-PRSN-ADR.PRA-FK-PRS-IREF-ID  Q�       D    P
RA-PRA-PRSN-ADR.PRA-ZIP-SETF  ��       D    PRA-PRA-PRSN-ADR.PRA-FK-ZIP-ZI
P-CD  U�       D    PRA-PRA-PRSN-ADR.PRA-ZIP-SET-TS  ��       D    
P
RA-PRA-PRSN-ADR.PRA-IDX-PRSN-ADR1-TS
DBRM       �      INSERT INTO ZIP_ZIP_V ( USER_CD , LAST_ACTVY_DT , FK_UNV
_UNVRS_CD , FK_STA_CNTRY_CD , ZIP_CD , CITY_NAME , SYS_ADD_FLAG , RCRD_USED_FLAG
 , FK_STA_STATE_CD , STA_SETF , STA_SET_TS ) VALUES ( : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H )       ��       D    ZIP-ZIP-ZIP.ZI
P-USER-CD   E�       	D    ZIP-ZIP-ZIP.ZIP-LAST-ACTVY-DT   ��       D   
 ZIP-ZIP-ZIP.ZIP-FK-UNV-UNVRS-CD   J�       D    ZIP-ZIP-ZIP.ZIP-FK-STA-CN
TRY-CD   P�       D    ZIP-ZIP-ZIP.ZIP-ZIP-CD   ��       D    ZIP-ZIP-
ZIP.ZIP-CITY-NAME   T�       D    ZIP-ZIP-ZIP.ZIP-SYS-ADD-FLAG   Z�      
 D    ZIP-ZIP-ZIP.ZIP-RCRD-USED-FLAG   ��       D    ZIP-ZIP-ZIP.ZIP-FK-
STA-STATE-CD   5�       D    ZIP-ZIP-ZIP.ZIP-STA-SETF   ��       D    
ZIP-ZIP-ZIP.ZIP-STA-SET-TS
DBRM  M           �UPDATE ADT_ADR_TYP_V SET USER_CD = : H , LAST_ACTVY_DT =
 : H , UNVRS_CD = : H , ADR_TYP_CD = : H , DSPLY_PRTY_SEQ = : H , SHORT_DESC = :
 H , LONG_DESC = : H , RCRD_USED_FLAG = : H WHERE ADR_TYP_CD = : H       �  
     D    ADT-ADT-ADR-TYP.ADT-USER-CD   �       	D    ADT-ADT-ADR-TYP.AD
T-LAST-ACTVY-DT   .�       D    ADT-ADT-ADR-TYP.ADT-UNVRS-CD   ;�       
D    ADT-ADT-ADR-TYP.ADT-ADR-TYP-CD   ��       U    ADT-ADT-ADR-TYP.ADT-D
SPLY-PRTY-SEQ   h�       D    ADT-ADT-ADR-TYP.ADT-SHORT-DESC   ��       
D    ADT-ADT-ADR-TYP.ADT-LONG-DESC   ��       D    ADT-ADT-ADR-TYP.ADT-RC
RD-USED-FLAG �  H�       D    DBLINK-R6137-ADR-TYPE-KEY.DBLINK-R6137-ADR-TYP
E-01
DBRM  � 	    )      uUPDATE STA_STATE_V SET USER_CD = : H , LAST_ACTVY_DT = :
 H , FK_UNV_UNVRS_CD = : H , FULL_NAME = : H , RCRD_USED_FLAG = : H WHERE CNTRY_
CD = : H AND STATE_CD = : H       �       D    STA-STA-STATE.STA-USER-CD 
  �       	D    STA-STA-STATE.STA-LAST-ACTVY-DT   &�       D    STA-ST
A-STATE.STA-FK-UNV-UNVRS-CD   ��       D    STA-STA-STATE.STA-FULL-NAME  
 `�       D    STA-STA-STATE.STA-RCRD-USED-FLAG �  ��       D    DBLINK-R
6151-STATE-KEY.DBLINK-R6151-STATE-01 �  ~�       D    DBLINK-R6151-STATE-KEY
.DBLINK-R6151-STATE-02
DBRM  =     f      �UPDATE ZIP_ZIP_V SET USER_CD = : H , LAST_ACTVY_DT = : H
 , FK_UNV_UNVRS_CD = : H , CITY_NAME = : H , SYS_ADD_FLAG = : H , RCRD_USED_FLAG
 = : H WHERE FK_STA_CNTRY_CD = : H AND ZIP_CD = : H       �       D    ZI
P-ZIP-ZIP.ZIP-USER-CD   �       	D    ZIP-ZIP-ZIP.ZIP-LAST-ACTVY-DT   +� 
      D    ZIP-ZIP-ZIP.ZIP-FK-UNV-UNVRS-CD   -�       D    ZIP-ZIP-ZIP.Z
IP-CITY-NAME   ��       D    ZIP-ZIP-ZIP.ZIP-SYS-ADD-FLAG   ��       D 
   ZIP-ZIP-ZIP.ZIP-RCRD-USED-FLAG   y�       D    DBLINK-R6152-ZIP-KEY.DBL
INK-R6152-ZIP-01   ��       D    DBLINK-R6152-ZIP-KEY.DBLINK-R6152-ZIP-02
DBRM       �      lUPDATE CTY_COUNTRY_V SET USER_CD = : H , LAST_ACTVY_DT =
 : H , FK_UNV_UNVRS_CD = : H , FULL_NAME = : H , RCRD_USED_FLAG = : H WHERE CNTR
Y_CD = : H       �       D    CTY-CTY-COUNTRY.CTY-USER-CD   �       	
D    CTY-CTY-COUNTRY.CTY-LAST-ACTVY-DT   ��       D    CTY-CTY-COUNTRY.CTY
-FK-UNV-UNVRS-CD   ��       D    CTY-CTY-COUNTRY.CTY-FULL-NAME   #�      
 D    CTY-CTY-COUNTRY.CTY-RCRD-USED-FLAG �  ��       D    DBLINK-R6153-CO
UNTRY-KEY.DBLINK-R6153-COUNTRY-01
DBRM       L      kUPDATE LPI_LSTASGNPID_V SET USER_CD = : H , LAST_ACTVY_D
T = : H , UNVRS_CD = : H , ALPHA_PREFIX = : H , LST_ASSGND_PID = : H WHERE UNVRS
_CD = : H       �       D    LPI-LPI-LSTASGNPID.LPI-USER-CD   �       
	D    LPI-LPI-LSTASGNPID.LPI-LAST-ACTVY-DT   +�       D    LPI-LPI-LSTASG
NPID.LPI-UNVRS-CD   ��       D    LPI-LPI-LSTASGNPID.LPI-ALPHA-PREFIX   :
�       U    
LPI-LPI-LSTASGNPID.LPI-LST-ASSGND-PID <  ��       D    DBLIN
K-R6161-LSTASGNPID-KEY.DBLINK-R6161-LSTASGNPID-01
DBRM       8      lUPDATE PRA_PRSN_ADR_V SET FK_ZIP_ZIP_CD = : H , ZIP_SET_
TS = : H , ZIP_SETF = : H WHERE FK_PRS_IREF_ID = : H AND ADR_TYP_CD = : H AND EN
D_DT = : H       �       D    PRA-PRA-PRSN-ADR.PRA-FK-ZIP-ZIP-CD   �  
     D    PRA-PRA-PRSN-ADR.PRA-ZIP-SET-TS   |�       D    DBLINK-CONSTAN
TS.DBLINK-MEMBER �  ��       U    DBLINK-R6139-PRSN-ADR-KEY.DBLINK-R6139-PRS
N-ADR-01 �  "�       D    DBLINK-R6139-PRSN-ADR-KEY.DBLINK-R6139-PRSN-ADR-02
 �  ��       D    DBLINK-R6139-PRSN-ADR-KEY.DBLINK-R6139-PRSN-ADR-03
DBRM            7SELECT USER_CD , LAST_ACTVY_DT , DTL_CD , SHORT_DESC , L
ONG_DESC , FLAG_1 , FLAG_2 , ACTV_INATV_CD_FLAG , START_SESN_CD , START_YEAR_DT
, END_SESN_CD , END_YEAR_DT , ALT_ACCESS1 , ALT_ACCESS2 , SUB_DATA , LGCL_DLT_FL
AG , FK_TBH_APLCN_CD , FK_TBH_TBL_CD , TBH_SET_AC1_SETF , TBH_SET_AC2_SETF INTO
: H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H FROM TBD_TABL_DTL_V WHERE FK_TBH_APLCN_CD
= : H AND FK_TBH_TBL_CD = : H AND DTL_CD = : H              D    TBD-TBD
-TABL-DTL.TBD-USER-CD          	D    TBD-TBD-TABL-DTL.TBD-LAST-ACTVY-DT  
        D    TBD-TBD-TABL-DTL.TBD-DTL-CD          D    TBD-TBD-TABL
-DTL.TBD-SHORT-DESC  �        D    TBD-TBD-TABL-DTL.TBD-LONG-DESC  �   
     D    TBD-TBD-TABL-DTL.TBD-FLAG-1  (        D    TBD-TBD-TABL-DTL.T
BD-FLAG-2  �        D    TBD-TBD-TABL-DTL.TBD-ACTV-INATV-CD-FLAG  �    
    D    TBD-TBD-TABL-DTL.TBD-START-SESN-CD  ^        U    TBD-TBD-TABL
-DTL.TBD-START-YEAR-DT  �        D    TBD-TBD-TABL-DTL.TBD-END-SESN-CD  
,        U    TBD-TBD-TABL-DTL.TBD-END-YEAR-DT  �        D    TBD-TBD-
TABL-DTL.TBD-ALT-ACCESS1  �        D    TBD-TBD-TABL-DTL.TBD-ALT-ACCESS2 
 '        D    TBD-TBD-TABL-DTL.TBD-SUB-DATA  c        D    TBD-TBD-T
ABL-DTL.TBD-LGCL-DLT-FLAG  i        D    TBD-TBD-TABL-DTL.TBD-FK-TBH-APLCN
-CD  �        D    TBD-TBD-TABL-DTL.TBD-FK-TBH-TBL-CD  n        D    

TBD-TBD-TABL-DTL.TBD-TBH-SET-AC1-SETF  �        D    
TBD-TBD-TABL-DTL.TBD
-TBH-SET-AC2-SETF � ��       D    DBLINK-S-6600-6601-KEY-O.DBLINK-S-6600-66
01-01 � T�       D    DBLINK-S-6600-6601-KEY-O.DBLINK-S-6600-6601-02 � 4�
       D    DBLINK-S-6600-6601-KEY-U.DBLINK-S-6600-6601-03
DBRM  �     >     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , ENTY_ID_DGT_
ONE , ENTY_ID_LST_NINE , FK2_ECG_CHG_DT , FK1_ENT_ENTY_ONE , FK1_ENT_ENTY_NINE ,
 ENT_SET_TS , FK2_ECG_ENTY_ONE , FK2_ECG_ENTY_NINE , FK2_ENT_ECG_SET_TS , ECG_SE
T_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
, : H FROM ENI_ENTY_ID_V WHERE ENTY_ID_DGT_ONE = : H AND ENTY_ID_LST_NINE = : H
      T        D    ENI-ENI-ENTY-ID.ENI-USER-CD   Z        	D    ENI-E
NI-ENTY-ID.ENI-LAST-ACTVY-DT   �        D    ENI-ENI-ENTY-ID.ENI-UNVRS-CD 
  5        D    ENI-ENI-ENTY-ID.ENI-ENTY-ID-DGT-ONE   �        D    ENI
-ENI-ENTY-ID.ENI-ENTY-ID-LST-NINE          D    ENI-ENI-ENTY-ID.ENI-FK2-E
CG-CHG-DT          D    ENI-ENI-ENTY-ID.ENI-FK1-ENT-ENTY-ONE         
 D    
ENI-ENI-ENTY-ID.ENI-FK1-ENT-ENTY-NINE          D    ENI-ENI-ENTY
-ID.ENI-ENT-SET-TS          D    ENI-ENI-ENTY-ID.ENI-FK2-ECG-ENTY-ONE  
        D    
ENI-ENI-ENTY-ID.ENI-FK2-ECG-ENTY-NINE  
        D    ENI
-ENI-ENTY-ID.ENI-FK2-ENT-ECG-SET-TS          D    ENI-ENI-ENTY-ID.ENI-ECG
-SET-TS � !�       D    DBLINK-S-INDX-ENTY-ID-KEY-U.DBLINK-S-INDX-ENTY-ID-0
1 � ��       D    DBLINK-S-INDX-ENTY-ID-KEY-U.DBLINK-S-INDX-ENTY-ID-02
DBRM  z     �     |SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , PRSN_ID_DGT_
ONE , PRSN_ID_LST_NINE , CHANGE_DT , FK_PRS_IREF_ID , PRS_SET_TS , FK_PCH_IREF_I
D , FK_PCH_CHG_DT , FK_PCH_SET_TS , PCH_SET_TS INTO : H , : H , : H , : H , : H
, : H , : H , : H , : H , : H , : H , : H FROM PPI_PRSN_PRVID_V WHERE PRSN_ID_DG
T_ONE = : H AND PRSN_ID_LST_NINE = : H       ]        D    PPI-PPI-PRSN-PR
VID.PPI-USER-CD   C        	D    PPI-PPI-PRSN-PRVID.PPI-LAST-ACTVY-DT   I
       D    PPI-PPI-PRSN-PRVID.PPI-UNVRS-CD   �        D    PPI-PPI-PRSN
-PRVID.PPI-PRSN-ID-DGT-ONE   N        D    PPI-PPI-PRSN-PRVID.PPI-PRSN-ID-L
ST-NINE   �        D    PPI-PPI-PRSN-PRVID.PPI-CHANGE-DT   �        U  
  
PPI-PPI-PRSN-PRVID.PPI-FK-PRS-IREF-ID   X        D    PPI-PPI-PRSN-PRVID
.PPI-PRS-SET-TS   �        U    
PPI-PPI-PRSN-PRVID.PPI-FK-PCH-IREF-ID   3
        D    PPI-PPI-PRSN-PRVID.PPI-FK-PCH-CHG-DT   9        D    PPI-PP
I-PRSN-PRVID.PPI-FK-PCH-SET-TS   �        D    PPI-PPI-PRSN-PRVID.PPI-PCH-S
ET-TS � �       D    DBLINK-S-INDX-PRSN-ID-KEY-U.DBLINK-S-INDX-PRSN-ID-01 
� <�       D    DBLINK-S-INDX-PRSN-ID-KEY-U.DBLINK-S-INDX-PRSN-ID-02
DBRM  3          �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , FICE_CD , SH
ORT_NAME , FULL_NAME , LINE_ADR_1 , LINE_ADR_2 , LINE_ADR_3 , LINE_ADR_4 , TLPHN
_ID , CITY_NAME , STATE_CD , ZIP_CD , CNTRY_CD , FK_SYS_SYS_CD , SYS_SET_TS INTO
 : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H , : H , : H FROM UNV_UNVRS_V WHERE UNVRS_CD = : H       �        D
    UNV-UNV-UNVRS.UNV-USER-CD   \        	D    UNV-UNV-UNVRS.UNV-LAST-ACTVY
-DT   W        D    UNV-UNV-UNVRS.UNV-UNVRS-CD   �        U    UNV-UNV
-UNVRS.UNV-FICE-CD   2        D    UNV-UNV-UNVRS.UNV-SHORT-NAME   8      
  D    UNV-UNV-UNVRS.UNV-FULL-NAME   �        D    UNV-UNV-UNVRS.UNV-LIN
E-ADR-1          D    UNV-UNV-UNVRS.UNV-LINE-ADR-2          D    U
NV-UNV-UNVRS.UNV-LINE-ADR-3          D    UNV-UNV-UNVRS.UNV-LINE-ADR-4  
        D    UNV-UNV-UNVRS.UNV-TLPHN-ID          D    UNV-UNV-UNVRS
.UNV-CITY-NAME          D    UNV-UNV-UNVRS.UNV-STATE-CD          D 
   UNV-UNV-UNVRS.UNV-ZIP-CD          D    UNV-UNV-UNVRS.UNV-CNTRY-CD  
        D    UNV-UNV-UNVRS.UNV-FK-SYS-SYS-CD          D    UNV-UNV-U
NVRS.UNV-SYS-SET-TS  -�       D    UNV-UNV-UNVRS.UNV-UNVRS-CD
DBRM  D     �      �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , ADR_TYP_CD ,
 DSPLY_PRTY_SEQ , SHORT_DESC , LONG_DESC , RCRD_USED_FLAG INTO : H , : H , : H ,
 : H , : H , : H , : H , : H FROM ADT_ADR_TYP_V WHERE ADR_TYP_CD = : H       
�        D    ADT-ADT-ADR-TYP.ADT-USER-CD   =        	D    ADT-ADT-ADR-TY
P.ADT-LAST-ACTVY-DT   d        D    ADT-ADT-ADR-TYP.ADT-UNVRS-CD   �     
   D    ADT-ADT-ADR-TYP.ADT-ADR-TYP-CD   �        U    ADT-ADT-ADR-TYP.A
DT-DSPLY-PRTY-SEQ   o        D    ADT-ADT-ADR-TYP.ADT-SHORT-DESC   �     
   D    ADT-ADT-ADR-TYP.ADT-LONG-DESC   s        D    ADT-ADT-ADR-TYP.AD
T-RCRD-USED-FLAG   ��       D    ADT-ADT-ADR-TYP.ADT-ADR-TYP-CD
DBRM  7     s      SSELECT USER_CD , LAST_ACTVY_DT , FK_UNV_UNVRS_CD , CNTRY
_CD , STATE_CD , FULL_NAME , RCRD_USED_FLAG , FK_CTY_CNTRY_CD INTO : H , : H , :
 H , : H , : H , : H , : H , : H FROM STA_STATE_V WHERE CNTRY_CD = : H AND STATE
_CD = : H       @        D    STA-STA-STATE.STA-USER-CD   b        	D  
  STA-STA-STATE.STA-LAST-ACTVY-DT   h        D    STA-STA-STATE.STA-FK-UNV
-UNVRS-CD   �        D    STA-STA-STATE.STA-CNTRY-CD   m        D    S
TA-STA-STATE.STA-STATE-CD   �        D    STA-STA-STATE.STA-FULL-NAME   �
        D    STA-STA-STATE.STA-RCRD-USED-FLAG   w        D    STA-STA-ST
ATE.STA-FK-CTY-CNTRY-CD   ��       D    STA-STA-STATE.STA-CNTRY-CD   ��  
     D    STA-STA-STATE.STA-STATE-CD
DBRM  �     S     SELECT USER_CD , LAST_ACTVY_DT , FK_UNV_UNVRS_CD , FK_ST
A_CNTRY_CD , ZIP_CD , CITY_NAME , SYS_ADD_FLAG , RCRD_USED_FLAG , FK_STA_STATE_C
D , STA_SETF , STA_SET_TS INTO : H , : H , : H , : H , : H , : H , : H , : H , :
 H , : H , : H FROM ZIP_ZIP_V WHERE FK_STA_CNTRY_CD = : H AND ZIP_CD = : H     
  y        D    ZIP-ZIP-ZIP.ZIP-USER-CD   �        	D    ZIP-ZIP-ZIP.ZI
P-LAST-ACTVY-DT   �        D    ZIP-ZIP-ZIP.ZIP-FK-UNV-UNVRS-CD   �      
  D    ZIP-ZIP-ZIP.ZIP-FK-STA-CNTRY-CD   {        D    ZIP-ZIP-ZIP.ZIP-Z
IP-CD   F        D    ZIP-ZIP-ZIP.ZIP-CITY-NAME   �        D    ZIP-ZI
P-ZIP.ZIP-SYS-ADD-FLAG   K        D    ZIP-ZIP-ZIP.ZIP-RCRD-USED-FLAG   Q
        D    ZIP-ZIP-ZIP.ZIP-FK-STA-STATE-CD   �        D    ZIP-ZIP-ZIP
.ZIP-STA-SETF   U        D    ZIP-ZIP-ZIP.ZIP-STA-SET-TS  �       D  
  ZIP-ZIP-ZIP.ZIP-FK-STA-CNTRY-CD  �       D    ZIP-ZIP-ZIP.ZIP-ZIP-CD
DBRM             ySELECT USER_CD , LAST_ACTVY_DT , FK_UNV_UNVRS_CD , CNTRY
_CD , FULL_NAME , RCRD_USED_FLAG INTO : H , : H , : H , : H , : H , : H FROM CTY
_COUNTRY_V WHERE CNTRY_CD = : H       ^        D    CTY-CTY-COUNTRY.CTY-US
ER-CD   �        	D    CTY-CTY-COUNTRY.CTY-LAST-ACTVY-DT   ,        D   
 CTY-CTY-COUNTRY.CTY-FK-UNV-UNVRS-CD   �        D    CTY-CTY-COUNTRY.CTY-C
NTRY-CD   �        D    CTY-CTY-COUNTRY.CTY-FULL-NAME   '        D    
CTY-CTY-COUNTRY.CTY-RCRD-USED-FLAG   v�       D    CTY-CTY-COUNTRY.CTY-CNTR
Y-CD
DBRM  �     �     �SELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , APLCN_CD , T
ABLE_CD , FNCTN_DESC , TBL_DTL_CD_LGTH , DTL_CD_CLMN_LBL_1 , DTL_CD_CLMN_LBL_2 ,
 FLAG_1_LABEL_1 , FLAG_1_LABEL_2 , FLAG_1_VALUE_1 , FLAG_1_VALUE_2 , FLAG_1_VALU
E_3 , FLAG_1_VALUE_4 , FLAG_1_VALUE_5 , FLAG_1_DFLT_VALUE , FLAG_2_LABEL_1 , FLA
G_2_LABEL_2 , FLAG_2_VALUE_1 , FLAG_2_VALUE_2 , FLAG_2_VALUE_3 , FLAG_2_VALUE_4
, FLAG_2_VALUE_5 , FLAG_2_DFLT_VALUE INTO : H , : H , : H , : H , : H , : H , :
H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H , : H
, : H , : H , : H , : H , : H FROM TBH_TABL_HDR_V WHERE APLCN_CD = : H AND TABLE
_CD = : H      t        D    TBH-TBH-TABL-HDR.TBH-USER-CD  z        	
D    TBH-TBH-TABL-HDR.TBH-LAST-ACTVY-DT  �        D    TBH-TBH-TABL-HDR.T
BH-UNVRS-CD  �        D    TBH-TBH-TABL-HDR.TBH-APLCN-CD  �        D 
   TBH-TBH-TABL-HDR.TBH-TABLE-CD  A        D    TBH-TBH-TABL-HDR.TBH-FNCT
N-DESC  G        U    TBH-TBH-TABL-HDR.TBH-TBL-DTL-CD-LGTH  �        
D    TBH-TBH-TABL-HDR.TBH-DTL-CD-CLMN-LBL-1  L        D    TBH-TBH-TABL-H
DR.TBH-DTL-CD-CLMN-LBL-2  R        	D    TBH-TBH-TABL-HDR.TBH-FLAG-1-LABEL-
1  �        	D    TBH-TBH-TABL-HDR.TBH-FLAG-1-LABEL-2  V        D    
TBH-TBH-TABL-HDR.TBH-FLAG-1-VALUE-1  �        D    TBH-TBH-TABL-HDR.TBH-FL
AG-1-VALUE-2  1        D    TBH-TBH-TABL-HDR.TBH-FLAG-1-VALUE-3  7     
   D    TBH-TBH-TABL-HDR.TBH-FLAG-1-VALUE-4  �        D    TBH-TBH-TABL
-HDR.TBH-FLAG-1-VALUE-5          D    TBH-TBH-TABL-HDR.TBH-FLAG-1-DFLT-VA
LUE          	D    TBH-TBH-TABL-HDR.TBH-FLAG-2-LABEL-1          	D   
 TBH-TBH-TABL-HDR.TBH-FLAG-2-LABEL-2  
        D    TBH-TBH-TABL-HDR.TBH-
FLAG-2-VALUE-1          D    TBH-TBH-TABL-HDR.TBH-FLAG-2-VALUE-2     
     D    TBH-TBH-TABL-HDR.TBH-FLAG-2-VALUE-3          D    TBH-TBH-TA
BL-HDR.TBH-FLAG-2-VALUE-4          D    TBH-TBH-TABL-HDR.TBH-FLAG-2-VALUE
-5          D    TBH-TBH-TABL-HDR.TBH-FLAG-2-DFLT-VALUE  *�       D 
   TBH-TBH-TABL-HDR.TBH-APLCN-CD  ?�       D    TBH-TBH-TABL-HDR.TBH-TABL
E-CD
DBRM  �     F      oSELECT USER_CD , LAST_ACTVY_DT , UNVRS_CD , ALPHA_PREFIX
 , LST_ASSGND_PID INTO : H , : H , : H , : H , : H FROM LPI_LSTASGNPID_V WHERE U
NVRS_CD = : H       &        D    LPI-LPI-LSTASGNPID.LPI-USER-CD   �    
    	D    LPI-LPI-LSTASGNPID.LPI-LAST-ACTVY-DT   *        D    LPI-LPI-LS
TASGNPID.LPI-UNVRS-CD   �        D    LPI-LPI-LSTASGNPID.LPI-ALPHA-PREFIX 
  �        U    
LPI-LPI-LSTASGNPID.LPI-LST-ASSGND-PID   l�       D    L
PI-LPI-LSTASGNPID.LPI-UNVRS-CD
DBRM   �     �      SET : H = CURRENT TIMESTAMP       	        D    DB
LINK-OTHER-WORK-AREAS.DBLINK-TIMESTAMP
